#coding=utf-8

import os, sys, inspect
import argparse

# realpath() will make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
  sys.path.insert(0, cmd_folder)

 # use this if you want to include modules from a subfolder
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"modules")))
if cmd_subfolder not in sys.path:
  sys.path.insert(0, cmd_subfolder)

from utils import python3_checker
python3_checker()
import commands as cmd
from repo_service import isdept


config_actions = ['show', 'set']
change_actions = ['add', 'remove', 'show']#TODO


def add_arguments(parser):#{
  parser.add_argument('-n', '--name', help='user name, e.g. Jack', dest=cmd.a_username, type=str)
  parser.add_argument('-m', '--mail', help='user mail, asd@gmail.com', dest=cmd.a_usermail, type=str)
  parser.add_argument('-r', '--remote-name', help='remote repository name, e.g. deptrepo', dest=cmd.a_reponame, type=str, default='deptrepo')
  parser.add_argument('-d', '--remote-addr', help='remote repository address. Format: [user:password@]address[:port]. Examples: dept.lan, 192.168.1.110:2121, user:password@localhost:21', dest=cmd.a_repoaddr, type=str)
  parser.add_argument('-e', '--remote-path', help='remote path to repository, e.g. /home/user/repos (for debug purposes only)', dest=cmd.a_repopath, type=str)
#}


if __name__ == "__main__":
  
  python3_checker()()
  if(len(sys.argv) == 1): sys.argv.append('-h')
  parser = argparse.ArgumentParser(prog="dept")
  subparsers = parser.add_subparsers(dest='command', title='Commands', help='valid commands')
  subparsers.required = True

  #init parser
  parser_init = subparsers.add_parser('init', help='initialize local dept repository')
  parser_init.add_argument('-u', '--update', help='Trying to update repo after initialization', action='store_true', dest=cmd.a_update)
  add_arguments(parser_init)
  parser_init.add_argument('-p', '--path', help='Path where create local repository (. by default)', dest=cmd.a_localpath, default='.', type=str)
  parser_init.add_argument('-b', '--branch', help='remote repository branch, e.g. master', required=False, dest=cmd.a_brname, type=str, default='master')
  parser_init.set_defaults(func=cmd.init)

  
  #install parser
  parser_install = subparsers.add_parser('install', help='install package')
  parser_install.add_argument(cmd.a_pname, metavar='PNAME', help='package name, e.g. libxml', type=str)
  parser_install.add_argument('-v', '--version', help='package version, e.g. 1.5', required=False, type=str, dest=cmd.a_pver)
  parser_install.add_argument('-s', '--single', action='store_true', help='install package without dependencies', required=False, dest=cmd.a_single)
  #parser_install.add_argument('-l', '--list', action='store_true', help='show list of all installed packages', dest='show')
  parser_install.add_argument('-f', '--force', action='store_true', help='force install package (reinstall package already installed)', dest=cmd.a_force)
  parser_install.add_argument('-y', '--yes', action='store_true', help='automatic "yes" to all questions', dest=cmd.a_yes)
  parser_install.add_argument('-F', '--no-files', action='store_true', help='install package without unpacking files', dest=cmd.a_nofiles)
  parser_install.set_defaults(func=cmd.install)
  

  #config parser
  parser_config = subparsers.add_parser('config', help='configure local dept repository and packages changes')
  config_subparsers = parser_config.add_subparsers(dest=cmd.a_action, title='actions', help='valid config actions')
  config_subparsers.required = True
  #config set subparser
  config_set = config_subparsers.add_parser('set', help='set configuration')
  config_set.add_argument('-b', '--branch', help='remote repository branch, e.g. master', required=False, dest=cmd.a_brname, type=str)
  add_arguments(config_set)
  #config show subparser
  config_show = config_subparsers.add_parser('show', help='show configuration')
  config_show.add_argument('-f', '--full', action='store_true', help='show full configuration', dest=cmd.a_all)
  config_show.add_argument('-a', '--arch', help='packages architecture', action='store_true', dest=cmd.a_arch)
  config_show.add_argument('-n', '--name', help='user name, e.g. Jack', dest=cmd.a_username, action='store_true')
  config_show.add_argument('-m', '--mail', help='user mail, asd@gmail.com', dest=cmd.a_usermail, action='store_true')
  config_show.add_argument('-r', '--remote-name', help='remote repository name, e.g. deptrepo', dest=cmd.a_reponame, action='store_true')
  config_show.add_argument('-d', '--remote-addr', help='remote repository address, e.g. dept.lan', dest=cmd.a_repoaddr, action='store_true')
  config_show.add_argument('-e', '--remote-path', help='remote path to repository, e.g. /home/user/repos', dest=cmd.a_repopath, action='store_true')
  config_show.add_argument('-b', '--branch', help='remote repository branch, e.g. master', dest=cmd.a_brname, action='store_true')
  parser_config.set_defaults(func=cmd.config)


  #remove parser
  parser_remove = subparsers.add_parser('remove', help='remove installed packages')
  parser_remove.add_argument('pname', metavar='PNAME', help='package name, e.g. libxml', type=str)
  parser_remove.add_argument('-s', '--single', action='store_true', help='remove package without dependencies', required=False, dest=cmd.a_single)
  parser_remove.add_argument('-f', '--force', action='store_true', help='force remove depend package with dependencies (not recommended)', dest=cmd.a_force)
  parser_remove.set_defaults(func=cmd.remove)
  

  #upload parser
  parser_upload = subparsers.add_parser('upload', help='upload installed packages and install upload packages')
  parser_upload.add_argument('-T', '--notime', action='store_true',\
help='trying upload package(s) not considering files timestamps (option not recommended)', dest=cmd.a_notime)
  parser_upload.add_argument('-b', '--update-packages', action='store_true',\
help='Add \'update binary package files\' change for all installed packages', dest=cmd.a_upchange)
  #parser_upload.add_argument('-i', '--install', action='store_true', help='Install packages after uploading (not force mode)', dest=cmd.a_install)
  #parser_upload.add_argument('-u', '--update-after', action='store_true', help='Update local repo if upload process succeed', dest=cmd.a_update)
  #
  #upload_subparsers = parser_upload.add_subparsers(dest='instance', title='instances', help='instances types')
  #upload_subparsers.required = True
  #upload all subparser
  #upload_all = upload_subparsers.add_parser('all', help='upload all changed packages')
  #upload package subparser
  #upload_package = upload_subparsers.add_parser('package', help='upload specified package(s)')
  #upload_package.add_argument('pname', metavar='PNAME', help='package name, e.g. libxml', type=str)
  #upload_package.add_argument('-s', '--single', action='store_true', help='upload package without dependencies', required=False, dest='single')
  #
  parser_upload.set_defaults(func=cmd.upload)

  
  #change parser
  parser_change = subparsers.add_parser('change', help='change installed packages anyway')
  parser_change.add_argument('pname', metavar='PNAME', help='package name, e.g. libxml', type=str)
  #change subparses
  change_subparsers = parser_change.add_subparsers(dest='action', title='Action', help='valid change actions')
  change_subparsers.required = True

  #change add subparser
  change_add = change_subparsers.add_parser('add', help="Add 'add' change to package")
  changeadd_subparsers = change_add.add_subparsers(dest='instance', title='instances', help='instances types')
  changeadd_subparsers.required = True
  #change add file subparser
  change_add_file = changeadd_subparsers.add_parser('file', help='file instance')
  change_add_file.add_argument('filepath', metavar='filepath', help='absolute file path, e.g. /home/jack/debug', type=str)
  change_add_file.add_argument('filename', metavar='filename', help='file name, e.g. libxml', type=str)
  change_add_file.add_argument('destpath', metavar='destpath', help='destination package path, where file would be located, e.g. bin/some/path', type=str)
  #change add dep subparser
  change_add_dep = changeadd_subparsers.add_parser('dep', help='dependent package instance. Note: choose package name and package version from existed packages list or create package first')
  change_add_dep.add_argument('deppname', metavar='pname', help='dependent package name, e.g. libxml', type=str)
  change_add_dep.add_argument('deppver', metavar='pver', help='dependent package version, e.g. 1.8', type=str)
  change_add_dep.add_argument('-r', '--repo', dest='depprepo', help='dependent package repo, e.g. deptrepo', type=str)
  change_add_dep.add_argument('-b', '--branch', dest='deppbr', help='dependent package branch, e.g. master', type=str)

  #change rm subparser
  change_rm = change_subparsers.add_parser('rm', help='Add \'remove\' change to package')
  changerm_subparsers = change_rm.add_subparsers(dest='instance', title='instances', help='instances types')
  changerm_subparsers.required = True
  #change rm file subparser
  change_rm_file = changerm_subparsers.add_parser('file', help='file instance. Note: file must exist inside package. Show package files before')
  change_rm_file.add_argument('filepath', metavar='filepath', help='file path, e.g. script', type=str)
  change_rm_file.add_argument('filename', metavar='filename', help='file name, e.g. postrm.py', type=str)
  #change rm dep subparser
  change_rm_dep = changerm_subparsers.add_parser('dep', help='dependent package instance. Note: choose package name and package version from existed packages list or create package first')
  change_rm_dep.add_argument('deppname', metavar='pname', help='dependent package name, e.g. libxml', type=str)
  change_rm_dep.add_argument('deppver', metavar='pver', help='dependent package version, e.g. 1.8', type=str)
  #change_rm_dep.add_argument('-r', '--repo', dest='depprepo', help='dependent package repo, e.g. deptrepo', type=str)
  #change_rm_dep.add_argument('-b', '--branch', dest='deppbr', help='dependent package branch, e.g. master', type=str)

  #change update subparser
  change_up = change_subparsers.add_parser('up', help='Add \'update\' change to package')
  changeup_subparsers = change_up.add_subparsers(dest='instance', title='instances', help='instances types')
  changeup_subparsers.required = True
  #change up package files subparser
  change_up_pkgbins = changeup_subparsers.add_parser('bins', help='Package binary files (bin directory package files)')
  #change up file subparser
  change_up_file = changeup_subparsers.add_parser('file', help='Package file. Note: file must exist inside package. Show package files before')
  change_up_file.add_argument('filepath', metavar='filepath', help='file path, e.g. script', type=str)
  change_up_file.add_argument('filename', metavar='filename', help='file name, e.g. postrm.py', type=str)
  change_up_file.add_argument('sourcepath', metavar='sourcepath', help='Source file path, e.g. /home/user/some/dir', type=str)

  #change alter subparser
  change_alter = change_subparsers.add_parser('alter', help='Add \'alter\' change to package')
  changealter_subparsers = change_alter.add_subparsers(dest='instance', title='instances', help='instances types')
  changealter_subparsers.required = True
  #change alter pname subparser
  change_alter_pname = changealter_subparsers.add_parser('name', help='Package name')
  change_alter_pname.add_argument('newpname', metavar='NEWNAME', help='new package name', type=str)
  #change alter pversion subparser
  change_alter_pver = changealter_subparsers.add_parser('version', help='Package version')
  change_alter_pver.add_argument('newpver', metavar='NEWVERSION', help='new package version', type=str)
  #change alter pmaintainer subparser
  change_alter_pmaintain = changealter_subparsers.add_parser('maintainer', help='Package maintainer')
  change_alter_pmaintain.add_argument('newpmaintainer', metavar='NEWMAINTAINER', help='new package maintainer', type=str)
  #change alter psection subparser
  change_alter_psect = changealter_subparsers.add_parser('section', help='Package section')
  change_alter_psect.add_argument('newpsect', metavar='NEWSECTION', help='new package section', type=str)
  #change alter pdesc subparser
  change_alter_pdesc = changealter_subparsers.add_parser('description', help='Package description')
  change_alter_pdesc.add_argument('newpdesc', metavar='NEWDESCRIPTION', help='new package description', type=str)
  #change alter pprior subparser
  change_alter_pprior = changealter_subparsers.add_parser('priority', help='Package priority')
  change_alter_pprior.add_argument('newpprior', metavar='NEWPRIORITY', help='new package priority', type=str)

  #change create subparser
  change_create = change_subparsers.add_parser('create', help='Add \'create\' change to package (if package is not already exist)')

  parser_change.set_defaults(func=cmd.change)

  #create parser
#  parser_create = subparsers.add_parser('createpkg', help='create dept package')
#  parser_create.add_argument('path', metavar='PATHTOPACKAGE', help='path to package, e.g. /home/user/some/path',  type=str)
#  parser_create.add_argument('-r', '--repo-name', help='repository name to save package, e.g. deptrepo (current by default)', dest='repo', default='current')
#  parser_create.add_argument('-b', '--branch', help='repository branch to save package, e.g. master (current by default)', dest='branch', default='current')
#  parser_create.set_defaults(func=cmd.create)

  #check changes parser
  """\
  parser_checkup = subparsers.add_parser('checkup', help='Check packages changes (by applying) without uploading')
  #parser_checkup.add_argument('-T', '--notime', action='store_true', help='trying upload package(s) not considering files timestamps (option not recommended)')
  checkup_subparsers = parser_checkup.add_subparsers(dest='instance', title='instances', help='instances types')
  checkup_subparsers.required = True
  #upload all subparser
  checkup_all = checkup_subparsers.add_parser('all', help='check upload changes for all changed packages')
  #upload package subparser
  checkup_package = checkup_subparsers.add_parser('package', help='check upload changes for specified package(s)')
  checkup_package.add_argument('pname', metavar='PNAME', help='package name, e.g. libxml', type=str)
  checkup_package.add_argument('-s', '--single', action='store_true', help='check package changes without dependencies', required=False, dest='single')
  parser_checkup.set_defaults(func=cmd.checkup)
  """

  #update parser
  parser_update = subparsers.add_parser('update', help='Update remote repository info')
  parser_update.set_defaults(func=cmd.update)


  #packages parser
  parser_packages = subparsers.add_parser('packages', help='Show any package(s) information')
  packages_subparsers = parser_packages.add_subparsers(dest=cmd.a_instance, title='instances', help='instances types')
  packages_subparsers.required = True
  #all subparser
  packages_all = packages_subparsers.add_parser(cmd.a_all, help='show all packages in remote repository')
  packages_all.add_argument('-b', '--branch', help='show packages by specific branch', required=False, type=str, dest=cmd.a_brname)
  #packages_all.add_argument('-s', '--branch-list', help='show branch list', action='store_true', 'brlist')
  #installed subparser
  packages_installed = packages_subparsers.add_parser(cmd.a_installed, help='show all installed packages inside local repository')
  packages_installed.add_argument('-m', '--main', action='store_true', help='show main (not depended) packages installed only', dest=cmd.a_mainpkgs)
  #depend subparsers
  packages_depend = packages_subparsers.add_parser(cmd.a_depended, help='show all depended packages for package specified')
  packages_depend.add_argument(cmd.a_pname, metavar='PNAME', help='package name, e.g. libxml', type=str)
  packages_depend.add_argument('-v', '--version', help='package version, e.g. 1.5', required=False, type=str, dest=cmd.a_pver)
  packages_depend.add_argument('-t', '--tree-view', action='store_true', help='show depended packages as tree-like view', dest=cmd.a_treeview)
  #based subparsers
  packages_based = packages_subparsers.add_parser(cmd.a_based, help='show all packages that based on package specified')
  packages_based.add_argument(cmd.a_pname, metavar='PNAME', help='package name, e.g. libxml', type=str)
  packages_based.add_argument('-v', '--version', help='package version, e.g. 1.5', required=False, type=str, dest=cmd.a_pver)
  #packages_based.add_argument('-t', '--tree-view', action='store_true', help='show depended packages as tree-like view', dest=cmd.a_treeview)
  #files subparsers
  packages_files = packages_subparsers.add_parser(cmd.a_files, help='show file(s) info for package specified')
  packages_files.add_argument(cmd.a_pname, metavar='PNAME', help='package name, e.g. libxml', type=str)
  packages_files.add_argument('-v', '--version', help='package version, e.g. 1.5', required=False, type=str, dest=cmd.a_pver)
  packages_files.add_argument('-d', '--directory', help='directory name inside package(bin, scripts or dept)', required=False, type=str, dest=cmd.a_directory)  
  #packages_files.add_argument('-f', '--file', help='relative path to file to show', required=False, type=str, dest=cmd.a_file)
  #versions subparsers
  packages_versions = packages_subparsers.add_parser(cmd.a_versions, help='show all package versions')
  packages_versions.add_argument(cmd.a_pname, metavar='PNAME', help='package name, e.g. libxml', type=str)  
  #logs subparsers
  packages_logs = packages_subparsers.add_parser(cmd.a_logs, help='show logs for any package(s)')
  #packages_logs.add_argument('-p', '--pname', help='show logs for package PNAME, e.g. plus', required=False, type=str, dest=cmd.a_pname)
  packages_logs.add_argument('-n', '--entries', metavar='N', help='output the last N entries, instead of the last 1', type=int, required=False, dest=cmd.a_lines, default=1)
  #changes subparsers
  packages_changes = packages_subparsers.add_parser(cmd.a_changes, help='show all packages changes')
  parser_packages.set_defaults(func=cmd.packages_info)

  #create package parser
  parser_cpkg = subparsers.add_parser('cpkg', help='Create dept package')
  parser_cpkg.add_argument(cmd.a_pname, metavar='PNAME', help='package name, e.g. libxml', type=str)
  parser_cpkg.add_argument('-v', '--version', help='start package version, 1.0 by default', required=False, default=None, type=str, dest=cmd.a_pver)
  parser_cpkg.add_argument('-e', '--depends', help='package dependencies in format pname:pver or pname. If pver is empty, last version assumed, (e.g. plus:1.0 minus mult:1.2)', nargs='+', required=False, default=None, dest=cmd.a_depended)
  parser_cpkg.add_argument('-d', '--description', help='package description, "Package \'pname\', software tools" by default', required=False, type=str, dest=cmd.a_desc)
  parser_cpkg.add_argument('-s', '--section', help='package section, "devel" by default', default=None, type=str, dest=cmd.a_section)
  parser_cpkg.add_argument('-p', '--priority', help='package priority, "optional" by default', type=str, default=None, dest=cmd.a_priority)
  parser_cpkg.add_argument('-t', '--paths', help='paths to package files (e.g. projects/debug /home/user/debug), . by default', nargs='+', default=None, required=False, dest=cmd.a_localpath)
  parser_cpkg.add_argument('-f', '--files', help='package files in format [prefixes/]file (e.g. libxml, plugins/lxml dir1/dir2/dir3/libxml2). Prefix means directories struct, that would be created while package installation. File means pattern that searches by regex (i.e. if pname is "plus" or "prefixes/plus", files plus.so, libplus.so.1, libplus.a, plus.exe will match). By default search files that matching package name by regex', nargs='+', required=False, default=None, dest=cmd.a_files)
  parser_cpkg.add_argument('-F', '--no-files', help='Flag to create package without binary files', action='store_true', dest=cmd.a_nofiles)
  #parser_cpkg.add_argument('-o', '--outpath', help='created package output path, e.g. /home/user/packages. If there is a dept repository, objects path else .dept/objects/repo/branch/pname by default. Note: if specifing, folder \'pname\' would be created on the top of outpath', type=str, required=False, dest=cmd.a_outpath)
  parser_cpkg.add_argument('-n', '--name', help='maintainer name (from configuration by default)', type=str, required=False, dest=cmd.a_username)
  parser_cpkg.add_argument('-m', '--mail', help='maintainer mail (from configuration by default)', type=str, required=False, dest=cmd.a_usermail)
  parser_cpkg.add_argument('-c', '--scripts', help='path to package pre/post scripts, e.g. /home/user/scripts. . by default', type=str, required=False, default=None, dest=cmd.a_scripts)
  parser_cpkg.add_argument('-y', '--yes', action='store_true', help='Automatic "yes" to all questions', dest=cmd.a_yes)
  parser_cpkg.add_argument('-x', '--postfix', help='package name postfix, e.g. if package name is "mylib" and postfix "d" specified, package "mylibd" would be created', default=None, required=False, dest=cmd.a_postfix)
  
  parser_cpkg.set_defaults(func=cmd.create_package)

  args = parser.parse_args()

  #TODO: for real repository
  #isdept(lambda : True)()

  args.func(args)
  
