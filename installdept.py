def bashexec(cmd):  #{
  import subprocess
  pipe = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  outs = [ str(one, errors='ignore') for one in pipe.stdout.readlines()]
  errs = [str(one, errors='ignore') for one in pipe.stderr.readlines()]
  pipe.communicate()
  return (''.join(outs)).strip(), errs, pipe.returncode

python = 'python3'
tests = 'runtests.py'
import os, sys, shutil
if os.name == 'nt': python = 'python'

os.chdir('tests/tests')
out, err, code = bashexec('{} {}'.format(python, tests))
print('Output:\n{}\n'.format(out))
if err: print('Errors:\n{}\n'.format(''.join(err)))
if code: sys.exit('tests failed. Cannot install')

os.chdir('../..')
if os.name == 'posix':
  dept = '/usr/bin/dept'
  out, err, code = bashexec(r"""sudo echo -e "#!/bin/bash\npython3 `pwd`/dept.py \$@" | sudo tee {}""".format(dept))
  if err: print(''.join(err))
  if code: sys.exit(code)
  print('OK')
  out, err, code = bashexec(r"""sudo chmod +x {}""".format(dept))
  if err: print(''.join(err))
  if code: sys.exit(code)
  print('OK')
  print('SUCCESS!\nType:\n#dept -h\nto show help message')
elif os.name == 'nt':
  print("OK. Let's install dept together!\n1.Create dept.bat file\n2.Insert following line into it:\npython path/to/file/dept.py %*\n3.Move file dept.bat to any system binary path\n4.Type\ndept --help")
