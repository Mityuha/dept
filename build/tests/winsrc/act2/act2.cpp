#include "../act1/act1.h"
#include "../mult/mult.h"

double act2(double a, double b, double c, double d)
{
    return mult(act1(a, b, c), d) ;
}

int act2(int a, int b, int c, int d)
{
    return mult(act1(a, b, c), d) ;
}
