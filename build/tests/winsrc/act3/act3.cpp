#include "../act2/act2.h"
#include "../mult/mult.h"

double act3(double a, double b, double c, double d, double e)
{
    return mult(act2(a, b, c, d), 1/e) ;
}

int act3(int a, int b, int c, int d, int e)
{
    return mult(act2(a, b, c, d), 1/e) ;
}
