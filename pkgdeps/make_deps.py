import os, sys

if os.name == 'nt':
    from vks_projects_win import projects
elif os.name == 'posix':
    from vks_projects_astra import projects
else: sys.exit('system not supported yet')


#create convenience package view
new_projects = {}
for p in projects.keys():
    deps = set()
    name = str()
    if not p.endswith('_lib') and not p.endswith('_exe'):
        try:
            name = projects[p]['name']
        except KeyError:
            if p.startswith('dir_'): name = p
            else: continue
    else: name = projects[p]['name']
    if 'depends' in projects[p].keys(): deps = projects[p]['depends']
    new_projects[p] = {'name':name, 'depends':deps}


#
def print_projects(projects):
    print('{')
    for p,val in sorted(projects.items()):
        print("'{}' : {{'name': '{}',\n'depends': {{ {} }}".format(p, val['name'], ', '.join(val['depends'])))
    print('}')


def get_depends(projects, key): #{
    named_projects = {}

    def get_named_projects_key(key):
        #print(key)
        if key.startswith('dir_'): #dir key
            return projects[key]['name'].replace('dir_', '')
        return projects[key]['name']

    def fill_dict(cur_key):
        np_key = get_named_projects_key(cur_key)
        if np_key in named_projects.keys(): return
        if not len(projects[cur_key]['depends']): return named_projects.setdefault(np_key, set())
        for dep_key in projects[cur_key]['depends']:
            if dep_key.startswith('dir_'): #dir key
                fill_dict(dep_key)
                named_projects[np_key] = named_projects.setdefault(np_key, set()) | named_projects.get(get_named_projects_key(dep_key), set())
                #print('here')
                #print(dep_key)
                #print(named_projects)
            else:
                #print('here: ', dep_key, get_named_projects_key(dep_key))
                fill_dict(dep_key)
                named_projects.setdefault(np_key, set()).add(get_named_projects_key(dep_key))
    fill_dict(key)
    return named_projects
#}


import subprocess
def bashexec(cmd):  #{
    pipe = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs = [ str(one, errors='ignore') for one in pipe.stdout.readlines()]
    errs = [str(one, errors='ignore') for one in pipe.stderr.readlines()]
    pipe.communicate()
    return (''.join(outs)).strip(), (''.join(errs)).strip(), pipe.returncode
#}


def create_package_tree(base_project, metaproject, include_dirs, debug_mode): #{
    named_projects = get_depends(new_projects, base_project)
    for p, deps in sorted(named_projects.items()):
        print("'{}': {{ {} }}".format(p, ', '.join(deps)))
    projects = set()
    debug_postfix = ''
    if debug_mode: debug_postfix = 'd'
    for prj, deps in named_projects.items():
        if prj.startswith('src_') or prj == 'src':continue
        projects.add(prj)
        cmd = 'dept cpkg {} -t {}'.format(prj, include_dirs)
        if len(deps): cmd += ' -e '
        for d in deps: cmd += ' {}{} '.format(d, debug_postfix)
        cmd += ' -y '
        if debug_mode: cmd += ' -x {}'.format(debug_postfix)
        #print(cmd.strip())
        outs, _, _ = bashexec(cmd)
        print(outs)
        if '][E]:' in outs: input()
        #print(errs)
    if not metaproject: return
    cmd = 'dept cpkg {} -F -y'.format(metaproject)
    deps = ' -e ' if len(projects) else ''
    for prj in projects: deps += ' {}{} '.format(prj, debug_postfix)
    if debug_mode: cmd += ' -x {}'.format(debug_postfix)
    cmd += deps
    outs, _, _ = bashexec(cmd)
    print(outs)
    if '][E]:' in outs: input()
#}


if __name__ == '__main__':
    debug_mode = True
    include_dirs = ''
    #common
    #if os.name == 'nt': include_dirs = 'C:/projects/all/common/trunk/debug'
    #else: include_dirs = '~/projects/common/debug'
    #create_package_tree(base_project='dir_src_common', metaproject='commonmeta', include_dirs=include_dirs, debug_mode=debug_mode)
    #cunc
    #if os.name == 'nt': include_dirs = 'C:/projects/all/cunc/debug'
    #else: include_dirs = '~/projects/cunc/debug'
    #create_package_tree(base_project='dir_src_cunc', metaproject='cuncmeta', include_dirs=include_dirs, debug_mode=debug_mode)
    #icegis
    #if os.name == 'nt': include_dirs = 'C:/projects/all/icegis/debug'
    #else: include_dirs = '~/projects/icegis/debug'
    #create_package_tree(base_project='dir_src_icegis', metaproject='icegismeta', include_dirs=include_dirs, debug_mode=debug_mode)
    #ssc
    #if os.name == 'nt': include_dirs = 'C:/projects/all/net/debug'
    #else: include_dirs = '~/projects/ssc/debug'
    #create_package_tree(base_project='dir_src_ssc', metaproject='', include_dirs=include_dirs, debug_mode=debug_mode)
    #pg
    #if os.name == 'nt': include_dirs = 'C:/projects/all/db/debug'
    #else: include_dirs = '~/projects/db/debug'
    #reate_package_tree(base_project='dir_src_pg', metaproject='pgmeta', include_dirs=include_dirs, debug_mode=debug_mode)
    #meteo
    #if os.name == 'nt': include_dirs = 'C:/projects/all/meteo/debug'
    #else: include_dirs = '~/projects/meteo/debug'
    #create_package_tree(base_project='dir_src_meteo', metaproject='meteometa', include_dirs=include_dirs, debug_mode=debug_mode)
    #vks
    if os.name == 'nt': include_dirs = 'C:/projects/all/vks/debug'
    else: include_dirs = '~/projects/vks/debug'
    create_package_tree(base_project='dir_src', metaproject='vksmeta', include_dirs=include_dirs, debug_mode=debug_mode)
#}

    
