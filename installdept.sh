#!/bin/bash
dept="/usr/bin/dept"
cd tests/tests && bash runtests.sh
if [ $? -ne 0 ]; then
  echo "tests failed. Cannot install" >&2
  exit
fi
cd -
sudo echo -e "#!/bin/bash\npython3 `pwd`/dept.py \$@" > $dept
sudo chmod +x $dept
