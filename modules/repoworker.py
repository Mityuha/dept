#coding=utf-8

import utils as util
import pack_dealer as pack
import zipper as zip
import log_service as log
from _root_path_ import root_path
import os
import sys
import re
import datetime
from tempfile import TemporaryDirectory
import shutil

dbfile = '.db'
depsfile = '.deps'
logsfile = '.logs'
repos_path = '/home/jack/deptrepos'



#br_path -- branch path
#pname -- package name
#return pname package versions in format: (v1, v2, v3, ...)
def all_package_versions(br_path, pname):#{
  #pdir = os.path.join(repos_path, repo, arch, br, pname)
  pdir = os.path.join(br_path, pname)
  if not os.path.isdir(pdir):
    log.error('No such path %s' % (pdir,))
    return ()
  psubdirs = util.subdirs_list(pdir)
  if psubdirs != []:
    log.warn('Path %s contains subdirectories, check it, please!' % (pdir,))
  plist = util.subfiles_list(pdir)
  versions = [zip.package_version(p) for p in plist]
  return set(versions)
#}


#pname -- package name only
#dbpath -- path to .db file
#br -- branch name
#For server: /repos_path/repo/arch/.db [os.path.join(repos_path, repo, arch, dbfile)]
#return pname package versions in format: (v1, v2, v3, ...) from db file
def all_package_versions_by_file(dbpath, pname, br):#{
  try:
    d = util.load_obj(dbpath)
  except FileNotFoundError:
    log.error('Cannot find database file (%s)' % (dbpath,))
    return set()    
    
  try:
    br_info = d[br] 
  except KeyError:
    log.error('No such branch %s in database file' % (br,))
    return set()
  
  try:
    return set(br_info[pname])
  except KeyError:
    log.warn('No such package %s for branch %s in database file' % (pname, br,))
  return set()
#}

#br_path -- path to branch, e.g. repos_path/repo/arch/branch
#returns dictionary in format: {pname1: [v1, v2, ...], pname2: [v1, v2, ...],}
def scan_branch(br_path): #{
  res = {}
  if not os.path.isdir(br_path): return res
  packages = util.subdirs_list(br_path)
  for p in packages:
    versions = all_package_versions(br_path, p)
    res[p] = versions
  return res
#}


#repo -- repo name in repos_path directory
#arch -- architecture name in repo directory
#br_only -- if specified, branch what database information would be updated for
#else -- for all branches
#updates architecture database file (.db)
def update_archdb_file(repo, arch, br_only=None):#{
  if br_only:
    if not os.path.isdir(os.path.join(repos_path, repo, arch, br_only)):
      log.error('No such path %s. Cannot update archdb file' % (os.path.join(repos_path, repo, arch, br_only),), to_exit=True)
  
  br_path = os.path.join(repos_path, repo, arch)
  branches = util.subdirs_list(br_path) #branches directories
  for_only = br_only in branches
  if for_only:
    branches = [br_only]
    
  d = {}
  for b in branches:
    cur_br = os.path.join(br_path, b)
    pver_dict = scan_branch(cur_br)
    d[b].update(pver_dict)
  #
  if for_only:
    util.update_config_values(br_only, d[br_only], confile=os.path.join(br_path, dbfile))
  else:
    util.save_obj(d, os.path.join(br_path, dbfile))   
  return
#}


#dbpath -- path to .db file
#For server: /repos_path/repo/arch/.db [os.path.join(repos_path, repo, arch, dbfile)]
#shows archdb file (repopath/architecture/.db)
def show_dbfile(dbpath, brname=None):#{
  try:
    d = util.load_obj(dbpath)
  except FileNotFoundError:
    util.save_obj({}, dbpath)
    
  if d is None:
    return
  for k,v in sorted(d.items()):
    if brname and (brname != k):
      continue
    util.dprint('[{}]:'.format(k))
    for p,ver in sorted(v.items()):
      util.dprint('-- {}: {{ {} }}'.format(p, ', '.join(ver)))
  return
#}

  
#zpath - path where zip package locates
#zname - full zip package name (in format [name]_v[ver].zip)
#attr - attribute, which value needs to be received
#return attribute value by attr attribute name
#or attribute dictionary if attr is None (by default)
def dept_package_value(zpath, zname, attr=None):#{
  pname = zip.package_name(zname)
  pver = zip.package_version(zname)
  fileh = zip.zip_package_hfile(zpath, pname, pack.control_file, pver, pack.control_dir)
  attrs = pack.check_control_fileh(fileh, os.path.join(zpath, zname, pack.control_dir, pack.control_file))
  if attr is None:
    return attrs
  if attr in attrs.keys():
    return attrs[attr]
  return None
#}


#brpath -- branch full path, where packages are located (e.g. repos_path/repo/arch/branch) or list of possible package paths, i.e. [path1, path2,...]
#Note: format of brpath may be useful in case you want to get dep_tree from arbitrary path. Simply specify brpath the right way!
#pname -- base package name
#pver -- base package version
#checks all possible variants of package pname:pver paths
#return full path (first met), where package pname:pver are located or None, if pname:pver not exists in enumerated paths
def determine_package_path(brpath, pname, pver):#{
  pfname = zip.full_package_name(pname, pver)
  if type(brpath) == str:#{
    if os.path.exists( os.path.join(brpath, pfname) ):
      return brpath
    elif os.path.exists( os.path.join(brpath, pname, pfname) ):
      return os.path.join(brpath, pname)
    return None
  #}
  elif type(brpath) == list:#{
    for p in brpath:#{
      ppath = determine_package_path(p, pname, pver)#recursion
      if ppath:
        return ppath
    #}
  #}
  return None
#}


#brpath -- branch full path, where packages are located (e.g. repos_path/repo/arch/branch) or list of possible package paths, i.e. [path1, path2,...]
#format of brpath may be useful in case you want to get dep_tree from arbitrary path. Simply specify brpath the right way!
#bpname -- base package name
#bpver -- base package version
#return dependencies tree of package, specified by brpath, pname, pver in format:
#{(bpname, bpver): [(pdname, pdver),...],
#(pdname, pdver): [(pddname, pddver),...],...}
#TODO: check circular dependencies!
def dept_package_dep_tree(brpath, bpname, bpver):#{
  #determines package name by tuple received in result of calling function pack.parse_pdep
  #return tuple of package name and package version or None if error occured or the case is not implemented
  #for example, returning tuple for tuple ('gcc', '<=', '4.8') would be (gcc, 4.8)
  #TODO: '>=' and no sign formats are not implemented yet, i.e. cannot determine the right package for
  #'gcc(>=4.8)' and siply 'gcc'
  def determine_package(tup):#{
    name = tup[0]
    sign = tup[1]
    ver = tup[2]
    if (name is None) or (name == ''):
      log.error('Bad dependency tuple: ', tup)
      return None
    if (sign is None) and (ver is not None):
      #sign = '>='
      sign = '='
    if (sign == '<=' or sign == '=') and (ver is not None):
      return (name.strip(), ver.strip(),)
    elif (sign == '>=') and (ver is not None):
      #TODO
      log.warn('package sign \'>=\' not implemented yet')
      return None
    elif (sign is None) and (ver is None):
      #TODO
      log.warn('packages without explicit version not implemented yet')
      return None
    else: #elif (sign is not None) and (ver is None):
      log.error('Bad dependency tuple: ', tup)
      return None
    return None
  #}
    
  #build list of dependencies
  def recursive_deps(pname, pver, brpath, dict):#{
    ppath = determine_package_path(brpath, pname, pver)
    if not ppath:
      log.fatal('Cannot determine path for package {}:{} with  brpath: {}'.format(pname, pver, brpath), to_exit=True)
    attrs = dept_package_value(ppath, zip.full_package_name(pname, pver))
    dict.update({(pname, pver):[]})
    try:
      depends_lst = attrs['Depends'].split(',')
    except KeyError:#no dependencies
      return
    #no dependencies
    if (len(depends_lst) == 1) and (depends_lst[0] == str()):
      return
    for d in depends_lst:#{
      tup = pack.parse_pdep(d)
      if tup is None:
        log.fatal('Bad dependency package: %s. Building dependencies tree process was terminated!'%(d,), to_exit=True)
      p = determine_package(tup)
      if p is None:
        log.error('Cannot parse dependency package:', tup, '.Building dependencies tree process was terminated!', to_exit=True)
      dict[(pname, pver)].append(p)
      #package already included in process by higher level package
      if p in dict.keys():
        continue
      recursive_deps(p[0], p[1], brpath, dict)
    #}
    return
  #}

  d = {}
  recursive_deps(bpname, bpver, brpath, d)
  return d
#}


#brpath -- branch full path, where packages are located (e.g. repos_path/repo/arch/branch) or list of possible package paths, i.e. [path1, path2,...]
#bpname -- base package name
#bpver -- base package version
#recursive -- flag for recursive dependencies (e.g. p1: p2, p3; p2:p5; for p1 with recursive flag list would be (p2, p3, p5))
#and if recursive==False -- (p2, p3)
#return dependencies set of package, specified by brpath, pname, pver in format:
#[(dpname1, dpver1), (dpname2, dpver2),...(dpnamen,dpvern)]
def dept_package_dep_list(brpath, bpname, bpver, recursive=True):#{
  d = dept_package_dep_tree(brpath, bpname, bpver)
  s = set()
  if recursive:#{
    for k,v in d.items():
      for p in v:
        if (p is not None) and p != '':
          s.add(p)
  #}
  else:
    s = {v for v in d[(bpname, bpver)]}
  return s
#}


#repo -- repo name in repos_path directory
#arch -- architecture name in repo directory
#br -- branch name in architecture directory
#updates branch dependency file (.deps)
def update_brdeps_file(repo, arch, br):#{
  br_path = os.path.join(repos_path, repo, arch, br)
  packages = util.subdirs_list(br_path)
  d = {}
  for p in packages:
    versions = all_package_versions(br_path, p)
    for v in versions:
      d[(p,v)] = dept_package_dep_list(br_path, p, v)
  util.save_obj(d, os.path.join(br_path, depsfile))  
  return
#}


#shows brdeps file
def show_depsfile(br_path):#{
  d = util.load_obj(os.path.join(br_path, depsfile))
  if d is None:
    return
  for k,v in d.items():
    print(k, ':', v)
  return
#}


#repo -- repo name
#arch -- architecture name
#br -- branch name
#pname -- package name what based packages list needs to be estimated for
#pver -- package name version -//-
#returns set in format: [(bpname1,bpver1), (bpname2, bpver2), ...]
def dept_package_base_list(repo, arch, br, pname, pver):#{
  update_brdeps_file(repo, arch, br)
  d = util.load_obj(os.path.join(repos_path, repo, arch, br, depsfile))
  if d is None:
    return set()
  l = [k for k,v in d.items() if (pname, pver) in v]
  return set(l)
#}


#repo -- existed repo name where to create package
#br -- branch name where to create package
#Attempts to create dept package, specified by path ppath
#Dept package writes to poutp
def dept_package_from_pack(ppath, poutp, nofiles_ok=False):#{
  pack.check_dir_struct(ppath, nofiles_ok)
  pattrs = pack.check_control_file(ppath)
  pack.update_sums_file(ppath, nofiles_ok)
  pack.verify_checksums(ppath, nofiles_ok)
  pname = pattrs['Package']
  pver = pattrs['Version']
  parch = pattrs['Architecture']
  #poutp = os.path.join(repos_path, repo, parch, br, pname)
  zip.create_zip_package(ppath, pname, poutp, pver)
  #update_archdb_file(repo, parch)
  #update_brdeps_file(repo, parch, br)
  return True
#}


def create_dept_package(pname, pver, maint, arch, sect, desc, pfiles, outpath, deps=[], prio=None, pscripts={}): #{
  tmpd = TemporaryDirectory()
  if not pack.create_dir_struct(tmpd.name): log.error('Cannot create dept package {}'.format(pname), to_exit=True)
  if not pack.create_control_file(tmpd.name, pname, pver, maint, arch, sect, desc, deps=deps, prio=prio): log.error('Cannot create dept package {}'.format(pname), to_exit=True)
  bin_dir = os.path.join(tmpd.name, pack.bin_dir)
  for f,p,prefix in pfiles:
    to_dir = bin_dir
    if prefix:
      to_dir = os.path.join(bin_dir, prefix)
      try: os.makedirs(to_dir, exist_ok=True)
      except Exception as e: log.error('{}\nCannot create dept package {}'.format(e, pname), to_exit=True)
    shutil.copy2(os.path.join(p,f), os.path.join(to_dir, f))
  script_dir = os.path.join(tmpd.name, pack.script_dir)
  for f,p in pscripts.items(): shutil.copy2(os.path.join(p,f), os.path.join(script_dir, f))
  dept_package_from_pack(tmpd.name, outpath, nofiles_ok=bool(len(pfiles)==0))
  return True
#}
  

if __name__ == '__main__':
  repo = 'deptrepo'
  arch = 'amd64'
  br = 'master'
  create_dept_package()
  #
  #def create_dept_test(ppath):
  #  create_dept_package(ppath, repo, br)
  #
  #create_dept_test(os.path.join(ppath, 'act1'))
  #update_brlogs_file(repo, arch, br, 'dmakarov', 'dmakarov@oaorti.ru', 'package act1 created')
  #create_dept_test(os.path.join(ppath, 'act2'))
  #create_dept_test(os.path.join(ppath, 'act3'))
  #create_dept_test(os.path.join(ppath, 'plus'))
  #create_dept_test(os.path.join(ppath, 'minus'))
  #create_dept_test(os.path.join(ppath, 'mult'))

  #update_archdb_file(repo, arch, br)
  #show_dbfile(repo, arch)
  #brpath_fake = os.path.join(repo, arch, br)
  #show_brdeps_file(brpath_fake, '', '')
  #print('*'*20)
  #brpath = os.path.join(repos_path, repo, arch, br)
  #d = dept_package_base_list(repo, arch, br, 'plus', '1.0')
  #d = dept_package_dep_list(os.path.join(repos_path, repo, arch, br), 'act3', '1.0')
  #for k,v in d.items():
  #for i in d:
  #  print(i)
  
