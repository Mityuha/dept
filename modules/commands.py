#coding=utf-8

import utils as util
import os
import platform
from _root_path_ import root_path
import log_service as log
import zipper
import pack_dealer as pack
import netman
import repo_service as reposrv
import repoworker as repowrk
import pkgchanges as changes
import pkgversion as versions
from install import install_dept_package
from remove import remove_dept_package
import upload as up
import shutil

not_implemented_message = """
###############################################################
########### Hello! Thank you for your interest in    ##########
########### our build artefacts control system!      ##########
########### This functionality is not implemented.   ##########
########### Yet! So if you really need this function ##########
########### please, mail us! We'll do our best!      ##########
###############################################################
"""


#a_ -- argument
a_action = 'action'
a_instance = 'instance'
a_username = 'uname'
a_usermail = 'umail'
a_reponame = 'reponame'
a_repoaddr = 'repoaddr'
a_repopath = 'repopath'
a_brname = 'brname'
a_arch = 'arch'
a_update = 'update'
a_localpath = 'local_paths'
a_all = 'all'
#
a_pname = 'pname'
a_pver = 'pver'
a_single = 'single'
a_force = 'force'
a_yes = 'yes'
#
a_notime = 'notime'
a_upchange = 'upchange'
a_install = 'install'
#
a_mainpkgs = 'mainpkgs'
a_treeview = 'treeview'
a_directory = 'pkgdir'
a_file = 'pkgfile'
a_installed = 'installed'
a_depended = 'depended'
a_based = 'based'
a_files = 'files'
a_versions = 'versions'
a_logs = 'logs'
a_lines = 'lines'
a_desc = 'description'
a_section = 'section'
a_priority = 'priority'
a_scripts = 'scriptspath'
a_outpath = 'outpath'
a_nofiles = 'no_files'
a_changes = 'changes'
a_postfix = 'postfix'


def define_local_platform():
  architecture = platform.architecture()
  if architecture and len(architecture[0]):
    bits = architecture[0][0:2]
    if int(bits) not in [32, 64]: return '{}, {}'.format(os.name, architecture[0])
    if os.name == 'nt': return 'win{0}'.format(bits)
    elif os.name == 'posix': return 'i386' if int(bits) == int(32) else 'amd64'
    else: return '{}, {}'.format(os.name, architecture[0])
  else:
    return os.name


@util.python3_checker
def init(args):#{
#  print('Just printing args: ', args)
  args = vars(args)
  path = os.path.abspath(args[a_localpath])
  arch = define_local_platform() 
  if arch not in pack.possible_arches():
    log.error('Can not initialize dept repository. Your system architecure ({}) is not supported yet'.format(arch), to_exit=True)
  uname = args[a_username]
  umail = args[a_usermail]
  rname = args[a_reponame]
  raddr = args[a_repoaddr]
  if raddr:
    raddr = netman.parse_hostaddr(raddr)
    if not raddr: return
  rpath = args[a_repopath]
  brname = args[a_brname]
  to_update = args[a_update]
  reposrv.initrepo(arch=arch, brname=brname, path=path, rname=rname, raddr=raddr, rpath=rpath, uname=uname, umail=umail)
  if to_update:
    netman.update()
  return
#}


@util.python3_checker
def config(args):#{
  args = vars(args)
  v_uname = args[a_username]
  v_umail = args[a_usermail]
  v_rname = args[a_reponame]
  v_raddr = args[a_repoaddr]
  v_rpath = args[a_repopath]
  v_brname = args[a_brname]
  if args[a_action] == 'show': #{
    v_arch = None
    if args[a_arch]:  v_arch = define_local_platform()
    sh_all = args[a_all]
    if sh_all or not any((v_arch, v_brname, v_rname, v_raddr, v_umail, v_uname, v_rpath)):
      reposrv.dept_info()
      reposrv.user_info()
    else:
      if v_arch:
        print('Architecture: {}'.format(reposrv.current_arch_name()))
      if v_brname:
        print('Remote branch: {}'.format(reposrv.current_br_name()))
      if v_rname:
        print('Remote repo name: {}'.format(reposrv.current_repo_name()))
      if v_raddr:
        print('Remote repo address: {}'.format(reposrv.remote_repo_address()))
      if v_rpath:
        print('Remote repo path: {}'.format(reposrv.remote_repo_path()))
      if v_umail:
        print('User mail: {}'.format(reposrv.get_user_mail()))
      if v_uname:
        print('User name: {}'.format(reposrv.get_user_name()))
  #}
  elif args[a_action] == 'set': #{
    if v_rname:
      reposrv.set_current_repo_name(v_rname)
    if v_raddr:
      tmp = netman.parse_hostaddr(v_raddr)
      if not tmp: return
      addr, port, user, passwd = tmp
      reposrv.set_current_repo_config(addr, port, user, passwd)
    if v_rpath:
      reposrv.set_remote_repo_path(v_rpath)
    if v_brname:
      reposrv.set_current_br_name(v_brname)
      log.warn("Clearing all changes and versions ...")
      changes.clear_all_changes()
      versions.clear_versions()
    if v_uname:
      util.update_config_values(section=util.user_section, values={util.user_name:v_uname})
    if v_umail:
      util.update_config_values(section=util.user_section, values={util.user_mail:v_umail})
  #}
  return
#}
  


#install package
@util.python3_checker
@reposrv.check_remotes
def install(args):#{
  repo = reposrv.current_repo_name()
  br = reposrv.current_br_name()
  arch = reposrv.current_arch_name()
  args = vars(args)
  v_pname = args[a_pname]
  v_pver = args[a_pver]
  with_deps = not args[a_single]
  v_force = args[a_force]
  v_yes = args[a_yes]
  v_nofiles = args[a_nofiles]
  v_pver = reposrv.check_package_listed(v_pname, v_pver)
  if not v_pver:
    log.error('Cannot install package: {}. Try to update local repository typing\n# dept update\n\
and get list of all package versions typing\n# dept packages versions {}'.format(v_pname, v_pname), to_exit=True)
  if with_deps:
    packages = reposrv.package_deps_from_db(repo, br, v_pname, v_pver)    
    if packages is None:
      log.error('Cannot install package {}:{}'.format(v_pname, v_pver), to_exit=True)
  else:#single
    packages = {(v_pname, v_pver)}
  packages_to_down = {(repo,br,pn,pv) for pn,pv in packages if not reposrv.check_package_exists(repo, br, pn, pv)}
  if len(packages_to_down):#{
    tmpdir = netman.download_packages(packages_to_down)
    if not tmpdir:
      log.error('Cannot download packages', to_exit=True)
    #copy packages from tmpdir to local repo    
    reposrv.copy_packages_to_local_repo(packages_to_down, tmpdir.name)
  #}
  install_dept_package(repo=repo, br=br, pname=zipper.full_package_name(v_pname, v_pver), with_deps=with_deps, force=v_force, assumeyes=v_yes, nofiles=v_nofiles)
  return
#}


@util.python3_checker
def remove(args):#{
  repo = reposrv.current_repo_name()
  br = reposrv.current_br_name()
  args = vars(args)
  v_pname = args[a_pname]
  v_force = args[a_force]
  with_deps = not args[a_single]
  pver = None
  for (_,_,pn,pv) in reposrv.get_all_installed_packages():#{
    if pn == v_pname:
      pver = pv
      break
  #}
  if not pver:
    log.warn('It seems that package "{}" is not installed yet'.format(v_pname), to_exit=True)
  remove_dept_package(repo=repo, br=br, pname=zipper.full_package_name(v_pname, pver), with_deps=with_deps, force=v_force)
  return
#}


@util.python3_checker
def upload(args):#{
  if not reposrv.local_repo_has_lastver():
    util.dprint('Remote repository has updated. To upload you need first update local repo. Type:')
    util.dprint('dept update')
    return
  args = vars(args)
  v_with_lmts = not args[a_notime]
  v_upchange = args[a_upchange]
  br = reposrv.current_br_name()
  #
  if v_upchange: #{apply bins change for all install packages
    for pkg_tup in reposrv.get_all_installed_packages():
      changes.add_change(pkg_tup, changes.act_update, changes.inst_package)
  #}
  changed_packages = up.get_really_changed_packages()
  tmpdir = up.prepare_changes_for_uploading(changed_packages, with_lmts=v_with_lmts)
  if not tmpdir:
    return

  if not all( (up.update_dbfile(tmpdir.name), up.update_depsfile(tmpdir.name),) ):
    return

  try:
    netman.upload_packages(tmpdir)
  except Exception as e:
    log.error(e, to_exit=True)

  new_packages = [(pkg[0],pkg[1],pkg[2],pnewver) for pkg,pnewver in versions.packages_new_versions().items()]

  changes.clear_all_changes()
  versions.clear_versions()

  reposrv.copy_packages_to_local_repo(new_packages, tmpdir.name, copy_other_files=True)

  #install new packages by default
  for repo,br,pn,pv in new_packages:
    install_dept_package(repo=repo, br=br, pname=zipper.full_package_name(pn, pv), with_deps=True, force=False)
  #}
  return True
#}


@util.python3_checker
def change(args):#{
  print(not_implemented_message)
  return
  #print('Just printing args: ', args)
#}


@util.python3_checker
def create(args):#{
  print(not_implemented_message)
  return
  #print('Just printing args: ', args)
#}


@util.python3_checker
def checkup(args):#{
  print(not_implemented_message)
  return
  #print('Just printing args: ', args)
#}


@util.python3_checker
@reposrv.check_remotes
def update(args):#{
  netman.update()
  return
#}


@util.python3_checker
def packages_info(args):#{
  #print('Just printing args: ', args)
  def check_pname_pver(pname, pver):#{
    if pver and (not reposrv.check_package_listed(pname, pver)):
      log.error('Package {} version {} not found. Try to update local repository typing\n#dept update\nand get list of all package versions typing\n#dept packages versions {}'.format(pname, pver, pname), to_exit=True) 
    if not pver:
      pkg = reposrv.check_package_installed(repo, br, pname, pver, by_name=True)
      if not pkg:
        log.error('Cannot determine package "{}". Try to specify package version'.format(pname), to_exit=True)
      pver = pkg[3]#(repo,br,pn,pv)
    return pname,pver
  #}
  args = vars(args)
  v_instance = args[a_instance]
  if v_instance == a_installed:#{#installed
    v_mainpkgs = args[a_mainpkgs]
    if not v_mainpkgs:#{
      title = 'List of packages installed:'
      packages = reposrv.get_all_installed_packages()
    else:
      title = 'List of main packages installed:'
      packages = reposrv.get_main_installed_packages()
    #}
    util.dprint(title)
    for repo,br,pn,pv in sorted(packages):
      util.dprint('-- {}:{} ({}:{})'.format(pn,pv,repo,br))
    return
  #}installed
  #
  repo = reposrv.current_repo_name()
  br = reposrv.current_br_name()
  if v_instance == a_depended:#{#depended
    asdict = args[a_treeview]
    pname, pver = check_pname_pver(args[a_pname], args[a_pver])
    #
    packages = reposrv.package_deps_from_db(repo, br, pname, pver, asdict=asdict)
    util.dprint('Package [{} : {}] dependencies:'.format(pname, pver))
    if not asdict:#{#simply set
      for pn,pv in sorted(packages):
        if not ((pn,pv) == (pname,pver)):
          util.dprint('-- {} : {}'.format(pn,pv))
    #}
    else:#{
      util.print_dict_as_tree(packages, (pname,pver))
      return
    #}
  #}#depended
  elif v_instance == a_based:#{#based
    pname, pver = check_pname_pver(args[a_pname], args[a_pver])
    #
    packages = reposrv.based_packages_from_db(repo, br, pname, pver)
    util.dprint('Package [{} : {}] based packages:'.format(pname, pver))
    for pn,pv in sorted(packages):
      if not ((pn,pv) == (pname,pver)):
        util.dprint('-- {} : {}'.format(pn,pv))
  #}#based
  elif v_instance == a_files:#{#files
    pname = args[a_pname]
    pver = args[a_pver]
    pdir = args[a_directory]
    if not pver:
      pkg = reposrv.check_package_installed(repo, br, pname, pver, by_name=True)
      if not pkg:
        log.warn('To show package {} files, package should be installed. Try to specify package version'.format(pname), to_exit=True)
      pver = pkg[3]
 
    if not reposrv.check_package_exists(repo, br, pname, pver):
      log.warn('To show package {}:{} files, package should be at least one time installed'.format(pname, pver), to_exit=True)
    files = reposrv.package_files(repo, br, pname, pver, pdir)
    if not len(files):
      return
    title_str = 'Package [{} : {}] files'.format(pname, pver)
    if pdir:
      title_str += ' in {} directory'.format(pdir)
    title_str += ':'
    util.dprint(title_str)
    for f in files:
      util.dprint('-- {}'.format(f))
  #}#files
  elif v_instance == a_versions:#{#versions
    pname = args[a_pname]
    versions = reposrv.package_versions_by_file(repo, br, pname)
    util.dprint('Package "{}" versions: {{ {} }}'.format(pname, ', '.join(versions)))
    return
  #}#versions
  elif v_instance == a_all: #{#all
    brname = args[a_brname]
    reposrv.show_all_packages(repo, brname)
  #}#all  
  elif v_instance == a_logs: #{#logs
    v_entries = args[a_lines]
    reposrv.show_branch_logs(reposrv.get_logsfile_path(repo, br), v_entries)
  #}#logs
  elif v_instance == a_changes: #{#changes
    changes.show_changes()
  return
#}

import re

@util.python3_checker
@reposrv.auth_checker()
@reposrv.isdept
def create_package(args): #{
  args = vars(args)
  v_pname = args[a_pname]
  v_pver = args[a_pver]
  v_depends = args[a_depended]
  v_desc = args[a_desc]
  v_section = args[a_section]
  v_prio = args[a_priority]
  v_paths = args[a_localpath]
  v_files = args[a_files]
  v_nofiles = args[a_nofiles]
  #v_outpath = args[a_outpath]
  v_user = args[a_username]
  v_mail = args[a_usermail]
  v_scrpath = args[a_scripts]
  v_yes = args[a_yes]
  v_postfix = args[a_postfix]

  if v_depends is None: v_depends = []
  if v_files is None: v_files = []
  if v_paths is None: v_paths = ['.']
  if v_pver is None: v_pver='1.0'
  if v_section is None: v_section = 'devel'
  if v_prio is None: v_prio = 'optional'
  if v_scrpath is None: v_scrpath = '.'

  if v_postfix: v_pname += v_postfix

  #package name test
  if reposrv.check_package_listed(v_pname):
    log.notice('Package {} already exists. To update package version apply some changes to package'.format(v_pname))
    return False

  #versions test
  if not versions.version_is_correct(v_pver):
    log.error('Version {} is incorrect. Version must be reachable with version increment = {}'.format(v_pver, versions.ver_inc))
    return False
  if all((v_files, v_nofiles)): log.notice('-f option would be omitted')
  #depends test
  depends_on = [] #list of (pname,pver) pairs
  for p in v_depends: #{
    l = p.split(':')
    pname = pver = None
    if any((len(l)==2, len(l)==1)):
      pname = l[0]
    else: 
      log.error('Bad package format: {}. Type:\n#dept cpkg -h\nto see command help message'.format(p))
      return False
    pver = reposrv.check_package_listed(pname, pver)
    if not pver:
      log.notice("Dept knows nothing about package '{}'. Version 1.0 assumed".format(pname))
      pver = '1.0'
    depends_on.append((pname, pver,))
  #}description
  if not v_desc: v_desc = "Package '{}', software tools".format(v_pname)
  #section
  if v_section not in pack.possible_sections(): 
    log.error('Unknown section: {}.\nPossible sections: {}'.format(v_section, ', '.join(pack.possible_sections())))
    return False
  #prio
  if v_prio not in pack.possible_prios():
    log.error('Unknown priority: {}.\nPossible priorities: {}'.format(v_prio, ', '.join(pack.possible_prios())))
    return False
  #paths
  paths = set()
  for p in v_paths:
    if not os.path.isdir(os.path.realpath(p)): 
      log.error('No such directory: {}. You should specify existing directory paths.'.format(p))
      return False
    paths.add(os.path.realpath(p))
  paths = list(paths)
  paths.append(util.curpath)
  #
  package_files = set() #format: set{(pkgfile, path, prefix), }
  #files
  if v_nofiles: util.dprint('Package would be created without binary files')
  else: #with files{
    if not v_files: v_files.append(args[a_pname])
    #print(v_files)
    for f in v_files:
      f_found = False
      bf = os.path.basename(f)
      bfprefix = os.path.dirname(f)
      #name_re = re.compile('.*((?i){}).*'.format(bf))
     # print(bf)
      for p in paths: #{
        #files_lst = util.subfiles_list(p)
        #files_lst = util.recursive_glob(p, bf)
        #flst = list(filter(name_re.match, files_lst))
        #pattern = '*{}*'.format(bf)
        pattern = '(?:lib)?(?:(?i){})(?:\..*)?'.format(bf)
        flst = util.recursive_glob(p, pattern, recursive=(p != util.curpath))
        if flst:
          for absfpath in flst:
            if os.path.islink(absfpath) or not os.path.isfile(absfpath): continue
            fprefix = os.path.join( os.path.dirname(os.path.relpath(absfpath, p)), bfprefix)
            fname = os.path.basename(absfpath)
            fpath = os.path.dirname(absfpath)
            package_files.add((fname, fpath, fprefix))
            f_found = True #test on symlinks (!)
          break
    #}
      if not f_found: 
        log.error('Cannot find matching files for "{}" regex. Search paths: {}'.format(f, ', '.join(paths)))
        return False
    util.dprint('Package {} files:'.format(v_pname))
    for f,p,prefix in package_files: util.dprint('File: {}, path: {}{}'.format(f,p, (', prefix: {}'.format(prefix) if prefix else '') ))
    if not v_yes and not util.ask_yes_no_question('Warning: files extensions filter not implemented yet!\nFiles list correct'):
      log.notice('Rename your package or change package files pattern (-f option)')
      return False
  #}
  #output path
  v_outpath = os.path.join(os.path.dirname(reposrv.get_depsfile_path(reposrv.current_repo_name(), reposrv.current_br_name())), v_pname)
  v_outpath = os.path.realpath(v_outpath)
  try: os.makedirs(v_outpath, exist_ok=True)
  except Exception as e: 
    log.error('{}\nError while creating output package directory "{}"'.format(e,v_outpath))
    return False
  else: log.notice('Output package directory "{}" is created'.format(v_outpath))
  #script path
  if not os.path.isdir(os.path.realpath(v_scrpath)): 
    log.error('Script directory "{}" is not exist.'.format(v_scrpath))
    return False
  v_scrpath = os.path.realpath(v_scrpath)
  #user and mail
  if not v_user: v_user = reposrv.get_user_name()
  if not v_mail: v_mail = reposrv.get_user_mail()
  if any((not v_user, not v_mail,)):
    log.error('Either user name or user mail is not specified')
    return False
  util.dprint('Maintainer: {} <{}>'.format(v_user, v_mail))
  if not v_yes and not util.ask_yes_no_question('Package maintainer info correct'): return False

  if len(depends_on): util.dprint('Depends on:')
  for p, v in depends_on: util.dprint('{}: {}'.format(p, v))

  #create package
  if not repowrk.create_dept_package(pname=v_pname, pver=v_pver, maint=pack.format_maintainer(v_user,v_mail), arch=reposrv.current_arch_name(),
                                     sect=v_section, desc=v_desc, pfiles=package_files, outpath=v_outpath, deps=depends_on, prio=v_prio, pscripts={}): 
    return False

  #append create package pname:pver change
  pkg_tup = (reposrv.current_repo_name(), reposrv.current_br_name(), v_pname, v_pver,)
  changes.add_change(pkg_tup, changes.act_create, changes.inst_package, args={changes.arg_nofiles:v_nofiles})
  changes.add_change(pkg_tup, changes.act_alter, changes.inst_package, args={changes.arg_pver:v_pver, changes.arg_nofiles:v_nofiles})
  return True
#}





if __name__ == '__main__':
  pass

