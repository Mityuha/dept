#coding=utf-8

import os, sys, zipfile
import tempfile
import shutil
import re
import log_service as log
import time
from utils import toposixpath
import posixpath

p_pattern = re.compile('\s*([\w-]+)_v(\w+\.\w+)\.zip')


def is_package_name(full_pname):#{
  lst = p_pattern.findall(full_pname)
  if len(lst) != 1 or len(lst[0]) != 2:
    return False
  return True
#}

def full_package_name(package_name, package_version):#{
  if (package_name == str()) or package_name is None:
    log.error('Cannot get full package name. Name is not specified', to_exit=True)
  if is_package_name(package_name):
    return package_name
  if (package_version == str()) or (package_version is None):
    #try:
    #  raise Exception
    #except Exception:
    #  log.process_exception(to_exit=False)
    log.error('Cannot get full package name for package %s. Version is not specified' % (package_name), to_exit=True)
  return package_name+'_v'+package_version+'.zip'
#}

p_format_mes = """Package name format: [package_name]_v[version].zip"""


def parse_package_name(full_pname):
  lst = p_pattern.findall(full_pname)
  if len(lst) != 1 or len(lst[0]) != 2:
    log.error('Bad package name {}.\n{}'.format(full_pname, p_format_mes), to_exit=True)
  return lst[0]


def package_version(full_pname):
  return parse_package_name(full_pname)[1]
  

def package_name(full_pname):
  return parse_package_name(full_pname)[0]

#check zip package for correction
def check_zip_package(ziph):
  try:
    res = ziph.testzip()
  except (AttributeError, RuntimeError):
    log.process_exception()
  else:
    return res

#returns ziph by path, name and mode
def get_zip_handle(path, name, mode):
  if mode == 'w':
    if not os.path.exists(path):
      os.makedirs(path, exist_ok=True)
  try:
    if '.zip' in name:
      name = name.split('.zip')[0]
    abszpath = os.path.join(path, (name + '.zip'))
    #if mode == 'r' or mode == 'a':
    #  if not zipfile.is_zipfile(abszpath):
    #    log.fatal('File %s not in zip format' % (abszpath,), to_exit=True)
    zip_handle = zipfile.ZipFile(abszpath, mode, zipfile.ZIP_DEFLATED)
  except Exception:
    log.process_exception()
  else:
    res = check_zip_package(zip_handle)
    if res:
      log.fatal("Archive '{}'.zip is bad. The first bad file in archive: {}".format(abszpath, res), to_exit=True)
    return zip_handle


#It's expected, that ziph is the first argument
def ziph_checker(func):
  def wrap(*args, **kwargs):
    if len(args) > 0:
      res = check_zip_package(args[0])
      if res is not None:
        log.fatal("Function '{}': Cannot process zip handle. The first bad file in archive: {}".format(func.__name__,res), to_exit=True)
      return func(*args, **kwargs)
  return wrap
    

#return list of all file names in archive
#or list of all file names in archive folder by path if specified
#in format: ['somepath/file1', 'somepath/file2', ...], 
#where somepath is relative path to 'path' specified
@ziph_checker
def zip_package_namelist(ziph, path=None):
  lst = ziph.namelist()
  if not path:
    return lst
  path = toposixpath(os.path.normpath(path))
  files = []
  for f in lst:
    fname = os.path.basename(f)
    dirname = os.path.dirname(f)
    if dirname.startswith(path) and fname:
      files.append(toposixpath(os.path.relpath(f, path)))
    #t = os.path.split(f)
    #if (len(t) == 2) and t[0].startswith(path) and t[1]:
  return files

#return list of all package directories
@ziph_checker
def zip_package_dirslist(ziph):
  lst = zip_package_namelist(ziph)
  dirs = set()
  for f in lst:
    d = os.path.dirname(f)
    if d:
      dirs.add(d)
  return dirs
      
#create a zip archive by path
@ziph_checker
def zipdir(ziph, path): #{
  # ziph is zipfile handle
  for root, dirs, files in os.walk(path):
    for file in dirs + files:
     #print(file)
      arcname = toposixpath(os.path.relpath(os.path.join(root, file), os.path.join(path, '.')))
      ziph.write(os.path.join(root, file), arcname)
#}


def preserve_attributes(path, info): #{
  perm = info.external_attr >> 16
  date_time = time.mktime(info.date_time + (0, 0, -1))
  os.chmod(path, perm)
  os.utime(path, (date_time, date_time))  
#}


def extractall(ziph, extract_dir): #{
  for info in ziph.infolist():
    ziph.extract( info.filename, path=extract_dir )
    out_path = os.path.join(extract_dir, info.filename )
    preserve_attributes(out_path, info)
#}


#if entire_dir is specified, members are ignored
#if not entire_dir and not members, call extractall (see above)
#else if members specified, trying extract specified members
@ziph_checker
def unzipdir(ziph, spath, members=None, entire_dir='', pwd=None): #{
  #if path not exists - create it
  path = os.path.abspath(spath)
  if not os.path.exists(path):
    os.makedirs(path)

  def unzip_files(ziph, outpath, files): #{
    lst = zip_package_namelist(ziph)
    #All slashes MUST be forward slashes '/' as opposed to
    #backwards slashes '\' for compatibility with Amiga
    #and UNIX file systems etc.  If input came from standard
    #input, there is no file name field.
	#https://pkware.cachefly.net/webdocs/casestudies/APPNOTE.TXT
    files = [toposixpath(f) for f in files]
    for member in files:
      if member not in lst: log.error("Cannot extract file '{}'. No such file in archive. Possible values: {}".format(member, ', '.join(lst)), to_exit=True)
      output_path = outpath
      filename = os.path.basename(member)

      if not filename: #directory
        os.makedirs(os.path.join(output_path, member), exist_ok=True)
        continue

      relpath = os.path.relpath(member, entire_dir)
      if filename != relpath: #create all intermediate directories (for bin/plugins/sql/some.lib it is plugins/sql)
        output_path = os.path.join(output_path, os.path.dirname(relpath))
        if not os.path.exists(output_path):
          os.makedirs(output_path, exist_ok=True)

      source = ziph.open(member)
      #print(member)
      #input(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(date_time)))
      target_path = os.path.join(output_path, filename)
      if os.path.exists(target_path):
        log.warn('File {} already exists. It would be overwritten'.format(target_path))
      target = open(target_path, 'wb')
      with source, target:
        shutil.copyfileobj(source, target)
      #
      preserve_attributes(target_path, info=ziph.getinfo(member))
  #}

  if entire_dir: #all files from entire_dir #{
    npath = toposixpath(os.path.normpath(entire_dir))
    if npath not in zip_package_dirslist(ziph):
      log.error("Path '{}' not exists in archive".format(entire_dir), to_exit=True)
    #get all files in archive, specified by path 
    files = [toposixpath(os.path.join(entire_dir, f)) for f in zip_package_namelist(ziph, entire_dir)]
    unzip_files(ziph, path, files)
  #}
  else: #for any members
    if not members: extractall(ziph, path)
    else: unzip_files(ziph, path, members)
#}


#######
#High-level functions
#######

#if pver specified, pname is considered as package name
#if pver is None, pname is considered as full package name
#if in_dir is None, returns all files list in package including dirs
#if in_dir is specified, return all files list in directory in_dir
#in format: ['somepath/file1', 'somepath/file2', ...], 
#where somepath is relative path to 'in_dir' directory if specified
def zip_package_files(ppath, pname, pver=str(), in_dir=str()):
  ziph = get_zip_handle(ppath, full_package_name(pname, pver), 'r')
  return zip_package_namelist(ziph, in_dir)


#if pver specified, pname is considered as package name
#if pver is None, pname is considered as full package name
def zip_package_dirs(ppath, pname, pver=str()):
  ziph = get_zip_handle(ppath, full_package_name(pname, pver), 'r')
  return zip_package_dirslist(ziph)
  
  
def choose_zip_file(ziph, fname, dir=None): #{
  lst = zip_package_namelist(ziph)
  if dir:
    fname_check = lambda f: toposixpath(os.path.join(dir,fname)) == posixpath.normpath(toposixpath(f))
  else:
    fname_check = lambda f: (posixpath.normpath(toposixpath(fname)) == os.path.basename(toposixpath(f)))# or (fname == f)
  for f in lst:
    if fname_check(f): return f
  return None
#}

#ppath -- package full pathh
#pname -- package name 
#fname -- file name what zip handle returns for
#pver -- package version
#dir -- directory within ppath, where needs to be search for
#return zip package file handler of file fname within directory dir if specified,
#or the first occurance of file fname
#If there is no such file in any package dir, returns None
def zip_package_hfile(ppath, pname, fname, pver=str(), dir=None): #{
  ziph = get_zip_handle(ppath, full_package_name(pname, pver), 'r')
  zip_fname = choose_zip_file(ziph, fname, dir)
  if zip_fname: return ziph.open(zip_fname)
  log.error("Cannot get file handle for package '{}'\nfile:'{}'\npver: {}\ndir: '{}'\nAvailable files: {}".format(os.path.join(ppath, pname), fname, pver, dir, ', '.join(lst)),
  to_exit=True)
  #return None
#}


#returns file info (ZipInfo Object)
def zip_package_file_info(ppath, pname, fname, pver=str(), dir=None):
  ziph = get_zip_handle(ppath, full_package_name(pname, pver), 'r')
  zip_fname = choose_zip_file(ziph, fname, dir)
  if zip_fname: return ziph.getinfo(zip_fname)
  log.error("Cannot get file handle for package '{}'\nfile:'{}'\npver: {}\ndir: '{}'\nAvailable files: {}".format(os.path.join(ppath, pname), fname, pver, dir, ', '.join(lst)),
  to_exit=True)
  
  
from datetime import datetime  
#returns datetime object
def zip_file_lmts(ppath, pname, fname, pver=str(), dir=None): #{
  info = zip_package_file_info(ppath, pname, fname, pver, dir)
  return datetime.fromtimestamp(time.mktime(info.date_time + (0, 0, -1)))
#}
  


#creates zip package
#if pver specified, pname is considered as package name
#if pver is None, pname is considered as full package name
def create_zip_package(ppath, pname, outpath='.', pver=str() ):
  ziph = get_zip_handle(outpath, full_package_name(pname, pver), 'w')
  zipdir(ziph, ppath)
  ziph.close()
  ziph = get_zip_handle(outpath, full_package_name(pname, pver), 'r')
  res = check_zip_package(ziph)
  if res:
    log.error("Archive '{}'.zip is bad. The first bad file in archive: {}".format(pname, res), to_exit=True)
  ziph.close()


#zpath - absolute path to zip package
#zname - zip package full name
#outpath - path to extract to
#if entire_dir is specified, members are ignored
#if entire_dir == '' or None and members == None or [], call ZipFile.extractall(path)
#else if members specified, trying extract specified members
def extract_zip_package(zpath, zname, outpath=os.curdir, members=None, entire_dir=''):
  ziph = get_zip_handle(zpath, zname, 'r')
  unzipdir(ziph, outpath, members, entire_dir)
  ziph.close()






if __name__ == '__main__':
  #create_zip_package('/home/jack/dept_tests/plus_package', 'plus', '/home/jack/dept/packages', '1.0')
  #extract_zip_package('/home/jack/zipper', 'plus_v1.0.zip', '../dept/packages/tmp')
  #tmp = update_zip_package('/home/jack/zipper', 'plus', '1.0', 'scripts', '/home/jack/debug')
  #print(tmp.name)
  ppath = '/home/jack/dept/packages'
  pname = 'plus_v2.0.zip'
  #ziph = get_zip_handle(ppath, pname, 'r')
  ziph = None
  print(zip_package_namelist(ziph))
  #time.sleep(20)
  #lst = []
  #fh = zip_package_hfile('/home/jack/zipper', 'plus', '1.0', 'control')
  #if fh is not None:
  #  for l in fh.readlines():
  #    if b'Version' in l:
  #      lst.append('Version : 1.1')
  #    else:
  #      lst.append(l)





