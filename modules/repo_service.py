#coding=utf-8

import os
import log_service as log
import utils as util
import zipper
import pack_dealer as pack
import repoworker as repowrk
from tempfile import TemporaryDirectory
#import netman
import shutil
import netman
from collections import OrderedDict
from datetime import datetime

#dept dir name
ddirname='.dept'
objects_dir = 'objects'
upload_dir = 'upload'
up_dir = 'up'
inst_file = '.installed'
mainst_file = '.mainstalled'
#########
config_file = '.config'
configf_remote = 'remote'
configf_remote_ip = 'raddress'
configf_remote_port = 'rport'
configf_remote_user = 'ruser'
configf_remote_passwd = 'rpassword'
configf_remote_repo = 'rrepo'
configf_remote_path = 'rpath'
configf_remote_br = 'rbrabch'
configf_remote_arch = 'rarchitecture'
#########
#dest_file = '.destination'
#destination file fields:
#remote architecture name
#destf_arch = 'arch'
#destf_repo = 'reponame'
#destf_br = 'brname'
#list of packages to upload to. Format: [(repo, br, pname, pver)]
#destf_pkgslist = 'packages'
#action for packages. Possible values: {'upload', 'delete', 'download'}
#destf_action = 'action'
#destf_action_up = 'upload'
#destf_action_del = 'delete'
#destf_action_down = 'download'
#number of directory within foruploading directory
#destf_commithash = 'commithash'
#


#initialize an empty dept repo
def initrepo(arch, brname='master', path=None, rname=None, raddr=None, rpath=None, umail=None, uname=None):#{
  util_curpath = util.curpath #save
  if path and path != util.curpath: log.notice("New local dept path '{}' would be created if not exists".format(path))
  else: path = util.curpath
  dept_path = os.path.join(path, ddirname)
  #definitions
  dept_objects = os.path.join(dept_path, objects_dir)
  dept_uploads = os.path.join(dept_path, upload_dir)
  dept_instf = os.path.join(dept_path, inst_file)
  dept_mainstf = os.path.join(dept_path, mainst_file)
  dept_conff = os.path.join(dept_path, config_file)
  #initializations
  os.makedirs(dept_objects, exist_ok=True)
  os.makedirs(dept_uploads, exist_ok=True)
  if path != util.curpath: util.curpath = path
  util.save_obj([], dept_instf)
  util.save_obj([], dept_mainstf)
  util.save_obj({}, dept_conff)
  util.update_config_values(section=configf_remote, values={configf_remote_arch:arch, configf_remote_br:brname, configf_remote_repo:rname, configf_remote_path:rpath}, confile=dept_conff)
  if raddr: set_current_repo_config(addr=raddr[0], port=raddr[1], user=raddr[2], passwd=raddr[3])
  util.update_config_values(section=util.user_section, values={util.user_name:uname, util.user_mail:umail}, confile=dept_conff)
  util.curpath = util_curpath #restore
  return
#}


#if there is dept repository
#return absolutely path of .dept directory (abspath/.dept)
#whereever curpath is pointed 
#else exits
def find_dept_path():#{
  dpath = util.curpath
  while not util.path_is_root(dpath):
    if not os.path.isdir(os.path.join(dpath, ddirname)):
      dpath = util.parent_path(dpath)
    else:
      return os.path.join(dpath, ddirname)
  log.error('{} is not a dept repository'.format(util.curpath), to_exit=True)
#}


#check if cwd contains .dept directory
def isdept(func):#{
  def wrap(*args, **kwargs):
    #TODO:
    def check_dept_struct(dpath):
      return os.path.isdir(os.path.join(dpath, objects_dir)) and os.path.isdir(os.path.join(dpath, upload_dir))
    #
    dpath = find_dept_path()
    if not check_dept_struct(dpath):
      log.error('Dept repository {} is corrupted. Create new repository'.format(dpath), to_exit=True)
    return func(*args, **kwargs)
  return wrap
#}


#retutns absolute path to package, specified by repo, br, pname
#pname -- full package name in format: pname_v{pver}.zip
@isdept
def get_package_path(repo, br, pname):#{
  deptpath = find_dept_path()  
  return os.path.join(deptpath, objects_dir, repo, br, zipper.package_name(pname))
#}


#returns absolute path to .db file in repo specified (e.g. dept/objects/deptrepo/.db)
@isdept
def get_dbfile_path(repo):#{
  return os.path.join(find_dept_path(), objects_dir, repo, repowrk.dbfile)
#}


#returns absolute path to .deps file in repo and branch specified (e.g. dept/objects/deptrepo/master/.deps)
@isdept
def get_depsfile_path(repo, br):#{
  return os.path.join(find_dept_path(), objects_dir, repo, br, repowrk.depsfile)
#}

#returns absolute path to .logs file in repo and branch specified (e.g. dept/objects/deptrepo/master/.logs)
@isdept
def get_logsfile_path(repo, br):#{
  return os.path.join(find_dept_path(), objects_dir, repo, br, repowrk.logsfile)
#}

#returns absolute path to .config file, i.e. dept/.config
@isdept
def get_configfile_path(): #{
  return os.path.join(find_dept_path(), config_file)
#}


@isdept
def get_remote_attr(attr): #{
  configf = os.path.join(find_dept_path(), config_file)
  return util.get_config_values(section=configf_remote, key=attr, confile=configf)
#}

@isdept
def set_remote_attr(attr, value): #{
  configf = os.path.join(find_dept_path(), config_file)
  return util.update_config_values(section=configf_remote, values={attr:value}, confile=configf)
#}


#returns current remote repo name
def current_repo_name(): return get_remote_attr(configf_remote_repo)


#sets current remote repo name
def set_current_repo_name(name): return set_remote_attr(configf_remote_repo, name)


#returns current remote repo configs
def remote_repo_address(): return get_remote_attr(configf_remote_ip)
def remote_repo_port(): return get_remote_attr(configf_remote_port)
def remote_repo_user(): return get_remote_attr(configf_remote_user)
def remote_repo_passwd(): return get_remote_attr(configf_remote_passwd)


#sets current remote repo ip-address or host name
#def set_current_repo_address(name): return set_remote_attr(configf_remote_ip, name)


#sets current remote repo configs
@isdept
def set_current_repo_config(addr, port, user, passwd): #{
  configf = os.path.join(find_dept_path(), config_file)
  util.update_config_values(section=configf_remote, values={configf_remote_ip:addr}, confile=configf)
  util.update_config_values(section=configf_remote, values={configf_remote_port:port}, confile=configf)
  util.update_config_values(section=configf_remote, values={configf_remote_user:user}, confile=configf)
  return util.update_config_values(section=configf_remote, values={configf_remote_passwd:passwd}, confile=configf)
#}

#returns remote repo path on current repo host
def remote_repo_path(): return get_remote_attr(configf_remote_path)

#set remote repo path
def set_remote_repo_path(rpath): return set_remote_attr(configf_remote_path, rpath)



#returns list of all possible branches
@isdept
def get_all_remote_branches():#{
  if (not current_repo_name()) or (not remote_repo_address()):
    log.error('Cannot get branches names. You need first set up repo name and repo ip-address', to_exit=True)
  dbfile = get_dbfile_path(current_repo_name())
  if not os.path.exists(dbfile):
    log.error("Cannot get branches name. You need first run 'update' command to receive all possible branch names", to_exit=True)
  branches = util.load_obj(dbfile)
  if branches is None:
    log.warn('There is no branches in remote repository {}:{}'.format(remote_repo_address(), current_repo_name()))
    return []
  if type(branches) != type({}):
    log.warn('Corrupt {} file! Update your working copy'.format(repowrk.dbfile), to_exit=True)
  return branches.keys()
#}


#returns current remote branch name
def current_br_name(): return get_remote_attr(configf_remote_br)


#sets current remote branch name
@isdept
def set_current_br_name(name, force=False):#{
  if not force:#{
    branches = get_all_remote_branches()
    if not name in branches:
      log.error("Cannot set up branch: '{}'\nPossible branches: [ {} ]\nTry to execute\n#dept --update\nto update remote info".format(name, ', '.join(sorted(branches))), to_exit=True)
  #}
  return set_remote_attr(configf_remote_br, name)
#}


#returns current remote architecture name
def current_arch_name(): return get_remote_attr(configf_remote_arch)

#sets current remote architecture name
@isdept
def set_current_arch_name(name):#{
  if name not in pack.mandatory_fields['Architecture']:
    log.error('Cannot set architecture {}\nPossible values: [ {} ]'.format(name, ', '.join(pack.possible_arches())), to_exit=True)
  return set_remote_attr(configf_remote_arch, name)
#}


def check_remotes(func):#{
  def wrap(*args, **kwargs):
    if not current_arch_name():
      log.fatal('Architecture is not set for local repository', to_exit=True)
    if not current_repo_name():
      log.error('Remote repository name is not set for local repository. Type:\ndept config set --remote-name NAME\nto set repo name', to_exit=True)
    if not remote_repo_address():
      log.error('Remote repository address is not set for local repository. Type:\ndept config set --remote-addr ADDR\nto set repo address', to_exit=True)
    if not current_br_name():
      log.error('Remote repository branch is not set for local repository. Type:\ndept config set --branch BRANCH\nto set branch', to_exit=True)
    return func(*args, **kwargs)
  return wrap
#}


#returns true if:
#1. /dept_path/objects/repo/br path exists
#2. /dept_path/objects/repo/.db file exists
#3. /dept_path/objects/repo/br/.deps file exists
#else returns false
@isdept
@check_remotes
def local_repo_updated():#{
  repo = current_repo_name()
  br = current_br_name()
  dept_path = find_dept_path()
  if not os.path.exists(os.path.join(dept_path, objects_dir, repo, br)):
    log.warn("Path for repo '{}' and branch '{}' does not exist".format(repo, br))
    return False
  if not os.path.exists(os.path.join(dept_path, objects_dir, repo, repowrk.dbfile)):
    log.warn("Database file for repo '{}' does not exist".format(repo))
    return False
  if not os.path.exists(os.path.join(dept_path, objects_dir, repo, br, repowrk.depsfile)):
    log.warn("Depends file for branch '{}' does not exist".format(br))
    return False
  return True
#}

#returns true if:
#1. /dept_path/objects/repo/.db file exists and has server equal version
#2. /dept_path/objects/repo/br/.deps file exists and has server equal version
#else returns false
def local_repo_has_lastver(): #{
  if not local_repo_updated(): return False
  tmpdir = TemporaryDirectory()
  #print(util.load_obj(get_dbfile_path(current_repo_name())))

  netman.get_db_file(tmpdir.name)
  local_db = get_dbfile_path(current_repo_name())
  remote_db = os.path.join(tmpdir.name, repowrk.dbfile)
  if util.load_obj(local_db) != util.load_obj(remote_db): return False

  netman.get_deps_file(tmpdir.name)
  local_deps = get_depsfile_path(current_repo_name(), current_br_name())
  remote_deps = os.path.join(tmpdir.name, repowrk.depsfile)
  if util.load_obj(local_deps) != util.load_obj(remote_deps): return False
  return True
#}


#ppath -- package absolute path
#pname -- package name in format package_v{version}.zip
#returns dictionary in format: {file: sha1,... }. Dictionary is readed from dept/sha1sums file within package.
def any_package_files_hashsums(ppath, pname): #{
  fhash = zipper.zip_package_hfile(ppath, pname, pack.hash_file, '', pack.control_dir)
  import posixpath
  bin_files = [posixpath.join(pack.bin_dir, b) for b in zipper.zip_package_files(ppath, pname, in_dir=pack.bin_dir)]
  script_files = [posixpath.join(pack.script_dir, s ) for s in zipper.zip_package_files(ppath, pname, in_dir=pack.script_dir)]
  lines = [l for l in fhash.readlines() if l.strip()]
  fhash.close()
  if len(lines) != len(bin_files) + len(script_files):
    log.error("len(files hashes) != len(bin_files) + len(script_files), i.e. {} != {} + {}\nBinaries: {}\nScripts: {}\nHashes: {}".format(len(lines), len(bin_files), len(script_files),
    ', '.join(bin_files), ', '.join(script_files), ', '.join(lines)), to_exit=True)
  sums = {}
  for l in lines:
    tmp = l.decode('utf-8').split(pack.sums_delim)
    if len(tmp) != 2:
      log.error("Error while check package {} (path '{}') integrity. Hash sums file is bad. Line: {}".format(pname, ppath, l), to_exit=True)
    fname = tmp[1].strip().replace('\\', '/') #for old packages
    if (fname not in bin_files) and (fname not in script_files):
      log.error('File {} has hash, but there is no file in package {}.\nBinaries: {}\nScripts:{}'.format(fname, pname, ', '.join(bin_files), ', '.join(script_files)), to_exit=True)
    sums[ fname ] = tmp[0].strip()
  return sums
#}


#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#returns dictionary in format: {file: sha1,... }. Dictionary is readed from dept/sha1sums file within package.
def package_files_hashsums(repo, br, pname): #{
  ppath = get_package_path(repo, br, pname)
  return any_package_files_hashsums(ppath, pname)
#}


#ppath -- package absolute path
#pname -- package name in format package_v{version}.zip
#verify all sha1 checksums of files that written inside dept/sha1sums file
#if all checksums are matched returns True, else returns False
def check_any_package_integrity(ppath, pname): #{
  sums = any_package_files_hashsums(ppath, pname)
  for k in sums.keys():
    dir = os.path.dirname(k)
    fname = os.path.basename(k)
    f = zipper.zip_package_hfile(ppath, pname, fname, '', dir )
    fsha = util.fileh_sha1_hash(f)
    if str(fsha) != sums[k]:
      log.error("package {} (path {}) hash sums are corrupted. Corrupted file: '{}'\nfile sha: {}\nfile sha from sums file: {}".format(pname, ppath, k, str(fsha), sums[k]), to_exit=False)
      return False
  return True
#}


#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#verify all sha1 checksums of files that written inside dept/sha1sums file
#if all checksums are matched return None
#else exits from program
@isdept
def check_package_integrity(repo, br, pname):#{
  ppath = get_package_path(repo, br, pname)
  if not check_any_package_integrity(ppath, pname):
    log.error('package {} ({}:{}): files hash sums are corrupted'.format(pname, repo, br), to_exit=True)
  return
#}


#prints dept remote info
@isdept
def dept_info():#{
  configf = os.path.join(find_dept_path(), config_file)
  adict = util.get_config_values(section=configf_remote, key=None, confile=configf)
  remote_info = "[Remote]:\n{}".format(''.join("{}: {}\n".format(key, val) for key, val in sorted(adict.items())))
  print(remote_info)
#}


#returns list of installed packages from file (inst_file or mainst_file)
#Note: function for internal usage. See get_all_installed_packages and get_main_installed_packages instead
@isdept
def get_installed_packages(file):#{
  dpath = find_dept_path()
  if not os.path.exists(os.path.join(dpath, file)):
    util.save_obj([], os.path.join(dpath, file))
    return []
  return util.load_obj(os.path.join(dpath, file))
#}
  

#returns list of all installed packages inside current local dept repo
#in format: [ (repo, br, pname, pver),... ].
@isdept
def get_all_installed_packages():
  return get_installed_packages(inst_file)


#returns list of main installed packages inside current local dept repo
#in format: [ (repo, br, pname, pver),... ].
@isdept
def get_main_installed_packages():
  return get_installed_packages(mainst_file)


#return list of all installed packages file inside current local dept repo
#in format: ['file1', 'file2', 'somedir/file3',...]
#TODO: WTF? What about manually created files? Rename function at least!
@isdept
def installed_packages_files():#{
  repopath = util.parent_path(find_dept_path())
  pf = []
  for root, dirs, files in os.walk(repopath):
    for file in dirs+files:
      f = os.path.relpath(os.path.join(root, file), os.path.join(repopath, '.'))
      if (ddirname not in f) and (os.path.isfile(os.path.join(repopath, f))):
        pf.append(f)
  return pf
#}


#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#compare hash sums of package pname files and files inside local repo directory
#returns list of files which hash sums are different from each others (format: [file1, file2, somedir/file3,...])
#empty list means that all files are identical
def cmp_inst_and_pkg_files(repo, br, pname):#{
  pn = zipper.package_name(pname)
  pv = zipper.package_version(pname)
  if (repo, br, pn, pv) not in get_all_installed_packages():
    log.warn('Cannot compare files for package {} ({}:{}), because it is not installed'.format(pname, repo, br))  
    return []
  repopath = util.parent_path(find_dept_path())
  diff_files = []
  sums = package_files_hashsums(repo, br, pname)
  for fname,fhash in sums.items():
    relfname = os.path.relpath(fname, pack.bin_dir)
    rfname = os.path.join(repopath, relfname)#repo file name
    if not os.path.exists(rfname):
      log.warn('Package {} ({}:{}) seems to be installed, but file "{}" is not exist in local repo "{}". This case would be considered as file is not changed'.format(pname, repo, br, relfname, repopath))
      #diff_files.append(fname)
      continue
    rfhash = util.file_sha1_hash(rfname)
    if fhash != str(rfhash):
      diff_files.append(fname)
  return diff_files
#}


#repo -- repo name
#br -- branch name
#pn -- package name (only)
#pv -- package version (only)
#main_list -- check whether package would be searched in .mainstalled file
#by_name -- search only by name
#pcache -- installed packages cache
#check if package pname is already installed (in pcache if specified)
#if True, returns tuple (repo, br, pname, pver) (package info)
#else return None
@isdept
def check_installed(repo, br, pn, pv, main_list, by_name=False, pcache=None):#{
  if pcache is None:
    if not main_list:
      pcache = get_all_installed_packages()
    else:
      pcache = get_main_installed_packages()      
  for p in pcache:
    if not by_name:
      if (repo, br, pn, pv) == p:
        return p
    else:
      if pn in p:
        return p
  return None
#}


#see check_installed function
def check_package_installed(repo, br, pn, pver, by_name=False, pcache=None):#{
  return check_installed(repo, br, pn, pver, main_list=False, by_name=by_name, pcache=pcache)
#}


#see chech_installed function
def check_package_mainstalled(repo, br, pn, pver, by_name=False, pcache=None):#{
  return check_installed(repo, br, pn, pver, main_list=True, by_name=by_name, pcache=pcache)  
#}


#check if package pname(pver) listed in remote repo database file
#returns package pname last version if package listed
#if pver specified and version exists returns pver itself
#returns None if package or version does not listed
@isdept
@check_remotes
def check_package_listed(pname, pver=None):#{
  if not local_repo_updated():
    return None
  dept_path = find_dept_path()
  repo = current_repo_name()
  br = current_br_name()
  versions = repowrk.all_package_versions_by_file(dbpath=os.path.join(dept_path, objects_dir, repo, repowrk.dbfile), pname=pname, br=br)
  if not len(versions):
    log.warn("Cannot get package '{}' versions. This case considered as package is not exist".format(pname))
    return None
  if pver:#{
    if not pver in versions:
      log.error("No such version {} for package '{}'".format(pver, pname))
      return None
    return pver
  #}
  return max(versions)
#}


#check if package pname(pver) physically exists in local repository (i.e. already downloaded)
#returns True if package pname_vpver.zip exists; else returns False
@isdept
def check_package_exists(repo, br, pname, pver):#{
  dept_path = find_dept_path()
  pfullname = zipper.full_package_name(pname, pver)
  if os.path.exists( os.path.join(dept_path, objects_dir, repo, br, pname, pfullname) ):
    return True
  return False
#}


#private function for 2 functions below
#returns dictionary from .deps file
@isdept
def __get_depsfile_entries(repo, br, pname, pver):#{
  if not check_package_listed(pname, pver):
    log.error('It seems package {}:{} is not listed in database'.format(pname, pver))
    return {}
  deps_file = get_depsfile_path(repo, br)
  try:
    d = util.load_obj(deps_file)
  except FileNotFoundError:
    log.error('Cannot load dependencies file {}'.format(deps_file)) 
    return {}

  try:
    d[(pname, pver)]
  except KeyError:
    log.fatal('.db file and .deps file are NOT synchronized. Mail this bug to developers or try\n# dept update')
    return {}
  return d
#}


#returns set of all depend packages (including passed)
#or None if something went wrong
#E.g. if pname is A and pver is 1 and .deps file
#contains following entries:
#A(1): B(1), C(2)
#B(1): D(1)
#C(2): 
#D(1):
#if asdict==False
#set {(A,1),(B,1),(C,2),(D,1)} will be return (set of tuples)
#else dictionary: {(A,1): [(B,1), (C,2)], (B,1):[(D,1)], (C,2):[], (D,1):[]} will be returned
@isdept
def package_deps_from_db(repo, br, pname, pver, asdict=False, recursive=True):#{
  d = __get_depsfile_entries(repo, br, pname, pver)
  if not len(d):
    return None
    
  if not recursive: #{
    try:
      if not asdict:
        pkgset = set()
        pkgset.add((pname, pver))
        pkgset.update({(p,v) for p,v in d[(pname, pver)]})
        return pkgset
      else:
        pkgdict = {}
        pkgdict[(pname,pver)] = d[(pname, pver)]
        return pkgdict
    except KeyError:
      log.fatal('Corrupt dependencies file {} (No package {}:{}). Mail this bug to developers or try\n# dept update'.format(deps_file, pname, pver))
      return None
  #}

  def deps_list(pname, pver, pkgset):#{
    pkgset.add((pname, pver))
    try:
      d[(pname, pver)]
    except KeyError:
      log.fatal('Corrupt dependencies file {} (No package {}:{}). Mail this bug to developers or try\n# dept update'.format(deps_file, pname, pver))
      raise Exception()
    for pn,pv in d[(pname, pver)]:
      deps_list(pn, pv, pkgset)
  #}
  def deps_dict(pname, pver, pkgdict):#{
    try:
      d[(pname,pver)]
    except KeyError:
      log.fatal('Corrupt dependencies file {} (No package {}:{}). Mail this bug to developers or try\n# dept update'.format(deps_file, pname, pver))
      raise Exception()
    pkgdict[(pname,pver)] = d[(pname, pver)]
    for pn,pv in d[(pname, pver)]:
      deps_dict(pn, pv, pkgdict)
  #}
  #
  if not asdict:#{
    packages = set()
    deps_func = deps_list
  else:
    packages = {}
    deps_func = deps_dict
  #}
  try:
    deps_func(pname, pver, packages)
  except Exception:
    return None
  return packages
#}


#returns set of all based packages
#or None if something went wrong
#E.g. if pname is C and pver is 2 and .deps file
#contains following entries:
#A(1): B(1), C(2)
#B(1): D(1), C(2)
#C(2): 
#D(1):
#set {(A,1),(B,1)} will be return (set of tuples)
@isdept
def based_packages_from_db(repo, br, pname, pver):#{
  d = __get_depsfile_entries(repo, br, pname, pver)
  if not len(d):
    return None
  packages = [k for k,v in d.items() if (pname, pver) in v]
  return set(packages)
#}


#repo -- repo name
#br -- branch name
#pname -- package name only
#return pname package versions in format: (v1, v2, v3, ...) from deptpath/objects/repo/.db file
@isdept
def package_versions_by_file(repo, br, pname):#{
  dbfile_path = get_dbfile_path(repo)
  return repowrk.all_package_versions_by_file(dbfile_path, pname, br)
#}

#repo -- repo name
#br -- branch name
#pname -- package name only
#returns package pname last version from deptpath/objects/repo/.db file
@isdept
def package_last_version(repo, br, pname): #{
  vers = package_versions_by_file(repo, br, pname)
  if not vers: return None
  return max(vers)
#}


#returns list of all packages files (in package dir pdir if specified)
@isdept
def package_files(repo, br, pname, pver, pdir=None):#{
  if pdir and ((pdir != pack.bin_dir) and (pdir != pack.control_dir) and (pdir != pack.script_dir)):
    log.warn('No such dir {} in package {}:{}'.format(pdir, pname, pver))
    return []
  ppath = get_package_path(repo, br, zipper.full_package_name(pname, pver))
  if not check_package_exists(repo, br, pname, pver):
    log.warn('Package {}:{} file not exists'.format(pname, pver))
    return []
  files = [f for f in zipper.zip_package_files(ppath=ppath, pname=pname, pver=pver, in_dir=pdir) if not f.endswith('/')]
  return files
#}

#checks if package has bin files
def has_bin_files(repo, br, pname, pver):
  return bool(len(package_files(repo, br, pname, pver, pdir=pack.bin_dir)))



#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#script -- script name within script directory
#execute package pname script
@isdept
def execute_package_script(repo, br, pname, script):#{
  print('execute script {} for {}:{}:{} (not implemented yet)'.format(script, repo, br, pname))
  pass
#}


#update installed file.
#adds info if installed = True
#else removes info
@isdept
def update_installed_file(repo, br, pname, installed=True, main_package=False, nofiles=False):#{
  pn = zipper.package_name(pname)
  pv = zipper.package_version(pname)
  dpath = find_dept_path()
  def update_file(pkgs, file, show_log=True, nofiles=False):#{
    details = ''
    if nofiles: details = ' (without unpacking files)'
    if (repo, br, pn, pv) in pkgs:
      if installed:
        if show_log:
          log.notice('Package {}:{} ({}:{}) is already installed'.format(pn, pv, repo, br))
        return
      else:
        pkgs.remove((repo, br, pn, pv))
        util.save_obj(pkgs, os.path.join(dpath, file))
        if show_log:
          log.debug('Package {}:{} ({}:{}) is successfully removed'.format(pn, pv, repo, br))
        return
    else:
      if installed:
        pkgs.append((repo, br, pn, pv))
        util.save_obj(pkgs, os.path.join(dpath, file))
        if show_log:
          log.debug('Package {}:{} ({}:{}) is successfully installed {}'.format(pn, pv, repo, br, details))
        return
      else:
        if show_log:
          log.notice('Package {}:{} ({}:{}) could not be removed, because it is not installed'.format(pn, pv, repo, br))
        return
  #}
  pkgs = get_all_installed_packages()
  update_file(pkgs, inst_file, nofiles=nofiles) #for inst_file
  if (not installed) and ((repo, br, pn, pv) in get_main_installed_packages()):#if in .mainstalled file, remove any case
    main_package = True
  if main_package:
    main_pkgs = get_main_installed_packages()
    update_file(main_pkgs, mainst_file, show_log=False, nofiles=nofiles)
  return
#}


#copies packages (package format: [(repo, br, pn, pv)]) from from_path to deptpath/objects/repo/br/pname
#make to_path if not exists
@isdept
def copy_packages_to_local_repo(packages, from_path, copy_other_files=False):#{
  objects_path = os.path.join(find_dept_path(), objects_dir)
  for repo,br,pn,pv in packages:
    pkg_path = os.path.abspath(os.path.join(objects_path, repo, br, pn))
    os.makedirs(pkg_path, exist_ok=True)
    try:
      shutil.copy2(os.path.join(from_path, zipper.full_package_name(pn,pv)), os.path.join(pkg_path, zipper.full_package_name(pn,pv)))
    except Exception:
      log.process_exception(to_exit=False)  
  if not copy_other_files: return
  if os.path.isfile(os.path.join(from_path, repowrk.dbfile)): 
    shutil.move(os.path.join(from_path, repowrk.dbfile), get_dbfile_path(current_repo_name()))
  if os.path.isfile(os.path.join(from_path, repowrk.depsfile)): 
    shutil.move(os.path.join(from_path, repowrk.depsfile), get_depsfile_path(current_repo_name(), current_br_name()))
  if os.path.isfile(os.path.join(from_path, repowrk.logsfile)): 
    shutil.move(os.path.join(from_path, repowrk.logsfile), get_logsfile_path(current_repo_name(), current_br_name()))
  return
#}


#shows all packages from .db file
#for every branch or for branch brname specified
@isdept
def show_all_packages(repo, brname=None):#{
  if not local_repo_updated():
    return
  dbpath = get_dbfile_path(repo)
  return repowrk.show_dbfile(dbpath, brname)
#}


#decorator
#now only for local configuration, i.e. for EVERY repository
def auth_checker(section=util.user_section):#{
  def wrap(f):
    #[user]
    #user_name = 'name'
    #user_mail = 'email'
    def wrapped_f(*args, **kwargs):
      def enter_attr(aname):
        while True:
          v = input('Enter your {}: '.format(aname))
          if not v.strip():
            continue
          qu = str('Is {} correct?'.format(aname))
          if util.ask_yes_no_question(qu):
            return v
      configfile = get_configfile_path()
      dict = {}
      update_configs = False
      #dict = util.get_config_values(section, confile=configfile)
      #dict_copy = {k:v for k,v in dict.items()}
      if not get_user_name():
      #if user_name not in dict.keys() or not dict[user_name].strip():
        print('*'*20)
        print('Glad to meet you! Please, introduce yourself.')
        name = enter_attr(util.user_name)
        dict[util.user_name] = name
        update_configs = True
      if not get_user_mail():
      #if user_mail not in dict.keys() or not dict[user_mail].strip():
        email = enter_attr(util.user_mail)
        dict[util.user_mail] = email
        update_configs = True
      if update_configs:
        util.update_config_values(section, dict, confile=configfile)
      return f(*args, **kwargs)
    return wrapped_f
  return wrap
#}


#logfile -- absolute path to log file
#logstr -- log string (string or list)
#write text log to file logfile
#with datetime and author information
@auth_checker()
def add_branch_logs(logfile, logstr):#{
  #user_info = get_config_values(user_section)
  if not os.path.isfile(logfile):
    util.save_obj(OrderedDict(), logfile)
  loglines = util.load_obj(logfile)
  if type(loglines) is not OrderedDict: loglines = OrderedDict()
  #configfile = get_configfile_path()
  uname = get_user_name()
  umail = get_user_mail()
  dtstr = datetime.utcnow().strftime('%d.%m.%Y %H:%M')
  lines_to_add = []
  if type(logstr) == type([]):
    lines_to_add = logstr
  else:
    lines_to_add = [logstr,]
  key = (uname, umail, dtstr,)
  loglines[key] = loglines.setdefault(key, []) + lines_to_add
  util.save_obj(loglines, logfile)
  return
#}


#logfile -- absolute path to log file
#lines -- last lines number to show
#template -- show lines with template
def show_branch_logs(logfile, entries=1, pname=None): #{
  if not os.path.isfile(logfile):
    util.dprint('No logs found. Try to update')
    return
  loglines = util.load_obj(logfile)
  if type(loglines) is not OrderedDict:
    util.dprint('No logs found. Try to update')
    return
  ent = int(0)
  for key, values in reversed(list(loglines.items())):
    if ent >= entries: break
    print('[{}, {}, {}]:'.format(key[0], key[1], key[2]))
    for value in values: print(value)
    ent += 1
  return
#}

def get_user_name(): return util.get_config_values(section=util.user_section, key=util.user_name, confile=get_configfile_path())
def get_user_mail(): return util.get_config_values(section=util.user_section, key=util.user_mail, confile=get_configfile_path())


#prints user info
def user_info(): #{
  #configfile = get_configfile_path()
  uname = get_user_name()
  umail = get_user_mail()
  user_str = """[User]
User name: {}
User mail: {}
""".format(uname, umail)
  print(user_str)
  return
#}




#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#checkout package pname (with dependencies) for branch br (if specified) and repository repo (if specified)
#if branch and repo is None, checkout pname for current repo and branch
@isdept
def checkout_branch(pname, repo=None, br=None):
  pass



if __name__ == '__main__':
  deptlocal = '/home/jack/deptlocal/debug'
  util.curpath = deptlocal
  pass
