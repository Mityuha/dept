#coding=utf-8

import os
import sys
import re
import time
from datetime import datetime
from _root_path_ import root_path
from traceback import print_exception
import pickle
from hashlib import sha1 as hashlibsha1
#import operator

curpath = os.getcwd()
filerc = '.deptrc'
confdir = 'config'
#config_file = os.path.join(root_path, confdir, filerc)

#dept info install package path
dinfo_path = '.dept'

#python3 decorator
def python3_checker(func=None):#{
  def wrap(*args, **kwargs):
    if (sys.version_info < (3, 4)):
      sys.exit("Python version >= 3.4 required!")
    if func is not None:
      return func(*args, **kwargs)
  return wrap
#}

(python3_checker())()
import log_service as log

#@python3_checker
#def foo():
#  pass
#the same as
#foo = python3_checker(foo)


#prints dictionary dict as tree
#dict format: {key: [value1, value2, ...], ...}
def print_dict_as_tree(dict, root_key):#{
  depend_keys = []
  connector = '|'
  line = '----'
  #prefix = lambda depth: ('%s%s' % (connector*(int(bool(depth))),' '*depth*(len(line)+1)))#*depth
  prefix = lambda depth: ('%s%s' % (connector,' '*len(line)))*depth
  def tree_print(key, depth, isroot=False):#{
    depend_keys.append(key)
    if isroot:
      print('#',key)
    else:
      print('#','%s%s%s' % (prefix(depth-1),connector,line),key)
    if not (key in dict.keys()):
      return
    for val in dict[key]:
      print('#','%s%s' % (prefix(depth), connector))
      tree_print(val, depth+1)
  #}
  if not root_key in dict.keys():
    print('No such key:', root_key, ' in dict', dict)
    return
  tree_print(root_key, int(0), isroot=True)
  for k in dict.keys():
    if not (k in depend_keys):
      tree_print(k, int(0), isroot=True)
  return  
#}


class time_decorator(object):#{
  def __init__(self, f):
    self.f = f
  def __call__(self, *args, **kwargs):
    t1 = time.time()
    log.debug("Entering function %s" % (self.f.__name__,))
    res=self.f(*args, **kwargs)
    log.notice('%s time: %.5f s' % (self.f.__name__, time.time() - t1,))
    return res
#}

#for private usage
def sublist(path, func):#{
  try:
    return [d for d in os.listdir(path) if func(os.path.join(path,d))]
  except Exception:
    log.process_exception(to_exit=False)
    return []
#}


#return list of all subdirectories inside path
def subdirs_list(path):#{
  return sublist(path, os.path.isdir)
#}


#return list of all subfiles inside path
#note: for recursive subfiles use os.walk()
def subfiles_list(path):#{
  return sublist(path, os.path.isfile)
#}

def recursive_glob(rootpath, mask, recursive=True): #{
  res = []
  p = re.compile(mask)
  if recursive:
    for root, _, files in os.walk(rootpath):
      r = [os.path.join(root, f) for f in files if p.fullmatch(f)]
      res += r
  else: res = [os.path.join(rootpath, f) for f in subfiles_list(rootpath) if p.fullmatch(f)]
  return res
#  pattern = mask
#  #for l in mask:  pattern += '[{}{}]'.format(l.lower(), l.upper()) if l.isalpha() else l      
#  if (sys.version_info >= (3, 5)):
#    import glob
#    return glob.glob('{}/**/{}'.format(rootpath, pattern), recursive=recursive)
#  else:
#    from pathlib import Path
#    if recursive: gen = Path(rootpath).glob('**/{}'.format(pattern))
#    else: gen = Path(rootpath).glob('{}'.format(pattern))
#    return [str(f) for f in gen]
#}

def toposixpath(path): return path.replace('\\', '/')


#check if path 'path' is a disk root path
def path_is_root(path):#{
  npath = os.path.normpath(path)
  if os.name == 'posix':
    root_path = '/'
    if npath == root_path:
      return True
    return False
  elif os.name == 'nt':
    root_path = re.compile('^[a-zA-Z][:][\\\]{1,2}')
    if root_path.fullmatch(npath):
      return True
    return False
  else:
    sys.exit('root path is not determined for {} system yet'.format(os.name))
  return False
#}


#return parent path for 'path'
def parent_path(path):#{
  return os.path.dirname(os.path.normpath(path))
#}


#obj -- object to save
#fname -- absolute path to file to save as
#save object obj as file fname
def save_obj(obj, fname):#{
  with open(fname, 'wb') as f:
    pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
#}


#fname -- absolute path to file to save as
#return object that had been saves as file fname
def load_obj(fname):#{
  try:
    with open(fname, 'rb') as f:
      return pickle.load(f)
  except Exception:
    log.process_exception(to_exit=False)
#}


#fileh - file like object opened in 'rb' mode
#returns file handler fileh hash(sha1) sum
def fileh_sha1_hash(fileh):#{
  try:
    sha1 = hashlibsha1()
    for chunk in iter(lambda: fileh.read( 128 * sha1.block_size), b''):
      sha1.update(chunk)
    return sha1.hexdigest()
  except Exception:
    log.process_exception()
#}


#filename -- absolute path to file
#returns file filename sha1 hash
def file_sha1_hash(filename):#{
  try:
    with open(filename, 'rb') as f:
      return fileh_sha1_hash(f)
  except Exception:
    log.process_exception()
#}


#lmts -- last modification time stamp
#fname -- absolutely path to file fname
#returns lmts of file fname or 0 if there is no such file
def file_lmts(fname):#{
  try:
    mtime = os.path.getmtime(fname)
  except Exception:
    log.process_exception(to_exit=False)
    mtime = 0
  return datetime.fromtimestamp(mtime)
#}


####################
user_section = 'user'
user_name = 'name'
user_mail = 'email'

#if key is None and section is None return dectionary of all sections with all atributes, i.e.
#{section: {attribute_name: attribute_value}, ...}
#else if key is None and section is not None return dictionary of all section attributes, i.e.
#{attribute_name:attribute_value}
#else if key is not None and section is not None return key value by section, i.e. attribute_value if exists
#else if key is not None and section is None, return dictionary from case 1 (i.e. key is ignored)
#if file is specified it must be absolute path to file. If file is not specified, 
#default configuration file (root_path/confdir/filerc) is meant
#@time_decorator
def get_config_values(section=None, key=None, confile=None): #{
  if confile is not None:
    config_file = confile
  else:
    os.makedirs(os.path.join(root_path, confdir), exist_ok=True)
    config_file = os.path.join(root_path, confdir, filerc)

  if not os.path.exists(config_file):
    save_obj({}, config_file)

  dict = load_obj(config_file)

  if not section:
    return dict

  try:
    sect_dict = dict[section]
  except KeyError:
    sect_dict = {}

  if not key:
    return sect_dict

  try:
    return sect_dict[key]
  except KeyError:
    return None
#}


#section -- section name in file
#values is dict in format  {attribute_name: attribute_value, ...}
#confile if specified is meant configuration file to update
#updates dictionary of section
def update_config_values(section, values, confile=None): #{
  if type(values) != type({}):
    log.error('Cannot update config values. Values argument is not dictionary, ', values)
    return

  if confile is not None:
    config_file = confile
  else:
    config_file = os.path.join(root_path, confdir, filerc)

  dict = get_config_values(confile=config_file) #all config values
  sect_dict = get_config_values(section, confile=config_file) #section config values
  sect_dict.update(values) #updating section values
  dict[section] = sect_dict #updating all values
  save_obj(dict, config_file)
  return
#}


#txt -- question string with trailing '?' sign
#return True, if question positive
def ask_yes_no_question(txt): #{
  check = input('{} [y/n]:'.format(txt))
  while ('y' != check.strip()) and ('n' != check.strip()):
    check = input("Only 'y' or 'n':")
  if check == 'y':
    return True
  return False
#}


#user params
#def get_user_name(confile=None): return get_config_values(section=user_section, key=user_name, confile=confile)
#def get_user_mail(confile=None): return get_config_values(section=user_section, key=user_mail, confile=confile)


def dprint(*args, **kwargs): return print('#', *args, **kwargs)


if __name__ == '__main__':#{
  dict = {'A':['B', 'C'], 'D':['K', 'L'], 'B':['D'], 'K':[]}
  print_dict_as_tree(dict, 'A')
#}
