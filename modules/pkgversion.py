#coding=utf-8

import utils as util
import log_service as log
import zipper
import repo_service as reposrv
import repoworker as repowrk
#import netman
import os
from decimal import Decimal

ver_inc = float(0.1)
newvers_file = '.versions'

def get_versfile_path(): return os.path.join(reposrv.find_dept_path(), reposrv.upload_dir, newvers_file)

def version_is_correct(ver): #{
  try:
    a = float(ver)
  except Exception as e:
    log.error('Bad version format. {}'.format(e))
    return False
  prec = len(str(ver_inc).split('.')[1])
  try:
    ver_prec = len(str(ver).split('.')[1])
  except Exception as e:
    log.error('Version must be float number, e.g. "1.0"')
    return False

  return float(Decimal(str(ver)) / Decimal(str(ver_inc))).is_integer()
#}


#repo -- repo name
#br -- branch name
#pname -- package name(only)
#pver -- package version (only)
def get_next_pver(repo, br, pname, newvers): #{
  plastver = reposrv.package_last_version(repo, br, pname)
  if plastver is None:
    if pname in newvers.keys(): return newvers[pname] #newly created package
    log.error('Cannot determine last package version for package {}'.format(pname), to_exit=True)
  #pnewver = str(float(plastver)+ver_inc)
  prec = len(str(ver_inc).split('.')[1])
  pnewver = '{0:.{1}f}'.format(float(plastver)+ver_inc, prec)
  if pname in newvers.keys(): #{
    if newvers[pname] <= plastver:
      log.warn('Cannot apply manual package version for package %s. Manual version(%s) <= last package version(%s).\
      Last version increment would be applied' % (pname, newvers[pname], plastver))
    else:
      pnewver = newvers[pname]
  #}
  return pnewver
#}


#removes file newvers_file if exists
@reposrv.isdept
def clear_versions(): #{
  newv_file = get_versfile_path()
  try:
    os.remove(newv_file)
  except FileNotFoundError:
    pass
  except Exception:
    log.process_exception(to_exit=False)
  return
#}


#pkglist -- packages list in format: [(repo, br, pn, pv),...]
#newvers -- packages new versions dictionary in format: {pname:newpver}
#calculates new versions for all packages in pkglist (considering newvers dictionary)
#and saves them in file deptrepo/foruploading/newvers_file
#format: {(repo,br,pname,pver): newver, ...}
@reposrv.isdept
def calc_packages_new_versions(pkglist, newvers): #{
  newv_file = get_versfile_path()
  nvers_dict = {(repo,br,pn,pv):get_next_pver(repo,br,pn,newvers) for (repo,br,pn,pv) in pkglist}
  util.save_obj(nvers_dict, newv_file)
  return
#}


#returns calculated packages versions from deptrepo/foruploading/newvers_file file
#format: {(repo,br,pname,pver): newver, ...}
@reposrv.isdept
def packages_new_versions():#{
  newv_file = get_versfile_path()
  try:
    return util.load_obj(newv_file)
  except Exception:
    return {}
#}


#repo -- repo name
#br -- branch name
#pn -- package name only
#pv -- package version
#Note: if pv is ommited, first meeting package new version would be returned 
#But if there are two packages with the same names and different versions -- it is a GLOBAL bug!
#returns calculated new package version from file newvers_file
#or None, if package missed in file, or file is not existing
@reposrv.isdept
def new_package_version(repo, br, pn, pv=str(), pcache={}, by_name=False):#{
  if (not len(pcache)) or pcache is None:
    pcache = packages_new_versions()

  if by_name:
    for r,b,n,v in pcache.keys():
      if n == pn:
        return pcache[(r,b,n,v)]

  if pv != str():#{
    try:
      return pcache[(repo, br, pn, pv)]
    except KeyError:
      return None
  #}
  else:#{
    for r,b,n,v in pcache.keys():
      if (r,b,n) == (repo, br, pn):
        return pcache[(r,b,n,v)]
  #}
  return None
#}


#repo -- repo name
#br -- branch name
#pn -- package name only
#pv -- package version only
#brpath -- branch directory full path
#pkg_changed -- flag specidied whether package had any changes
#pkgbin_changed -- flag specified whether package bin files has been modified
#returns dictionary in format {(pname, pnewver, poldver): [(pname1, pnewver1), ...], ...}
#package old version differ from package new version, if there are some package changes or/and package files were updated
#updating ptree versions for all packages
def get_new_package_versions_tree(repo, br, pn, pv, brpath, pkg_changed=False, pkgbin_changed=True, newvers={}):#{
  new_ptree = {}#format: {(name, newver, oldver): [(name1, newver), (name2, newver),...], ...}
  #1. Update all keys
  ptree = repowrk.dept_package_dep_tree(brpath, pn, pv)
  for pn,pv in ptree.keys():#{
    npv = pv #new package version
    #if package has no changes and all package files are the same (not updated) -- don't calculate new package version
    if (pkg_changed) or (pkgbin_changed and len(reposrv.cmp_inst_and_pkg_files(repo, br, zipper.full_package_name(pn, pv)))):
      npv = get_next_pver(repo, br, pn, newvers)#new package version
    new_ptree[(pn, npv, pv)] = ptree[(pn, pv)]
  #}

  #2. Update all values
  for k in new_ptree.keys():#{
    pn, pnewv, poldv = k
    if pnewv == poldv:
      continue
    newlst = [(pn,pnewv) if x==(pn,poldv) else x for x in new_ptree[k]]
    new_ptree[k] = newlst
  #}
  return new_ptree
#}

