#coding=utf-8

#Package changes are such changes, that influence the fact the package will be update
import utils as util
import repo_service as reposrv
from tempfile import TemporaryDirectory
import zipper
import pack_dealer as pack
import log_service as log
import pkgversion as pversion
from collections import OrderedDict
import os

change_file = '.changes'
#change actions
change_action = 'action'
act_add = 'add'
act_rm = 'rm'
act_alter = 'alter'
act_update = 'update'
act_create = 'create'

#change instances
change_instance = 'instance'
inst_dep = 'dep'
inst_file = 'file'
inst_package = 'package'

#change arguments
arg_repo = 'repo'
arg_br = 'br'
arg_pname = 'pname'
arg_pver = 'pver'
arg_plog = 'plog'
arg_fpath = 'filepath'
arg_fname = 'filename'
arg_pfpath = 'fileppath'
arg_pfname = 'filepname'
arg_nofiles = 'nofiles'


#get all changes for all packages
#if changes file not exists and create_file flag is True, file will be created
#returns ordered dict in format: {[repo,br,pn,pv]:[(action, instance, args)], ...}
@reposrv.isdept
def get_all_changes(create_file=False):#{
  changesfile = os.path.join(reposrv.find_dept_path(), reposrv.upload_dir, change_file)
  if not os.path.exists(changesfile):
    if create_file:
      util.save_obj(OrderedDict(), changesfile)
    else:
      return OrderedDict()
  return util.load_obj(changesfile)
#}


#clear all changes by removing change_file
@reposrv.isdept
def clear_all_changes():#{
  changesfile = os.path.join(reposrv.find_dept_path(), reposrv.upload_dir, change_file)
  try:
    os.remove(changesfile)
  except FileNotFoundError:
    pass
  except Exception:
    log.process_exception(to_exit=False)
  return
#}


#returns list of all changed packages in format: [(repo, br, pn, pv), ...]
@reposrv.isdept
def get_changed_pkg_list(): return [p for p in get_all_changes().keys()]


#package -- tuple in format: (repo, br, pn, pv)
#action -- action for package
#instance -- instance for action
#args -- arguments for action instance change
#Function does not check if package installed or even exists. To check it call reposrv.check_package_installed(...) BEFORE this call
@reposrv.isdept
def add_change(package, action, instance, args={}):#{
  if action not in action_dict.keys():
    log.error('Unknown change action: {}'.format(action), to_exit=True)

  changesfile = os.path.join(reposrv.find_dept_path(), reposrv.upload_dir, change_file)
  changes = get_all_changes(create_file=True)
  pkg_in_changes = package in changes.keys()
  if not pkg_in_changes: changes[package] = []

  if (action, instance, args) not in changes[package]:
    changes[package].append( (action, instance, args,) )
  util.save_obj(changes, changesfile)
  log.debug("Change '{}' '{}' added to package {}:{} ({}:{})".format(action, instance, package[2], package[3], package[0], package[1]))
  return
#}


#shows list of all changes
#changes stored in format:
#OrderedDict{(repo,br,pn,pv):[(action1, instance1, arg1), ...], ...}
@reposrv.isdept
def show_changes(): #{
  changes = get_all_changes()
  for knum,key in enumerate(changes.keys()):
    print('%d. %s:%s (%s:%s):' % (knum+1, key[2], key[3], key[0], key[1]))
    for chnum,(action, instance, args) in enumerate(changes[key]):
      print('--> %d. %s, %s, ' % (chnum+1,action,instance), args)
  return
#}


#returns package changes for packages repo:br:pn:pv
#format: [action, instance, args={...}]
@reposrv.isdept
def package_changes(repo, br, pn, pv): #{
  changes = get_all_changes()
  try:
    return changes[(repo, br, pn, pv)]
  except KeyError:
    return []
#}


#returns all packages versions changes if present
#format: {pn: new_version}
@reposrv.isdept
def packages_ver_changes(): #{
  pvers = {}
  for pkg,chgs in get_all_changes().items():
    for chg in chgs:
      action, inst, args = chg
      if not all((action==act_alter, inst==inst_package, arg_pver in args.keys())): continue
      pvers[ pkg[2] ] = args[arg_pver]
  return pvers
#}


#delete all changes for package
@reposrv.isdept
def rm_changes_for_package(repo, br, pn, pv):#{
  changesfile = os.path.join(reposrv.find_dept_path(), reposrv.upload_dir, change_file)
  changes = get_all_changes()
  try:
    changes.pop( (repo, br, pn, pv) )
    util.save_obj(changes, changesfile)
  except KeyError:
    pass
  return
#}


#removes change specified by package and change numbers
#Note: see show_changes() function
@reposrv.isdept
def rm_change(pkgnum, chnum):#{
  pass
#}


#repo -- repo name
#br -- branch name
#pn -- package name only
#pv -- package version only
#check if package has any changes
@reposrv.isdept
def package_has_changes(repo, br, pn, pv): return (repo, br, pn, pv) in get_changed_pkg_list()


@reposrv.isdept
def package_change_log(repo, br, pn, pv):#{
  return str()
#}


#check whether package has 'update bin files' change
@reposrv.isdept
def package_has_upbin_change(repo, br, pn, pv):#{
  changes = package_changes(repo, br, pn, pv)
  for (act, inst, args) in changes:
    if (act == act_update) and (inst == inst_package):
      return True
  return False
#}

#
#function to apply changes
#

#pack_path -- path where unzipped (extracted) package files are located
#instance -- instance to which changes applied
#args -- arguments dictionary for act
#apply add change for instance with specified args
@reposrv.isdept
def apply_add_change(pack_path, instance, args):#{
  log.notice('Add action not implemented yet')
  return
#}



#pack_path -- path where unzipped (extracted) package files are located
#instance -- instance to which changes applied
#args -- arguments dictionary for act
#apply rm change for instance with specified args
@reposrv.isdept
def apply_rm_change(pack_path, instance, args):#{
  log.notice('Remove action not implemented yet')
  return
#}



#pack_path -- path where unzipped (extracted) package files are located
#instance -- instance to which changes applied
#args -- arguments dictionary for act
#apply alter change for instance with specified args
@reposrv.isdept
def apply_alter_change(pack_path, instance, args): #{
  log.notice('Alter action not implemented yet')
  return
#}


#pack_path -- path where unzipped (extracted) package files are located
#instance -- instance to which changes applied
#args -- arguments dictionary for act
#apply update change for instance with specified args
@reposrv.isdept
def apply_update_change(pack_path, instance, args):#{
  if instance == inst_package:
    repopath = util.parent_path(reposrv.find_dept_path())
    pack.update_package_files(extractdir=pack_path, pdir=pack.bin_dir, uppath=repopath)
  else:
    log.notice('Update action for instance %s not implemented yet' % (instance,))
  return
#}


#pack_path -- path where unzipped (extracted) package files are located
#args -- arguments dictionary for act
#creates package
@reposrv.isdept
def apply_create_change(pack_path, instance, args): #{
  #log.notice('Create package action not implemented yet')
  return
#}


action_dict = { act_add : apply_add_change,
act_rm : apply_rm_change,
act_alter : apply_alter_change,
act_update : apply_update_change,
act_create : apply_create_change
}


#repo -- repo name
#br -- branch name
#pn -- package name only
#pv -- package version
#1. Apply package changes (!!!NOT RECURSIVELY, FOR PACKAGE ONLY!!!) if present without any dependence changes
#2. Returns TemporaryDirectory() instance with package applied changes and new package version
@reposrv.isdept
def apply_changes(repo, br, pn, pv):#{
  #if not reposrv.check_package_installed(repo, br, pn, pver=pv):
  #  log.error('Package %s:%s is not installed' % (pn, pv))
  #  return None

  if not package_has_changes(repo, br, pn, pv):
    return None

  repopath = util.parent_path(reposrv.find_dept_path())
  ppath = reposrv.get_package_path(repo, br, zipper.full_package_name(pn, pv))
  brpath = util.parent_path(ppath)

  tmpd = TemporaryDirectory()
  zipper.extract_zip_package(ppath, zipper.full_package_name(pn, pv), outpath=tmpd.name)
  changes = package_changes(repo, br, pn, pv)

  #input(tmpd.name)

  nofiles = None

  for (act, inst, args) in changes:
    if arg_nofiles in args.keys():
      if (nofiles is not None) and (nofiles != args[arg_nofiles]): log.error("Package {}:{} ({}:{}) ambigious argument '{}'".format(pn,pv,repo,br,arg_nofiles), to_exit=True)
      nofiles = args[arg_nofiles]
    action_dict[act](tmpd, inst, args)

  #package version and package dependencies (as a result) updates by default
  #other package attributes need to be changed

  #new package version
  pnewver = pversion.new_package_version(repo, br, pn, pv)

  def update_package_version():#{
    pack.update_package_value(tmpd.name, 'Version', pnewver, reposrv.get_user_name(), reposrv.get_user_mail(), nofiles_ok=nofiles)
    return
  #}

  def update_package_dependencies():#{
    dep_str = str()
    deps = pack.get_package_value(tmpd.name, 'Depends', nofiles_ok=nofiles)
    if deps is None:
      return
    for pdep in deps.split(pack.deps_delim):#{
      pname, sign, pver = pack.parse_pdep(pdep)
      pnewv = pversion.new_package_version(repo, br, pname, pver)
      if not pnewv:#dependence package not updated or already updated
        pnewv = pver
      if dep_str != str():
        dep_str += pack.deps_delim
      dep_str += pack.format_pdep(pname, pnewv)
    #}
    pack.update_package_value(tmpd.name, 'Depends', dep_str, reposrv.get_user_name(), reposrv.get_user_mail(), nofiles_ok=nofiles)
    return
  #}

  def update_package_log(): #{
    plog = package_change_log(repo, br, pn, pv)
    if plog:
      pack.add_package_logs(os.path.join(tmpd.name, pack.control_dir, pack.log_file), plog, reposrv.get_user_name(), reposrv.get_user_mail())
    return
  #}

  update_package_version()
  update_package_dependencies()
  update_package_log()

  return tmpd
#}

