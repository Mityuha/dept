#coding=utf-8

import utils as util
import repo_service as reposrv
import zipper
import log_service as log
import pack_dealer as pack
import shutil
import pkgchanges as changes
import pkgversion as versions
import repoworker as repowrk
import os
import netman
from tempfile import TemporaryDirectory


#uppath -- path of packages to upload
#returns dictionary in format: {pname1:[v1, v2, ...], pname2:[v1, v2,...]...}
def scan_uppath(uppath): #{
  res = {}
  if not os.path.isdir(uppath): return res
  for pfname in util.subfiles_list(uppath):
    if not zipper.is_package_name(pfname): continue
    pn,pv = zipper.package_name(pfname), zipper.package_version(pfname)
    if pn in res.keys(): res[pn].add(pv)
    else: res[pn] = {pv, }
  return res
#}

#append new entries to .db file
def update_dbfile(uppath): #{
  tmp_db = os.path.join(uppath, repowrk.dbfile)
  br = reposrv.current_br_name()
  pkgver_dict = scan_uppath(uppath) #{pname:[v1, v2,...],...}
  cur_pkgver = util.get_config_values(section=br, key=None, confile=tmp_db)
  for p,vers in pkgver_dict.items():
    cur_pkgver[p] = cur_pkgver.setdefault(p, set()).union(vers)
  util.update_config_values(section=br, values=cur_pkgver, confile=tmp_db)
  return True
#}

#append new entries to .deps file
def update_depsfile(uppath): #{
  tmp_deps = os.path.join(uppath, repowrk.depsfile)
  br = reposrv.current_br_name()
  br_path = os.path.dirname(reposrv.get_depsfile_path(reposrv.current_repo_name(), br))
  new_deps = {}
  for pfname in util.subfiles_list(uppath):
    if not zipper.is_package_name(pfname): continue
    pn, pv = zipper.package_name(pfname), zipper.package_version(pfname)
    #build the whole tree (!!!) to check all dependencies are correct!
    pn_deplist = repowrk.dept_package_dep_list([uppath, br_path], pn, pv, recursive=False)
    if (pn,pv,) in new_deps.keys():
      log.error('Something went wrong while uploading. Package {}(v{}) at least updated twice'.format(pn,pv))
      return False
    new_deps[(pn, pv)] = pn_deplist

  cur_deps = util.load_obj(tmp_deps)
  intersection = set(cur_deps.keys() & new_deps.keys())
  if len(intersection): 
    log.error('Error while upload packages. Packages ({}) versions have not been updated for some reason\nTry to update your repo'.format(', '.join('{}:{}'.format(pn, pv) for pn,pv in intersection)))
    return False
  cur_deps.update(new_deps)
  util.save_obj(cur_deps, tmp_deps)
  return True
#}


def get_lmts(lmts_dct, fname, ppath, pname, pver='', dir=None): #{
  try: return lmts_dct[fname]
  except KeyError: return zipper.zip_file_lmts(ppath=ppath, pname=pname, fname=fname, pver=pver, dir=dir)
#}

#repo -- repo name
#br -- branch name
#ptree -- package tree in format of function dept_package_dep_tree returning value
#checks whether dependencies packages have older modification time stamp
#returns set of packages which time stamps needs to be updated in format: [(repo, br, pname, pver),...]
#or return empty set
def check_lmts(repo, br, ptree, uppath): #{
  repopath = util.parent_path(reposrv.find_dept_path())
  lmts_dct = {util.toposixpath(f):util.file_lmts(os.path.join(repopath, f)) for f in reposrv.installed_packages_files()}
  pkgstoup = {}
  for bn, bv in ptree.keys(): #{ #basename, baseversion
    if not reposrv.check_package_installed(repo, br, bn, None, by_name=True):
      log.debug('Check lmts for base package {} ({}:{}) skipped, because it is not installed'.format(bn, repo, br))
      continue
    bppath = reposrv.get_package_path(repo, br, zipper.full_package_name(bn, bv))
    bpth = repowrk.determine_package_path([bppath, uppath], bn, bv)
    if not bpth:
      log.fatal('Cannot determine path for package {}:{} with branch {}'.format(bn, bv, br), to_exit=True)
    ########################
    bfiles = zipper.zip_package_files(bpth, bn, bv, in_dir=pack.bin_dir) #basepackage files
    for n, v in ptree[(bn, bv)]: #{
      if not reposrv.check_package_installed(repo, br, n, None, by_name=True):
        log.debug('Check lmts for dependent package {} ({}:{}) skipped, because it is not installed'.format(n, repo, br))
        continue
      dppath = reposrv.get_package_path(repo, br, zipper.full_package_name(n, v))
      dpth = repowrk.determine_package_path([dppath, uppath], n, v)
      if not dpth:
        log.fatal('Cannot determine path for package {}:{} with branch {}'.format(bn, bv, br), to_exit=True)
      ########################
      files = zipper.zip_package_files(dpth, n, v, in_dir=pack.bin_dir)
      next_package = False
      for f in files: #{
        if next_package:
          break
        for bf in bfiles:
          #The idea is: if time stamp of dependency package files is updated,
          #there needs to be update time stamp of base package files, i.e. A: B
          #if B is updated, you need to update A either, i.e. ts(A) >= ts(B)
          try:
            bf_lmts = get_lmts(lmts_dct, fname=bf, ppath=bpth, pname=bn, pver=bv, dir=pack.bin_dir)
            df_lmts = get_lmts(lmts_dct, fname=f, ppath=dpth, pname=n, pver=v, dir=pack.bin_dir)
            
            #if lmts_dct[bf] < lmts_dct[f]:
            if bf_lmts < df_lmts:
              log.notice('ts({}:{}) < ts({}:{}), (ts({}) < ts({}))'.format(bn, bv, n, v, bf, f))
              pkgstoup[(repo,br,bn,bv)] = bfiles
              next_package = True
              break
          except Exception as e:
            #print(bppath, bpth)
            #print(dppath, dpth)
            log.error("{}\nCannot check files '{}' and '{}'\nBase package: {}\nDependent package: {}".format(e, bf, f, bn, n), to_exit=True)
      #}
      if next_package:
        break
    #}
  #}
  return {k:pkgstoup[k] for k in set(pkgstoup.keys())}
#}


#repo -- repo name
#br -- branch name
#pname -- package name only
#pver -- package version only
#uppath -- path to copy new packages
#ch_pkgs -- really changed packages
#with_deps -- specify whether package pname would be updated with dependencies
#with_lmts -- specify whether Last Moditication Time Stamp of based packages would be considered
#prepares package pname for uploading recursively
#if package pname had no changes, return
#applies changes to package pn,pv recursively, 
@reposrv.isdept
def prepare_for_uploading(repo, br, pn, pv, uppath, ch_pkgs, with_deps=True, with_lmts=True):
  repopath = util.parent_path(reposrv.find_dept_path())
  ppath = reposrv.get_package_path(repo, br, zipper.full_package_name(pn, pv))
  brpath = util.parent_path(ppath)

  if all(( (repo, br, pn, pv) not in reposrv.get_all_installed_packages(), not changes.package_has_changes(repo,br,pn,pv))):
    log.warn('package {} ({}:{}) cannot be updated, because it is either not installed nor newly created'.format(pn,repo,br), to_exit=True)

  #return true, if package pn_vpnewver.zip already exists,
  #i.e. change for base package already applied
  def change_already_applied(pn, pnewver):#{
    pfullname = zipper.full_package_name(pn, pnewver)
    return os.path.exists( os.path.join(uppath, pfullname) )
  #}

  pn_new_version = pv

  ptree = repowrk.dept_package_dep_tree(brpath, pn, pv)
  #applying changes
  for n,v in ptree.keys(): #{
    if (repo, br, n, v) not in ch_pkgs: #if package is not changed, continue
      continue
    pnewv = versions.new_package_version(repo, br, n, v)
    if change_already_applied(n, pnewv): #if changes is already applied, continue (the same as the first test)
      continue
    tmpd = changes.apply_changes(repo, br, n, v)
    if tmpd:
      zipper.create_zip_package(tmpd.name, pname=n, pver=pnewv, outpath=uppath)
      if not reposrv.check_any_package_integrity(uppath, zipper.full_package_name(n,pnewv)):
        log.error('Package {}:{} is corrupted. Mail about this bug to developers and try to apply changes again'.format(n,pnewv), to_exit=True)
      if (n == pn) and (v == pv):
        pn_new_version = pnewv
    else: log.warn('Package {}:{} : changes not applied'.format(n, v))
    log.debug('remove {}:{} ({}:{}) from changed packages'.format(n, v,repo, br))
    ch_pkgs.remove((repo, br, n, v))
  #}

  #CHECK LAST MODIFICATION TIME STAMPS FOR PACKAGE TREE
  if with_lmts: #{
    ptree = repowrk.dept_package_dep_tree([brpath, uppath], pn, pn_new_version)
    pkgstoup = check_lmts(repo, br, ptree, uppath=uppath)
    if len(pkgstoup): #{
      err_str = 'To upload {}:{} ({}:{}) package you need first update (time stamp) next packages (in brackets) files:\n'.format(pn, pv, repo, br)
      for key,val in pkgstoup.items():
        r,b,n,v = key
        err_str += '[{}:{}:{}:{}]:\n'.format(r,b,n,v)
        for f in val:
          err_str += '- {}\n'.format(f)
      log.error(err_str)
      return False
    #}
  #}
  return True
#}


#
def get_really_changed_packages(main_installed_pkgs=None):#{
  really_changed_pkgs = [] #[(repo, br, pn, pv)]
  #filter changed packages
  for repo,br,pn,pv in changes.get_changed_pkg_list(): #[(repo, br, pn, pv)]
    pkg_has_upbin_change = changes.package_has_upbin_change(repo,br,pn,pv)
    if (pkg_has_upbin_change and len(reposrv.cmp_inst_and_pkg_files(repo,br,zipper.full_package_name(pn,pv)))) or\
       (pkg_has_upbin_change and len(changes.package_changes(repo,br,pn,pv))>1) or\
       (not pkg_has_upbin_change and len(changes.package_changes(repo,br,pn,pv))) :
      really_changed_pkgs.append((repo, br, pn, pv))
  
  if not main_installed_pkgs: main_installed_pkgs = reposrv.get_main_installed_packages()
  for repo, br, pn, pv in main_installed_pkgs:
    if reposrv.has_bin_files(repo, br, pn, pv): continue #0. Check if meta
    first_level_deps = {(repo, br, n, v) for n,v in reposrv.package_deps_from_db(repo, br, pn, pv, asdict=False, recursive=False)}
    first_level_deps.remove((repo, br, pn, pv))
    for entry in first_level_deps:
      if entry in really_changed_pkgs:
        really_changed_pkgs.append((repo, br, pn, pv))
        changes.add_change((repo, br, pn, pv), changes.act_update, changes.inst_package)
        break
  return list(set(really_changed_pkgs))
#}


#returns absolute path to upload directory, where packages, .logs and .destination files are located
#for all packages in 'really_changed_pkgs' list if specified
#or for all changed packages
@reposrv.isdept
@reposrv.check_remotes
def prepare_changes_for_uploading(really_changed_pkgs=None, with_lmts=True, with_deps=True):#{
  versions.clear_versions()
  main_installed_pkgs = reposrv.get_main_installed_packages()
  
  if not really_changed_pkgs:
    really_changed_pkgs = get_really_changed_packages(main_installed_pkgs)

  if not len(really_changed_pkgs):
    log.warn('No packages for uploading. Make any binary package file changes and try again')
    return None

  #print(really_changed_pkgs)

  newvers = changes.packages_ver_changes() #{pn:newversion}
  versions.calc_packages_new_versions(really_changed_pkgs, newvers)


  uploaddir = TemporaryDirectory()#new upload directory
  #up_packages = []#[(pname, pnewversion, poldversion),...]

  #new_packages_vers = changes.packages_ver_changes()#{package_name: new_version,...}

  really_changed_pkgs_copy = [p for p in really_changed_pkgs]
  #for all main packages (and their dependencies)
  while True: #{
    main_pkg = None #(repo, br, pn, pv)
    for repo,br,pn,pv in really_changed_pkgs_copy:
      if (repo,br,pn,pv) in main_installed_pkgs:
        main_pkg = (repo, br, pn, pv)
        break
    if not main_pkg: #no changes for main packages
      break
    if not prepare_for_uploading(main_pkg[0], main_pkg[1], main_pkg[2], main_pkg[3], uppath=uploaddir.name, ch_pkgs=really_changed_pkgs_copy):
      return None
  #}

  #for all other packages
  for repo,br,pn,pv in [p for p in really_changed_pkgs_copy]:#{
    if not prepare_for_uploading(repo, br, pn, pv, uppath=uploaddir.name, ch_pkgs=really_changed_pkgs_copy):
      return None
  #}

  #test0
  if len(really_changed_pkgs_copy):
    log.error('Something went wrong while updating. Next packages have not been updated (why?): ', really_changed_pkgs_copy)
    return None

  #test1
  #{
  changed_packages = util.subfiles_list(uploaddir.name)
  if len(changed_packages) != len(really_changed_pkgs):
    log.error('Something went wrong while updating. Packages that should have been updated:', really_changed_pkgs, '. Updated packages:', changed_packages)
    return None
  #}
  #test2
  current_repo = reposrv.current_repo_name()
  current_br = reposrv.current_br_name()
  new_versions = versions.packages_new_versions() #{(repo, br, pn, pv): pnewversion, ...}
  for pname in changed_packages:#{
    pn = zipper.package_name(pname)
    pv = zipper.package_version(pname)
    p_calc_newv = versions.new_package_version(current_repo, current_br, pn, pv=str(), pcache=new_versions, by_name=False)
    if not p_calc_newv:
      log.error("Something went wrong while updating. Package '{}' should not have been updated, but new package (v.{}) have been created".format(pn, pv))
      return None
    if pv != p_calc_newv:
      log.error("Something went wrong. Package {} should have version {}, but its version {}".format(pn, p_calc_newv, pv))
      return None
  #}

  brlog_str = str()
  for (_,_,pn,pv), pnewv in new_versions.items():
    brlog_str += '{} ({} --> {})\n'.format(pn, pv, pnewv)
  print(brlog_str)

  logsfile = reposrv.get_logsfile_path(current_repo, current_br)
  depsfile = reposrv.get_depsfile_path(current_repo, current_br)
  dbfile   = reposrv.get_dbfile_path(current_repo)
  try:
    shutil.copy2(logsfile, os.path.join(uploaddir.name, repowrk.logsfile))
    shutil.copy2(depsfile, os.path.join(uploaddir.name, repowrk.depsfile))
    shutil.copy2(dbfile, os.path.join(uploaddir.name, repowrk.dbfile))
  except Exception as e:
    log.error(e)
    return None
  reposrv.add_branch_logs(os.path.join(uploaddir.name, repowrk.logsfile), brlog_str)
  return uploaddir
#}


#TODO
@reposrv.isdept
@reposrv.check_remotes
def upload(packages=[]):
  #tmpdir = prepare_changes_for_uploading()
  #if not tmpdir:
    #return
  #status = netman.upload_packages(tmpdir)
  #if status ok:
    #remove changes file (clear changes)
    #copy new packages to local repo
  return True
