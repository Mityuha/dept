import ftplib
from ftplib import FTP
import os
import repo_service as reposrv
import log_service as log


def init():
  global ftp_login
  global ftp_passwd
  global ftp_port
  ftp_login = reposrv.remote_repo_user() or ''
  ftp_passwd = reposrv.remote_repo_passwd() or ''
  ftp_port = reposrv.remote_repo_port() or 21

def connect():
  init()
  ftp = FTP(timeout=10)
  try:
    ftp.connect(reposrv.remote_repo_address(), int(ftp_port))
    ftp.login(ftp_login , ftp_passwd)
    return ftp
  except Exception as e:
       log.error("{}\nCheck if there is a server on address '{}' and try again".format(e, reposrv.remote_repo_address()) , to_exit=True)

def ftp_cd(ftp, path, from_root = True):
  rpath = reposrv.remote_repo_path() if from_root else ''
  if rpath: path = os.path.join(rpath, path)
  path = path.replace('\\', '/')
  if from_root and not path.startswith('/'): path = '/' + path
  log.debug('try to change dir from {} to {}'.format(ftp.pwd(), path))
  try:
    ftp.cwd(path)
  except Exception as e:
    ftp.quit()
    log.error('{}\npwd = {}. Cannot change working directory on {}'.format(e, ftp.pwd(), path) , to_exit=True)


def receive(ftp, dest_file, file_name):
  path = os.path.join(ftp.pwd(), file_name)
  path = path.replace('\\', '/')
  file_name = file_name.replace('\\', '/')
  log.notice("try to get file from {0} and save to {1}".format(path, dest_file))
  with open(dest_file, 'wb') as f:
    try:
      ftp.retrbinary("RETR " + file_name, f.write)
    except ftplib.perm_error as e:
      ftp.quit()
      log.error('{}. File {} download failed'.format(e, file_name), to_exit = True)


def connect_and_recv(ftp_path, dest_path, file_name, dest_name=None):
  ftp = connect()
  log.debug("dest path {0}".format(dest_path))
  log.debug("Connected to dept_server")
  ftp_cd(ftp, ftp_path)
  if not dest_name: dest_name=file_name
  dest_file = os.path.join(dest_path, dest_name)
  log.debug("Client is in source folder {0}".format(ftp.pwd()))
  try: receive(ftp, dest_file, file_name)
  except Exception as e: log.error('{}. Receive file {} failed'.format(e, file_name), to_exit=True)
  ftp.quit()

def receive_files(ftp, dest_path, filenames):
  for file in filenames:
    path = os.path.join(ftp.pwd(), file)
    pname = os.path.basename(file)
    dest_file = os.path.join(dest_path,pname)
    try: receive(ftp, dest_file=dest_file, file_name=file)
    except Exception as e: log.error('{}. dest_file = {}, file_name = {}.\nDownload failed'.format(e, dest_file, file), to_exit=True)
    log.notice("download {0} to {1}".format(file, dest_file))

def connect_and_receive_files(ftp_path, dest_path, filenames):
  ftp = connect()
  ftp_cd(ftp, ftp_path)
  log.debug("client is in source folder {0}".format(ftp.pwd()))
  receive_files(ftp, dest_path, filenames)
  ftp.quit()


#files - dict {('ftp-path':'local-path'):[filenames, ]}
def post(files):
  ftp = connect()
  for paths in files:
    if not files[paths]: continue
    ftp_cd(ftp, paths[0])
    filenames = files[paths]
    log.debug('send files {} by ftp'.format(filenames))
    for name in filenames:
      log.debug('try to post file {} in ftp dir {}'.format(name, ftp.pwd()))
      send(ftp, paths[1],name)
  ftp.quit()

#files - dict {('ftp-path':'local-path'):[filenames, ]}
def post_packages(files):
  ftp = connect()
  for paths in files:
    if not files[paths]: continue

    branch_path = os.path.dirname(paths[0])
    if not branch_path.endswith(reposrv.current_br_name()):
      log.error('Unexpected error with branch name: branch_path = {} current_branch_name = {}'.format(branch_path, reposrv.current_br_name()), to_exit = True)
    ftp_cd(ftp, branch_path)
    folder = os.path.basename(paths[0])

    if folder in ftp.nlst():
      ftp_cd(ftp, folder, False)
    else:
      try:
        ftp.mkd(folder)
      except Exception as e:
        ftp.quit()
        log.error('{}\nUnexpected error with creating folder on ftp'.format(e), to_exit = True)
      ftp_cd(ftp, folder, False)

    filenames = files[paths]
    for filename in filenames:
      log.debug('try to post file {} in ftp dir {}'.format(filename, ftp.pwd()))
      send(ftp, paths[1], filename)
    #ftp_cd_if_not_exist(ftp, paths[0])
    ftp_cd(ftp, branch_path)
  ftp.quit()


def send(ftp, local_path, filename):
  try:
    with open(os.path.join(local_path, filename) , 'rb') as f: 
      log.debug('try to STOR "{}" file by ftp'.format(filename))
      ftp.storbinary('STOR ' + filename,f,1024)
  except OSError as e:
    log.error('{}\nunable to open file {}'.format(e,filename), to_exit = True)
  except ftplib.error_perm as e:
    ftp.quit()
    log.error('{}\nunable to create file "{}" in ftp directory "{}" by ftp'.format(e, filename, ftp.pwd()), to_exit = True)


