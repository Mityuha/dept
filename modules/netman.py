#coding=utf-8
import utils as util
import os
from tempfile import TemporaryDirectory
import repo_service as reposrv
import dept_client
from repoworker import dbfile, depsfile, logsfile
import log_service as log
import zipper as zip


#addr -- host address in format: [user:password@]address[:port]
#returns tuple in format: (host, port, user, password)
#None if errors occured
def parse_hostaddr(addr): #{
  l = addr.split('@')
  def parse(s, is_addr):
      tmp = s.split(':')
      if all((is_addr, len(tmp) == 1)): return (tmp[0], None)
      elif len(tmp) == 1: log.error('Bad URL: {}. Format: [user:password@]address[:port]'.format(addr))
      elif len(tmp) == 2:
        if all((is_addr, not tmp[1].isdigit())): log.error('Bad URL: {}. Format: [user:password@]address[:port]'.format(addr))
        else: return (tmp[0], tmp[1])
      else: log.error('Bad URL: {}. Format: [user:password@]address[:port]'.format(addr))
      return None

  if len(l) == 1:
    addr_port = parse(l[0], is_addr=True)
    if addr_port: return (addr_port[0], addr_port[1], None, None)
  elif len(l) == 2:
    us_pw = parse(l[0], is_addr=False)
    if not us_pw: return None
    addr_port = parse(l[1], is_addr=True)
    if addr_port: return (addr_port[0], addr_port[1], us_pw[0], us_pw[1])
  else: log.error('Bad URL: {}. Format: [user:password@]address[:port]'.format(addr))
  return None
#}


#TODO:
def upload_packages(tmpd):#{

  files = os.listdir(tmpd.name)
  if not depsfile in files:
    log.error('there is no {} file to upload'.format(depsfile), to_exit = True)
  if not dbfile in files:
    log.error('there is no {} file to upload'.format(dbfile), to_exit = True)
  if not logsfile in files:
    log.error('there is no {} file to upload'.format(logssfile), to_exit = True)

  for f in files:
    log.debug('filename to transfer {}'.format(f))
  arch_path = os.path.join(reposrv.current_repo_name(), reposrv.current_arch_name())
  branch_path = os.path.join(arch_path, reposrv.current_br_name())

  deps_path = (branch_path, tmpd.name)
  db_path = (arch_path, tmpd.name)
  log_path = (branch_path, tmpd.name)

  zip_files = list(filter(lambda x: x.endswith('.zip'), files))
  transfer_zip = {}

  #create dict. Dict has the structure: {(ftp_folder_path, local_path): [filename]}
  #e.g. {('/deptrepo/amd64/master/act1', '/tmpdir/'):['act1_v1.0.zip', ] , }
  #creating package info, and try to transfer them by ftp
  for f in zip_files:
    folder_name = zip.package_name(f)
    key = (os.path.join(branch_path, folder_name), tmpd.name)
    if key in transfer_zip: transfer_zip[key].append(f)
    else: transfer_zip[key] = [f,]
  #log.debug(transfer_zip)
  dept_client.post_packages(transfer_zip)

  #now, making common info files and post them to ftp
  transfer = {(branch_path, tmpd.name):[depsfile, logsfile], (arch_path, tmpd.name): [dbfile,]}
  dept_client.post(transfer)


def depsfile_ftp_path(): return os.path.join(reposrv.current_repo_name(), reposrv.current_arch_name(), reposrv.current_br_name())
def dbfile_ftp_path():   return os.path.join(reposrv.current_repo_name(), reposrv.current_arch_name())
def logsfile_ftp_path(): return depsfile_ftp_path()

def get_deps_file(dest_path, dest_name=depsfile): #{
  return dept_client.connect_and_recv(depsfile_ftp_path(), dest_path, depsfile, dest_name)
#}

def get_db_file(dest_path, dest_name=dbfile): #{
  return dept_client.connect_and_recv(dbfile_ftp_path(), dest_path, dbfile, dest_name)
#}

def get_logs_file(dest_path, dest_name=logsfile): #{
  return dept_client.connect_and_recv(logsfile_ftp_path(), dest_path, logsfile, dest_name)
#}


def update():#{
  path = os.path.dirname(reposrv.get_depsfile_path(reposrv.current_repo_name(), reposrv.current_br_name()))
  os.makedirs(path, exist_ok = True)

  get_db_file(os.path.dirname(reposrv.get_dbfile_path(reposrv.current_repo_name())))
  get_deps_file(path)
  get_logs_file(path)
  return
#}


#packages -- set of packages in format: {(repo, br, pname, pver), ...}
def download_packages(packages):#{
  uploaddir = TemporaryDirectory()#new upload directory
  ftp_path = os.path.join(reposrv.current_repo_name(), reposrv.current_arch_name(), reposrv.current_br_name())
  files = [os.path.join(pack[2],zip.full_package_name(pack[2], pack[3])) for pack in packages]
  log.debug("Ready for getting files {0}".format(files))
  dept_client.connect_and_receive_files(ftp_path, uploaddir.name, files)
  return uploaddir
#}
