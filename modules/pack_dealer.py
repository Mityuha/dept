#coding=utf-8

import hashlib
from utils import log
import utils as util
from _root_path_ import root_path
import sys
import os
from collections import OrderedDict
from datetime import datetime
from re import compile
from shutil import copy2
#PACKAGES DIRECTORIES
control_dir = 'dept'
script_dir = 'scripts'
bin_dir = 'bin'
control_file = 'control'
hash_file = 'sha1sums'
log_file = 'log'
sums_delim = '  '
ctrlfile_delim = ':'

deps_delim = ','
whitespace = ' '
deps_openbr = '('
deps_closebr = ')'

#ppath -- package abs path

#returns true, if ppath directory struct is correct 
#(i.e. contains control_dir, script_dir and bin_dir without any garbage files and control_file within control_dir)
#@time_decorator
def check_dir_struct(ppath, nofiles_ok=False):#{
  reallst = [control_dir, script_dir, bin_dir,]
  try:
    lst = os.listdir(ppath)
  except Exception:
    log.process_exception()
  for d in reallst:
    if d not in lst:
      log.error('There must be {}, {} and {} directories in {}'.format(control_dir, script_dir, bin_dir, ppath), to_exit=True)
    lst.remove(d)
  if len(lst):
    log.error('There is garbage files: ', lst, '. Path: ', ppath, to_exit=True)
  if not len(os.listdir(os.path.join(ppath, bin_dir))):
    if not nofiles_ok: 
      #try: raise Exception()
      #except: log.process_exception()
      log.error("Directory '{}' is empty. There must be at least one file".format(os.path.join(ppath,bin_dir)), to_exit=True)
  for cfile in [control_file, hash_file, log_file]:
    if cfile not in util.subfiles_list(os.path.join(ppath, control_dir)): log.error("No file '{}' in '{}' directory".format(cfile, os.path.join(ppath,control_dir)), to_exit=True)
  return True
#}


#creates package directories struct, i.e.
#control_dir, script_dir and bin_dir in path_to directory
#path_to must be empty!
def create_dir_struct(path_to): #{
  try:
    os.makedirs(os.path.join(path_to, control_dir))
    os.makedirs(os.path.join(path_to, script_dir))
    os.makedirs(os.path.join(path_to, bin_dir))
    ctrl_dir = os.path.join(path_to, control_dir)
    open(os.path.join(ctrl_dir, control_file), 'w').close()
    open(os.path.join(ctrl_dir, hash_file), 'w').close()
    open(os.path.join(ctrl_dir, log_file), 'w').close()
  except Exception as e:
    log.error('{}\nCannot create dir struct'.format(e))
    return False
  return check_dir_struct(path_to, nofiles_ok=True)
#}


#ppath -- absolute package path
#returns dict in format: {base_file_dir/file: sha1} for bin_dir and script_dir files
#where base_file_dir is relative path from ppath, e.g. bin, scripts, bin/smth/smthelse
def get_package_sums(ppath, nofiles_ok=False):#{
  check_dir_struct(ppath, nofiles_ok)
  #sha1dir -- dir that will be written in sha1sums file (e.g. bin, script, bin/smth/smthelse etc.)
  def get_hashes(sha1dir):
    sums = {}
    abs_path = os.path.join(ppath, sha1dir)
    subdirs = util.subdirs_list(abs_path)
    subfiles = util.subfiles_list(abs_path)
    import posixpath
    for filename in subfiles:
      sums[posixpath.join(sha1dir, filename)] = util.file_sha1_hash(os.path.join(abs_path, filename))
    for subd in subdirs:
      sums.update( get_hashes(posixpath.join(sha1dir, subd)) )
    return sums

  sums = get_hashes(bin_dir)
  s = get_hashes(script_dir)
  sums.update(s)
  return sums
#}


#returns dict in format: {file: sha1}. Dictionary based on hash_file within package
def get_sums_from_file(ppath):#{
  sums = {}
  try:
    f = open(os.path.join(ppath, control_dir, hash_file), 'r')
  except Exception:
    log.error('Error while reading {} file for package path {}'.format(hash_file, ppath), to_exit=True)
  else:
    lines = f.readlines()
    f.close()
    for l in lines:
      lst = l.split(sums_delim)
      if len(lst) != 2:
        log.error("File {}, Line:'{}': syntax error".format(hash_file, l), to_exit=True)
      sums[ util.toposixpath(lst[1].strip()) ] = lst[0].strip() #replace for old packages
  finally:
    f.close()
  return sums
#}

#returns True if there are some files in bin dir (based on hash_file within package)
def has_bin_files(ppath): #{
  for filepath,_ in get_sums_from_file(ppath).items():
    if filepath.startswith(os.path.normpath(bin_dir)): return True
  return False
#}



#updates hashes of bin_dir and script_dir files within files by rewriting hash_file
#@time_decorator
def update_sums_file(ppath, nofiles_ok=False):#{
  sums = get_package_sums(ppath, nofiles_ok)
  try:
    f = open(os.path.join(ppath, control_dir, hash_file), 'w+')
  except Exception:
    log.process_exception(to_exit=False)
    log.error('Could not update {} file for package path {}'.format(hash_file, ppath), to_exit=True)
  else:
    f.write('\n'.join(str(v + sums_delim + k) for k,v in sums.items() ) )
  finally:
    f.close()
  return
#}

  
#verify checksums
def verify_checksums(ppath, nofiles_ok=False):#{
  psums = get_package_sums(ppath, nofiles_ok)
  fsums = get_sums_from_file(ppath)

  if len(psums) > len(fsums):
    tmp = {k:v for k,v in psums.items() if not k in fsums.keys()}
    log.error("Corrupt package. Cannot process unknown package files: {}".format(', '.join(tmp.keys()), to_exit=True))

  for k,v in fsums.items():
    if k not in psums.keys():
      log.error('Corrupt package. Cannot find file {}, that presented in file {}'.format(k, hash_file), to_exit=True)
    if fsums[k] != psums[k]:
      log.error('Corrupt package file: {}. Hashes are different (calculated hash: {}). Reinstall or rebuild package.'.format(k,v), to_exit=True)
  return
#}


mandatory_fields = {'Package': [], 
'Version': [], 
'Maintainer': [], 
'Architecture' : ['i386', 'amd64', 'source', 'all', 'any', 'win32', 'win64'],
'Section': ['admin', 'base', 'comm', 'contrib', 'devel', 'doc', 'editors',
  'electronics', 'embedded', 'games', 'gnome', 'graphics', 'hamradio', 'interpreters',
  'kde', 'libs', 'libdevel', 'mail', 'math', 'misc', 'net', 'news', 'non-free', 'oldlibs',
  'otherosfs', 'perl', 'python', 'science', 'shells', 'sound', 'tex', 'text', 'utils', 'web', 'x11'],
'Description': [], 
}

possible_fields = { 'Depends': [],
'Priority': ['extra', 'optional', 'standard', 'important', 'required'],
}

def possible_arches(): return mandatory_fields['Architecture']
def possible_sections(): return mandatory_fields['Section']
def possible_prios(): return possible_fields['Priority']


dep_patt = compile('\s*([\w-]+)\s*\%s\s*([><=]{0,2})\s*(\d+\.\w+)\s*\%s\s*' % (deps_openbr, deps_closebr,))

#pdep = dependency in format pname([psign] pver), e.g. gcc(<= 4.8)
#return tuple in format: ('pname', {'operator' or None}, {'version' or None}), e.g. ('gcc', '<=', '4.8'), ('libxml', None, None)
#or return empty tuple if errors occured (for logs)
def parse_pdep(pdep):#{
  if (deps_openbr in pdep) and (deps_closebr in pdep):
    vals = dep_patt.findall(pdep)
    return vals[0] #(name, sign, version)
  elif (whitespace not in pdep):
    return (pdep, None, None)
  else:
    log.fatal("Bad dependencies string: '%s'" % (pdep,))
    return None
#}

#format package dependency in format: pname([psign] pver), e.g. gcc(<= 4.8)
def format_pdep(pname, pver, sign='='): #{
  return '{}{}{}{}{}'.format(pname, deps_openbr, sign, pver, deps_closebr)
#}

#format maintainer by name and e-mail
def format_maintainer(name, mail): return '{} <{}>'.format(name, mail)


#fileh -- control file handler
#ctrl_file -- absolute path to control file (for logs only)
def check_control_fileh(fileh, ctrl_file):#{
  attr_dic = OrderedDict()
  last_attr = str()
  lines = fileh.readlines()
  fileh.close()
  #check file line by line
  for lnum,line in enumerate(lines):#{
    if type(line) == type(b''):
      pline = str(line.strip(), 'utf-8')
    else:
      pline = line.strip()
    if len(pline) == 0 or pline.startswith('#'):
      continue
    lst = pline.split(ctrlfile_delim)
    if len(lst) != 2:
      if (last_attr == 'Description'):
        attr_dic[ last_attr ] = attr_dic[ last_attr ] +'\n'+ pline
      else:
        log.error("Error while processing control file %s. No key:value, line: %d:'%s'" % (ctrl_file, lnum,pline), to_exit=True)
    else:
      last_attr = lst[0].strip()
      attr_dic[ lst[0] ] = lst[1].strip()
  #}
  
  #check attributes
  mand_dic = { k:v for k,v in mandatory_fields.items() }
  poss_dic = { k:v for k,v in possible_fields.items() }
  tmp = {}
  for k,v in attr_dic.items():#{
    pk = k.strip()
    pv = v.strip()
    if pk in mand_dic.keys():
      tmp = mand_dic
    elif pk in poss_dic.keys():
      tmp = poss_dic
    else:
      log.error("File %s: unknown field name '%s', or field occurs twice" % (ctrl_file, pk), to_exit=True)

    if tmp[pk] != []:#{
      if pv not in tmp[pk]:
        log.error("File %s: unknown field value '%s', field: '%s'" % (ctrl_file, pv, pk), to_exit=True )
    #}
    del(tmp[pk])

    #check dependencies correction
    if pk == 'Depends':#{
      for pdep in pv.split(deps_delim):
        if not parse_pdep(pdep):
          log.fatal("File %s: bad dependencies string: '%s'" % (ctrl_file, pv,), to_exit=True)
    #}
  #}
  return attr_dic
#}
  

#check if control file is correct
#@time_decorator
def check_control_file(ppath):#{
  ctrl_file = os.path.join(ppath, control_dir, control_file)
  try:
    f = open(ctrl_file, 'r')
  except FileNotFoundError:
    log.process_exception()
  else:
    return check_control_fileh(f, ctrl_file)
  finally:
    f.close()
  return {}
#}


#create control file
def create_control_file(ppath, pname, pver, maint, arch, sect, desc, deps=[], prio=None):
  ctrl_file = os.path.join(ppath, control_dir, control_file)
  lines = []
  lines.append('{}{}{}\n'.format('Package', ctrlfile_delim, pname))
  lines.append('{}{}{}\n'.format('Version', ctrlfile_delim, pver))
  lines.append('{}{}{}\n'.format('Maintainer', ctrlfile_delim, maint))
  lines.append('{}{}{}\n'.format('Architecture', ctrlfile_delim, arch))
  lines.append('{}{}{}\n'.format('Section', ctrlfile_delim, sect))
  lines.append('{}{}{}\n'.format('Description', ctrlfile_delim, desc))
  if prio: lines.append('{}{}{}\n'.format('Priority', ctrlfile_delim, prio))
  if deps: lines.append('{}{}{}\n'.format('Depends', ctrlfile_delim, '{} '.format(deps_delim).join( '{}'.format( format_pdep(pname, pver)) for pname, pver in deps)))
  try:
    with open(ctrl_file, 'w') as f: f.writelines(lines)
  except Exception as e:
    log.error('{}\nCannot create control file'.format(e))
    return False
  return check_control_file(ppath) != {}
#}


#logfile -- absolute path to log file
#logstr -- log string (string or list)
#write text log to file logfile
#with datetime and author information
#TODO: merge branch and package logs functions. To convert package logs to pickle format, like for branch
def add_package_logs(logfile, logstr, uname, umail): #{
  #user_info = util.get_config_values(user_section, confile=configfile)
  if not os.path.exists(logfile):
    open(logfile, 'w').close()
  loglines = []
  with open(logfile, 'r') as f:
    loglines = f.readlines()

  dtstr = datetime.utcnow().strftime('%d.%m.%Y %H:%M')
  ls = ''
  if type(logstr) == type([]):
    for l in logstr:
      ls += l.strip() + '\n'
  else:
    ls = logstr
  lstr = '[{}, {}, {}]: {}'.format(uname, umail, dtstr, ls.strip())
  loglines.append(lstr+'\n')
  with open(logfile, 'w') as f:
    f.writelines(loglines)
  return
#}


#ppath -- package path, specified directory with bin, script and dept directories
#attr -- attribute in control file (possible value see above)
#value -- value in control file
#log_str -- log string, that will be written to package logs (if specified)
#update package control file attribute value
#if value == None, attribute attr would be removed from control file
#@util.auth_checker()
#TODO: delete nofiles_ok parameter
def update_package_value(ppath, attr, value, uname, umail, log_str=None, nofiles_ok=False): #{
  nofiles_ok = not has_bin_files(ppath)
  check_dir_struct(ppath, nofiles_ok)
  if (attr not in mandatory_fields.keys()) and (attr not in possible_fields.keys()):
    log.error("Cannot update package '{}': impossible attribute name: '{}'".format(ppath, attr), to_exit=True)
  attrs = check_control_file(ppath)
  value_old = str()
  if attr in attrs.keys():
    value_old = attrs[attr]
  if value is None:
    if attr in attrs.keys():
      attrs.pop(attr)
  else:
    attrs[attr] = value
  lines = []
  for k,v in attrs.items():
    lines.append(k + ctrlfile_delim + v + '\n')
  with open(os.path.join(ppath, control_dir, control_file), 'w') as f:
    f.writelines(lines)
  lstr = "Package attribute '{}' changed. Old value: '{}', new value: '{}'\n".format(attr, value_old, value)
  if log_str:
    lstr += log_str.strip() + '\n'
  add_package_logs(os.path.join(ppath, control_dir, log_file), lstr, uname, umail)
  return
#}


#ppath -- package path
#attr -- Possible attribute in control file
#get package value from comtrol file and return attribute attr value
#path is the path to package directory (with bin, control and script directories within)
#TODO: delete nofiles_ok parameter
def get_package_value(ppath, attr, nofiles_ok=False):#{
  nofiles_ok = not has_bin_files(ppath)
  check_dir_struct(ppath, nofiles_ok)
  attrs = check_control_file(ppath)
  if attr in attrs.keys():
    return attrs[attr]
  return None
#}


#extractdir -- TemporaryDirectory instance, specified where package has been extracted
#pdir - base package directory (inside package) to update
#uppath - path, what files should be uploaded to new package from
#note: temporary directory must contain uploaded package files in non-zipped format
#to zip this directory, use zipper.create_zip_package function, for instance
def update_package_files(extractdir, pdir, uppath):#{
  nofiles_ok = not has_bin_files(extractdir.name)
  pname = get_package_value(extractdir.name, 'Package', nofiles_ok=nofiles_ok)
  pver = get_package_value(extractdir.name, 'Version', nofiles_ok=nofiles_ok)
  pdir = os.path.normpath(pdir)
  if (pdir != bin_dir) and (pdir != control_dir) and (pdir != script_dir):
    log.error('No such dir {} in package {}:{}. Updating failed.'.format(pdir, pname, pver), to_exit=True)
  #
  verify_checksums(extractdir.name, nofiles_ok=nofiles_ok)
  pdir_files = []
  for filepath,_ in get_sums_from_file(extractdir.name).items():
    if filepath.startswith(os.path.normpath(pdir)):
      pdir_files.append(os.path.relpath(filepath, pdir))

  for f in pdir_files:
    fname = os.path.join(uppath, f)
    if (not os.path.exists(fname)) or (not os.path.isfile(fname)):
      log.warn("Cannot update file '{}' from '{}' for package '{}'. No such file. This case would be considered as file '{}' is not changed".format(f, uppath, pname, f))
      continue
    #replace old files by new files
    try:
      source = os.path.join(uppath, f)
      target = os.path.join(extractdir.name, pdir, f)
      copy2(source, target)
    except FileNotFoundError as e:
      log.error(e, to_exit=True)
  #}
  update_sums_file(extractdir.name, nofiles_ok=nofiles_ok)
  return
#}



if __name__ == '__main__':
  def package_value_test():
    ppath = '/home/jack/dept_tests/plus_package'
    ppath2 = '/home/jack/dept/packages'
    pname = 'plus_v2.0.zip'
    print('get_package_value:', get_package_value(ppath, 'Depends'))
    print('get_zip_package_value:', get_zip_package_value(ppath2, pname, 'Depends'))

  package_value_test()
  #verify_checksums(ppath)
  #update_package_value('/home/jack/dept_tests/plus_package', 'Version', '1.0')
  #zip_package('/home/jack/dept_tests/plus_package')
