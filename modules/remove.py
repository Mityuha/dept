#coding=utf-8

import utils as util
import repo_service as reposrv
import zipper
import os 
import shutil
import repoworker as repowrk
import pack_dealer as pack
import log_service as log
from pkgchanges import rm_changes_for_package


#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#deletes package pname bin path files from repo
#execute package pname scripts (post and pre rm)
#updates local repo info (.installed file)
@reposrv.isdept
def delete_dept_package(repo, br, pname, scripts_exec=True, main_package=False):#{
  #check_package_integrity(repo, br, pname)
  ppath = reposrv.get_package_path(repo, br, pname)
  scripts = zipper.zip_package_files(ppath, pname, in_dir=pack.script_dir)
  if scripts_exec:
    if 'prerm.py' in scripts:
      reposrv.execute_package_script(repo, br, pname, 'prerm.py')
  bins = zipper.zip_package_files(ppath, pname, in_dir=pack.bin_dir)
  repopath = util.parent_path(reposrv.find_dept_path())
  for pfile in bins:#{
    while True:#{
      try:#{
        if not os.path.exists(os.path.join(repopath, pfile)):
          #log.debug('File {} does not exists or it was deleted manually. Continuing...'.format(os.path.join(repopath, pfile)))
          break
        if os.path.basename(pfile) == pfile:
          os.remove(os.path.join(repopath, pfile))
        if os.path.dirname(pfile):
          d = os.path.dirname(pfile)
          while os.path.dirname(d): d = os.path.dirname(d)
          log.debug("Removing directory '{}' ...".format(os.path.join(repopath, d)))
          shutil.rmtree(os.path.join(repopath, d))
        break
      #}
      except Exception as e:
        input(str(e) + '\nDelete this file manually and type any key.')
    #}
  #}
  reposrv.update_installed_file(repo, br, pname, installed=False, main_package=main_package)
  rm_changes_for_package(repo, br, zipper.package_name(pname), zipper.package_version(pname))
  if scripts_exec:
    if 'postrm.py' in scripts:
      reposrv.execute_package_script(repo, br, pname, 'postrm.py')
  return
#}


#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#with_deps -- specified if package would be removed with all dependencies
#remove package pname bin path files from local repo, specified by curpath
#execute package pname scripts (post and pre rm)
#dept_package file (.zip) won't be removed
#updates local repo info (.installed file)
@reposrv.isdept
def remove_dept_package(repo, br, pname, with_deps=True, force=False, scripts_exec=True):#{
  instpkgs = reposrv.get_all_installed_packages()
  pn = zipper.package_name(pname)
  pv = zipper.package_version(pname)

  if reposrv.check_package_installed(repo, br, pn, pv, pcache=instpkgs) is None:#{
    log.notice('package {}:{} ({}:{}) cannot be removed, because it is not installed'.format(pn,pv,repo,br))
    #TODO: candidates (function check_package_installed(by_name=True))
    return
  #}

  if reposrv.check_package_mainstalled(repo, br, pn, pv, pcache=None) is None:#{
    if not force:
      log.error('Cannot remove package {}:{} ({}:{}) because it is a depend package. Specify -f flag to force remove it or type\n\
# dept packages based {}\nto show based packages'.format(pn,pv,repo,br,pn))
      return
    log.notice('Force removing depend package {}:{} with dependencies'.format(pn, pv))
  #}

  pkgstoberm = [(repo, br, pn, pv)]
  if with_deps:#{
    brpath = util.parent_path( reposrv.get_package_path(repo, br, pname) ) #branch path
    for n,v in repowrk.dept_package_dep_list(brpath, pn, pv): #dependencies set [(pkg1, v1), (pkg2, v2),...]
      pkgstoberm.insert(0, (repo,br,n,v))
  #}

  for r,b,n,v in pkgstoberm:#{
    main_pkg = False
    if ((r,b,n,v) == (repo, br, pn, pv)) and (not force):
      main_pkg = True
    if reposrv.check_package_installed(r, b, n, v, pcache=instpkgs) is None:
      log.warn('package {}:{} ({}:{}) cannot be removed, because it is not installed. Skipping...'.format(n,v,r,b))
      continue
    delete_dept_package(r, b, zipper.full_package_name(n,v), main_package=main_pkg)
  #}
  return
#}
