#coding=utf-8

import utils as util
import repo_service as reposrv
import zipper
import remove as pkgrm
import repoworker as repowrk
import pack_dealer as pack
import log_service as log


#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#scripts_exec -- flag whether install scripts would be executed
#extracts package pname bin path files to repo, specified by util.curpath (i.e. cwd)
#execute package pname scripts (post and pre install)
#updates local repo info (.installed file)
@reposrv.isdept
def extract_dept_package(repo, br, pname, scripts_exec=True, main_package=False, nofiles=False):
  reposrv.check_package_integrity(repo, br, pname)
  ppath = reposrv.get_package_path(repo, br, pname)
  scripts = zipper.zip_package_files(ppath, pname, '', pack.script_dir)
  if scripts_exec:
    if 'preinst.py' in scripts:
      reposrv.execute_package_script(repo, br, pname, 'preinst.py')
  if not nofiles: #extract if nofiles == False
    zipper.extract_zip_package(ppath, pname, util.parent_path( reposrv.find_dept_path() ), None, pack.bin_dir)
  reposrv.update_installed_file(repo, br, pname, installed=True, main_package=main_package, nofiles=nofiles)
  if scripts_exec:
    if 'postinst.py' in scripts:
      reposrv.execute_package_script(repo, br, pname, 'postinst.py')
  return


#repo -- repo name
#br -- branch name
#pname -- package name in format package_v{version}.zip
#with_dept -- specified if package would be install with all dependencies
#install package pname bin path files to local repo, specified by curpath
#executes package pname scripts (post and pre inst)
#updates local repo info (.installed file)
@reposrv.isdept
@reposrv.check_remotes
def install_dept_package(repo, br, pname, with_deps=True, force=False, assumeyes=False, nofiles=False):#{
  instpkgs = reposrv.get_all_installed_packages()
  pn = zipper.package_name(pname)
  pv = zipper.package_version(pname)
  #For main package
  if reposrv.check_package_installed(repo, br, pn, pv, pcache=instpkgs):
    log.notice('package {}:{} ({}:{}) is already installed. Skipping...'.format(pn,pv,repo,br))
    return

  oldmainst = reposrv.check_package_mainstalled(repo, br, pn, pv, by_name=True)
  if oldmainst:#{
    log.notice('package {} ({}) is already installed. It will be updated to version {}'.format(pn, oldmainst[3], pv))
    pkgrm.remove_dept_package(oldmainst[0], oldmainst[1], zipper.full_package_name(oldmainst[2], oldmainst[3]), with_deps=with_deps, force=force)#remove old
    install_dept_package(repo, br, pname, with_deps=with_deps, force=force, assumeyes=assumeyes, nofiles=nofiles)#install new (recursive to avoid conflicted packages)
    return
    #}

  oldpkg = reposrv.check_package_installed(repo, br, pn, pv, by_name=True, pcache=instpkgs)
  if oldpkg:#{
    if not force:
      log.notice('package {} ({}) is already installed. To force install package specify -f flag.\n\
Note: In case of force install package {} will not be inserted to main list, because it is depend package'.format(pn, oldpkg[3], pn))
      return
    pkgrm.remove_dept_package(oldpkg[0], oldpkg[1], zipper.full_package_name(oldpkg[2], oldpkg[3]), with_deps=with_deps, force=force)#remove old
    install_dept_package(repo, br, pname, with_deps=with_deps, force=force, assumeyes=assumeyes, nofiles=nofiles)#install new (recursive to avoid conflicted packages)
    return
    #}
    
    
  #conflicted -- packages with the same name and different version
  conflicted = []#list of lists, i.e. [ [(instrepo, instbr, pname, instver), (newrepo, newbr, pname, newver)], ... ]
  pkgstobeinst = [(repo, br, pn, pv)]
  #conflicted.append([oldpkg, (repo, br, pn, pv)])

  #Conflicted section
  if with_deps:#{
    brpath = util.parent_path( reposrv.get_package_path(repo, br, pname) ) #branch path
    deps = repowrk.dept_package_dep_list(brpath, pn, pv)#dependencies set [(pkg1, v1), (pkg2, v2),...]
    for n, v in deps:
      newpkg = (repo, br, n, v)
      if reposrv.check_package_installed(repo, br, n, v, pcache=instpkgs):
        log.notice('package {}:{} ({}:{}) is already installed. Skipping...'.format(n,v,repo,br))
        continue
      pkgstobeinst.insert(0, newpkg)#insert at the front of the list
      oldpkg = reposrv.check_package_installed(repo, br, n, v, by_name=True, pcache=instpkgs)
      if oldpkg:
        conflicted.append( [oldpkg, newpkg] )
  #}

  if len(conflicted):#{
    qu = 'The following packages will be replaced:\n'
    for old,new in conflicted:
      qu += '{}:{}:{}:{}'.format(old[0], old[1], old[2], old[3]) + ' --> ' + '{}:{}:{}:{}'.format(new[0], new[1], new[2], new[3]) + '\n'
    if not assumeyes:#{
      qu += 'Continue?'
      if not util.ask_yes_no_question(qu):
        log.notice('Install process terminated', to_exit=True)
    else:
      log.notice(qu)   
    #}
  #}

  #Install section
  #1. For conflicted
  for old,new in conflicted:#{
    pkgrm.remove_dept_package(old[0], old[1], zipper.full_package_name(old[2], old[3]), with_deps=False, force=force)
    main_pkg = False
    if (new == (repo, br, pn, pv)) and (not force) :
      main_pkg = True
    extract_dept_package     (new[0], new[1], zipper.full_package_name(new[2], new[3]), main_package=main_pkg, nofiles=nofiles)
    try:
      pkgstobeinst.remove(new)
    except Exception:
      log.error('Something went wrong! pkgstobeinst list', pkgstobeinst, 'does not contain conflicted item', new, '.Check it urgently!')
  #}
 
  #2. For remains (including head package)
  for r,b,n,v in pkgstobeinst:#{
    main_pkg = False
    if ((r,b,n,v) == (repo, br, pn, pv)) and (not force):
      main_pkg = True
    extract_dept_package(r, b, zipper.full_package_name(n,v), main_package=main_pkg, nofiles=nofiles)
  #}
  return
#}

