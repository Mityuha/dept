#coding=utf-8

from enum import IntEnum
import inspect
from sys import exit
from traceback import print_exc, print_stack

class LogLevel(IntEnum):
  debug = 0
  notice= 1
  warn = 2
  error = 3
  fatal = 4

curLevel = LogLevel.debug

def check_levels(level):
  if int(curLevel) <= int(level):
    return True
  return False

def print_message(*mes, level, stack, to_exit):
  #frame = stack[1]
  tup = inspect.getframeinfo(stack[1][0])
  print('[%s][%s][L%d][%s]:' %(tup[0], tup[2], tup[1], level,), *mes)
  if to_exit:
    exit(1)

def debug(*mes, to_exit=False):
  if check_levels(LogLevel.debug):
    print_message(*mes, level='D', stack=inspect.stack(), to_exit=to_exit)

def notice(*mes, to_exit=False):
  if check_levels(LogLevel.notice):
    print_message(*mes, level='N', stack=inspect.stack(), to_exit=to_exit)

def warn(*mes, to_exit=False):
  if check_levels(LogLevel.warn):
    print_message(*mes, level='W', stack=inspect.stack(), to_exit=to_exit)

def error(*mes, to_exit=False):
  if check_levels(LogLevel.error):
    print_message(*mes, level='E', stack=inspect.stack(), to_exit=to_exit)

def fatal(*mes, to_exit=False):
  if check_levels(LogLevel.fatal):
    print_message(*mes, level='F', stack=inspect.stack(), to_exit=to_exit)

def process_exception(to_exit = True):
  fatal('Exception occured!')
  print_stack()
  print('-'*20)
  print_exc()
  if to_exit:
    exit(1)
