#coding=utf-8

import include
from include import dept_test_dir
import zipper
import pack_dealer as pack
import os


def runtests(): #{
  pkgs_path = '/home/jack/debs/depts'
  outpath = os.path.join(dept_test_dir, 'objects/deptrepo/master')
  def create_package(name):#{copied from repoworker.py
    ppath = os.path.join(pkgs_path, name)
    pack.check_dir_struct(ppath)
    pattrs = pack.check_control_file(ppath)
    pack.update_sums_file(ppath)
    pack.verify_checksums(ppath)
    pname = pattrs['Package']
    pver = pattrs['Version']
    parch = pattrs['Architecture']
    poutp = os.path.join(outpath, name)
    zipper.create_zip_package(ppath, pname, poutp, pver)
  #}
  create_package('minus')
  create_package('plus')
  create_package('mult')
  create_package('act1')
  create_package('act2')
  create_package('act3')
#}


if __name__ == '__main__':
  runtests()
