#coding=utf-8

import include
import utils as util
from install import install_dept_package
from remove import remove_dept_package
from repo_service import ddirname, initrepo
import pkgchanges as changes
from shutil import copytree
import os

def runtests():
  #
  repo = 'deptrepo'
  br = 'master'
  pn = 'act3'
  pname = 'act3_v1.0.zip'
  #
  #
  install_dept_package(repo, br, pname)
  changes.add_change(pn, changes.act_add, changes.inst_file)
  changes.add_change(pn, changes.act_rm, changes.inst_file)
  changes.add_change(pn, changes.act_alter, changes.inst_dep)
  changes.add_change(pn, changes.act_update, changes.inst_package)
  changes.show_changes()
  remove_dept_package(repo, br, pname)

if __name__ == '__main__':#{
  runtests()
