#coding=utf-8

import inspect, os, sys

#cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
#if cmd_folder not in sys.path:
#  sys.path.insert(0, cmd_folder)

 # use this if you want to include modules from a subfolder
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"../../modules")))
if cmd_subfolder not in sys.path:
  sys.path.insert(0, cmd_subfolder)

make_name = ''
src_path = ''
objects_path = ''

if os.name == 'nt':
  make_name = 'nmake'
  src_path = '../winsrc'
  objects_path = 'winobjects'
elif os.name == 'posix':
  make_name = 'make'
  src_path = '../src'
  objects_path = 'objects'

import utils as util
from shutil import copytree
from repo_service import initrepo, ddirname

dept_test_dir = os.path.abspath(os.path.join(os.getcwd(), '..'))

repo_name = 'deptrepo'
repo_addr = 'dept.lan'
repo_br = 'master'
if os.name == 'posix':
  repo_arch = 'amd64'
elif os.name == 'nt':
  repo_arch = 'win32'

deptlocal = os.path.join(dept_test_dir, 'debug')
util.curpath = deptlocal

attempts = int(5)
def create_test_repo():
  a = int(0)
  if not os.path.exists(deptlocal):#{
  #os.makedirs(os.path.join(deptlocal, ddirname, 'objects'))
    while True:
      try:
        copytree(os.path.abspath(os.path.join(deptlocal, '..', objects_path)), os.path.join(deptlocal, ddirname, 'objects'))
        break
      except Exception as e:
        a += 1
        if a == attempts: sys.exit("{}\nCannot create test repo for '{}'".format(str(e), deptlocal))
    initrepo(arch=repo_arch, brname=repo_br, rname=repo_name, raddr=repo_addr, uname='test', umail='test@test.ru')
#}

def delete_test_repo(): #{
  import shutil
  rp = os.path.abspath(os.path.join(util.curpath, '../debug'))
  while os.path.exists(rp):
    try:
      shutil.rmtree(os.path.abspath(rp), ignore_errors = True)
    except Exception as e: print('{}\nTry to delete path "{}" manually'.format(e,rp))
 #}

create_test_repo()

def print_test(text): print('*'*10 + text + '*'*10)
def exit_test(text): 
  input('*'*10 + text + '*'*10 + '\nType any key to continue')
  sys.exit(1)
