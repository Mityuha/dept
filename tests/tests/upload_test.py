#coding=utf-8

import include
from include import dept_test_dir
import utils as util
import upload
from shutil import copytree, rmtree
import os
import sys
from install import install_dept_package
from remove import remove_dept_package
import upload as up
import zipper
import pack_dealer as pack
from repoworker import dept_package_value, dbfile, depsfile
import pkgchanges as changes
from tempfile import TemporaryDirectory
import subprocess
from repo_service import ddirname, initrepo

repo = 'deptrepo'
br = 'master'


def bashexec(cmd):  #{
  pipe = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  #reader = io.TextIOWrapper(pipe.stdout, encoding='utf8')
  outs = [ str(one, errors='ignore') for one in pipe.stdout.readlines()]
  #outs = [one for one in reader.readline()]
  errs = [str(one, errors='ignore') for one in pipe.stderr.readlines()]
  pipe.communicate()
  return (''.join(outs)).strip(), errs, pipe.returncode
#}



#pkg_list -- list of packages names (package_vversion.zip)
#returns True, if packages prepared for uploading
#returns False otherwise
def update_change_test(pkg_list, to_make=True):#{
  changes.clear_all_changes()
  for pkg in pkg_list:
    pn = zipper.package_name(pkg)
    pv = zipper.package_version(pkg)
    changes.add_change((repo,br,pn,pv), changes.act_update, changes.inst_package)
  print('\nRebuild ' + ','.join(pkg_list) + ' packages:\n')
  input_str = str()
  for num, package in enumerate(pkg_list):
    pkg = zipper.package_name(package)
    #pv = zipper.package_version(package)
    if num == 0:
      input_str += 'cd {}/{} && {} '.format(include.src_path, pkg, include.make_name)
    else:
      input_str += '&& cd ../{} && {} '.format(pkg, include.make_name)
  #input_str += '&& popd'
  #print('\n and type any key to continue...')
  print(input_str)
  tup = bashexec(input_str)
  print(tup[0].encode('utf-8'))
  print(''.join(tup[1]).encode('utf-8'))
  print()
  upload_dir = up.prepare_changes_for_uploading()
  if not upload_dir:
    print('\n' + '*'*5, 'Packages not uploaded', '*'*5 + '\n')
    return False

  #pkg_names_copy = [zipper.package_name(p) for p in pkg_list]
  pkg_names_copy = [p for _,_,p,_ in up.get_really_changed_packages()]
  print('really changed packages: ', pkg_names_copy)
  new_pkgs = up.scan_uppath(upload_dir.name)
  print('Files inside %s directory: '% upload_dir.name, new_pkgs)
  for pn, vers in new_pkgs.items():#{
    try:
      pkg_names_copy.remove(pn)
    except ValueError as e:
      print('Package {} should not be updated or it is updated twice\n{}'.format(pn, e))
      return False

    if len(vers) != 1:
      print('Package {} has more that one version (or no versions) while upload(versions: {})'.format(pn, ', '.join(vers)))
      return False

    pver = vers.copy().pop()
    pfn = zipper.full_package_name(pn, pver)
    
    print('#'*20)
    print('Name: ', pfn)
    print('Version: ', dept_package_value(upload_dir.name, pfn, 'Version'))
    print('Depends:', dept_package_value(upload_dir.name, pfn, 'Depends'))
    #input('press any key...')
  #
  #

  if len(pkg_names_copy):
    print('Next packages: {}\n has not been updated'.format(', '.join(pkg_names_copy)))
    return False

  print('*'*5, 'System files tests', '*'*5)
  if not all( (up.update_dbfile(upload_dir.name), up.update_depsfile(upload_dir.name),) ):
    print('*'*5, 'System files tests FAILED', '*'*5)
    return False
  
  tmp_db_br = util.load_obj(os.path.join(upload_dir.name, dbfile))[br]
  tmp_deps = util.load_obj(os.path.join(upload_dir.name, depsfile))

  for pn, vers in new_pkgs.items(): #{
    ver = vers.copy().pop()
    try:
      if ver not in tmp_db_br[pn]:
        print('.db has not been updated. Package {} has no version {}'.format(pn, ver))
        return False
    except Exception as e:
      print('Package {} has no entries in .db file, or {}'.format(pn, e))
      return False

    if not (pn, ver) in tmp_deps.keys():
      print('Package {}:{} has no entry in .deps file. Perhaps, file .deps has not been updated'.format(pn,ver))
      return False
    ctrlf_h = zipper.zip_package_hfile(ppath=upload_dir.name, pname=pn, fname=pack.control_file, pver=ver, dir=pack.control_dir)
    pkg_attrs = pack.check_control_fileh(ctrlf_h, ctrl_file=os.path.join(upload_dir.name, '{}_v{}'.format(pn,pv), pack.control_dir, pack.control_file))
    if not 'Depends' in pkg_attrs.keys(): continue
    depends = pkg_attrs['Depends']
    deps_from_control = set()
    for d in depends.split(','):
      dpn, _, dpv = pack.parse_pdep(d)
      deps_from_control.add((dpn, dpv))
    d = util.load_obj(os.path.join(upload_dir.name, depsfile))
    deps_from_db = d[(pn, ver)]
    #test1
    if len(deps_from_db) != len(deps_from_control):
      print('Depends from control file differ from depends from .deps file')
      print('Depends from control: ', deps_from_control)
      print('Depends from .deps  : ', deps_from_db)
      return False
    #test2
    if len(deps_from_control.difference(deps_from_db)):
      print('Depends from control file but not from .deps file: ', deps_from_control.difference(deps_from_db))
      return False
    #test3
    if len(deps_from_db.difference(deps_from_control)):
      print('Depends from .deps file but not from control file: ', deps_from_db.difference(deps_from_control))
      return False

  #}
  print('*'*5, 'System files tests PASSED', '*'*5)
  return upload_dir
#}


def runtests(): #{
#  repo_name = 'deptrepo'
#  repo_addr = 'dept.lan'
#  repo_br = 'master'
#  if os.name == 'POSIX':
#    repo_arch = 'amd64'
#  else:
#    repo_arch = 'win32'

#  deptlocal = os.path.join(dept_test_dir, 'debug')
#  util.curpath = deptlocal
#  if not os.path.exists(deptlocal):#{
    #os.makedirs(os.path.join(deptlocal, ddirname, 'objects'))
#    copytree(os.path.abspath(os.path.join(deptlocal, '..', 'objects')), os.path.join(deptlocal, ddirname, 'objects'))
#    initrepo(arch=repo_arch, brname=repo_br, rname=repo_name, raddr=repo_addr)
  #}
  act3_pkg = 'act3_v1.0.zip'
#  pn = 'act3'
#  pv = '1.0'
  pnewv = None

  minus_pkg = 'minus_v1.0.zip'
  plus_pkg = 'plus_v1.0.zip'
  mult_pkg = 'mult_v1.0.zip'
  act1_pkg = 'act1_v1.0.zip'
  act2_pkg = 'act2_v1.0.zip'
  meta1_pkg = 'meta1_v1.0.zip'
  
  #test 1: updating min, plus and mult packages
  include.print_test('UPLOAD TESTS')
  include.print_test('TEST1')
  updir = TemporaryDirectory()
  install_dept_package(repo, br, act3_pkg)
  res = update_change_test([minus_pkg, plus_pkg, mult_pkg])
  remove_dept_package(repo, br, act3_pkg)
  if not res:
    sys.exit('*'*10 + 'TEST1 FAILED' + '*'*10)

  #test 2: updating plus and act1 packages
  include.print_test('TEST2')
  updir = TemporaryDirectory()
  install_dept_package(repo, br, act3_pkg)
  res = update_change_test([plus_pkg, act1_pkg])
  remove_dept_package(repo, br, act3_pkg)
  if not res:
    sys.exit('*'*10 + 'TEST2 FAILED' + '*'*10)

  #test 3: updating act1 and act2 packages
  include.print_test('TEST3')
  updir = TemporaryDirectory()
  install_dept_package(repo, br, act3_pkg)
  res = update_change_test([act1_pkg, act2_pkg])
  remove_dept_package(repo, br, act3_pkg)
  if not res:
    sys.exit('*'*10 + 'TEST3 FAILED' + '*'*10)

  #test 4: updating mult, act2 and act3 packages
  include.print_test('TEST4')
  updir = TemporaryDirectory()
  install_dept_package(repo, br, act3_pkg)
  res = update_change_test([mult_pkg, act2_pkg, act3_pkg])
  remove_dept_package(repo, br, act3_pkg)
  if not res:
    sys.exit('*'*10 + 'TEST4 FAILED' + '*'*10)

  #test 5: updating act1 and plus packages (test for lmts)
  include.print_test('TEST5')
  updir = TemporaryDirectory()
  install_dept_package(repo, br, act3_pkg)
  res = update_change_test([act1_pkg, plus_pkg])
  remove_dept_package(repo, br, act3_pkg)
  if res:
    sys.exit('*'*10 + 'TEST5 FAILED' + '*'*10)

  #test 6: updating act3, mult and act2 packages (test for lmts)
  include.print_test('TEST6')
  updir = TemporaryDirectory()
  install_dept_package(repo, br, act3_pkg)
  res = update_change_test([act3_pkg, mult_pkg, act2_pkg])
  remove_dept_package(repo, br, act3_pkg)
  if res:
    sys.exit('*'*10 + 'TEST6 FAILED' + '*'*10)
  
  #test 7: updating mult, act2, act3. meta1 should be updated
  include.print_test('TEST7')
  updir = TemporaryDirectory()
  install_dept_package(repo, br, meta1_pkg)
  res = update_change_test([mult_pkg, act2_pkg, act3_pkg])
  remove_dept_package(repo, br, meta1_pkg)
  if not res:
    sys.exit('*'*10 + 'TEST7 FAILED' + '*'*10)
    
  #test 8: updating mult and act2, meta1 should not be updated
  include.print_test('TEST8')
  updir = TemporaryDirectory()
  install_dept_package(repo, br, meta1_pkg)
  res = update_change_test([mult_pkg, act2_pkg])
  remove_dept_package(repo, br, meta1_pkg)
  if res:
    sys.exit('*'*10 + 'TEST8 FAILED' + '*'*10)
  
  print('*'*30)
  include.print_test('UPLOAD TESTS PASSED')
  print('*'*30)
#}



if __name__ == '__main__': #{
  runtests()
#}  
  
