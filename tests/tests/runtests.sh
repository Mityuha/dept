#!/bin/bash

rm -rf ../debug
function test_ok {
    python3 $1
    local status=$?
    if [ $status -ne 0 ]; then
        echo "test $1 failed" >&2
        rm -rf ../debug
		exit 1
    fi
    rm -rf ../debug
    return
}

test_ok instrm_test.py
test_ok versions_test.py
test_ok upload_test.py
test_ok hostaddr_test.py
test_ok cpkg_test.py
rm -rf ../debug
echo "ALL TESTS PASSED"
