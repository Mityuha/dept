python = 'python3' 
import os, sys, shutil
if os.name == 'nt': python = 'python'


import importlib
import include
  
def runtest(testfile):
  include.create_test_repo()
  try:
    test = importlib.import_module(testfile)
    test.runtests()
    include.delete_test_repo()
  except Exception as e: 
    include.delete_test_repo()
    include.exit_test(str(e))

runtest('instrm_test')
runtest('versions_test')
runtest('upload_test')
runtest('hostaddr_test')
runtest('cpkg_test')
print("ALL TESTS PASSED")

#for installer
import instrm_test
import versions_test
import upload_test
import hostaddr_test
import cpkg_test
include.delete_test_repo()

