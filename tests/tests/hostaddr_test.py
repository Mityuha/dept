#coding=utf-8

import include
import utils as util
from shutil import copytree
import os
import netman
import sys

def check_expected(hostaddr, ehost, eport, euser, epasswd, bad_format=False):
  tup = netman.parse_hostaddr(hostaddr)
  if bad_format:
    if not (tup is None): return False
    return True
  host,port,user,passwd = tup
  if not all(((host == ehost),
  (port == eport),
  (user == euser),
  (passwd == epasswd))): return False
  return True
  
def runtests(): #{
  include.print_test('PARSE HOST ADDRESS TESTS')
  if not all((\
  check_expected('ftp:user@ftp.lan', 'ftp.lan', None, 'ftp', 'user'),
  check_expected('storage.lan', 'storage.lan', None, None, None),
  check_expected('192.168.0.1:211', '192.168.0.1', '211', None, None),
  check_expected('192.12.13.14', '192.12.13.14', None, None, None),
  check_expected('asd:dsa@ftp.lan:12', 'ftp.lan', '12', 'asd', 'dsa'),
  check_expected('qwe:rty@192.23.13.13:12', '192.23.13.13', '12', 'qwe', 'rty'),
  check_expected('qwe@storage.lan', None, None, None, None, bad_format=True),
  check_expected('qwe.asd@storage.lan', None, None, None, None, bad_format=True),
  check_expected(':113:12@storage.lan', None, None, None, None, bad_format=True),
  check_expected('@storage.lan', None, None, None, None, bad_format=True),
  check_expected('ftp@12', None, None, None, None, bad_format=True))): include.exit_test('PARSE HOST ADDRESS TESTS FAILED')
  include.print_test('PARSE HOST ADDRESS TESTS PASSED')
#}
  

if __name__ == '__main__':#{
  runtests()
#}
