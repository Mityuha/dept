#coding=utf-8

from upload_test import update_change_test
import include
from include import dept_test_dir
import utils as util
import upload
from shutil import copytree, rmtree
import os
import sys
from install import install_dept_package
from remove import remove_dept_package
from upload import prepare_for_uploading, prepare_changes_for_uploading
import zipper
from repoworker import dept_package_value
import pkgchanges as changes
from tempfile import TemporaryDirectory
import subprocess
from repo_service import ddirname, initrepo
import pkgversion as vers

repo = 'deptrepo'
br = 'master'


def runtests(): #{
  act3_pkg = 'act3_v1.0.zip'
  pnewv = None

  minus_pkg = 'minus_v1.0.zip'
  plus_pkg = 'plus_v1.0.zip'
  mult_pkg = 'mult_v1.0.zip'
  act1_pkg = 'act1_v1.0.zip'
  act2_pkg = 'act2_v1.0.zip'

  #test 1: updating min, plus and mult packages
  vers.clear_versions()
  print('*'*10 + 'VERSIONS TESTS' + '*'*10)
  install_dept_package(repo, br, act3_pkg)
  updir = update_change_test([minus_pkg, plus_pkg, mult_pkg, act1_pkg, act2_pkg, act3_pkg])
  remove_dept_package(repo, br, act3_pkg)
  if not updir:
    sys.exit('*'*10 + 'VERSIONS TESTS FAILED' + '*'*10)

  ver_inc = vers.ver_inc
  new_versions = vers.packages_new_versions()
  for pkg, newpv in new_versions.items():
    _,_,pn,_ = pkg
    expected_ver = vers.get_next_pver(repo, br, pn, {})
    if newpv != expected_ver:
      print('*'*10 + 'VERSIONS TEST FAILED.' + '*'*10)
      print('Package {} has new version = {}, but expected version = {}'.format(pn, newpv, expected_ver))
      sys.exit(1)
    if not os.path.isfile(os.path.join(updir.name,zipper.full_package_name(pn, newpv))):
      print('*'*10 + 'VERSIONS TEST FAILED.' + '*'*10)
      print('Package {} is not created for some reasons'.format(zipper.full_package_name(pn, newpv)))
      sys.exit(1)

  print('*'*10 + 'VERSIONS TESTS PASSED' + '*'*10)
#}


if __name__ == '__main__':
  runtests()
#}  
  
