#coding=utf-8

import include
from install import install_dept_package
from remove import remove_dept_package
from repo_service import ddirname, initrepo
import utils as util
from shutil import copytree
import os


def install_remove(repo, br, pname, nofiles=False):
  print('install:')
  install_dept_package(repo, br, pname, nofiles=nofiles)
  print('remove:')
  remove_dept_package(repo, br, pname)
  return

def install_install(repo, br, pname, nofiles=False):
  print('install1:')
  install_dept_package(repo, br, pname, nofiles=nofiles)
  print('install2:')
  install_dept_package(repo, br, pname, nofiles=nofiles)
  return

def remove_remove(repo, br, pname, nofiles=False):
  print('remove1:')
  remove_dept_package(repo, br, pname)
  print('remove2:')
  remove_dept_package(repo, br, pname)
  return

def remove_install(repo, br, pname, nofiles=False):
  print('remove:')
  remove_dept_package(repo, br, pname)
  print('install:')
  install_dept_package(repo, br, pname, nofiles=nofiles)
  return
  
def runtests():
  #
  def _run(nofiles=False): #{
    repo = 'deptrepo'
    br = 'master'
    pname = 'act3_v1.0.zip'
    include.print_test('INSTALL-REMOVE TESTS')
    #
    #
    print('install_remove test:')
    install_remove(repo, br, pname, nofiles=nofiles)
    #
    print('remove_install test:')
    remove_install(repo, br, pname, nofiles=nofiles)
    #
    print('install_install test:')
    install_install(repo, br, pname, nofiles=nofiles)
    #
    print('remove_remove test:')
    remove_remove(repo, br, pname, nofiles=nofiles)
    #
    #to upgrage package test
    pname_plus0 = 'plus_v1.0.zip'
    pname_plus1 = 'plus_v1.1.zip'

    print('install_1.0_install_1.1_remove_1.0 test:')
    install_install(repo, br, pname_plus0, nofiles=nofiles)
    install_install(repo, br, pname_plus1, nofiles=nofiles)
    install_remove(repo, br, pname_plus0, nofiles=nofiles)
    include.print_test('INSTALL-REMOVE TESTS PASSED')
  #}
  _run()
  _run(nofiles=True)


if __name__ == '__main__':
  runtests()

  

