#coding=utf-8

import include
import utils as util
import commands as cmd
from shutil import copytree
import os
#import netman
import repo_service as reposrv
import upload as up
from pkgchanges import clear_all_changes
from pkgversion import clear_versions
import sys

def checkup():
  changed_packages = up.get_really_changed_packages()
  print(changed_packages)
  tmpdir = up.prepare_changes_for_uploading(changed_packages, with_lmts=True)
  if not tmpdir: include.exit_test('CREATE PACKAGES TESTS FAILED')
  if not all( (up.update_dbfile(tmpdir.name), up.update_depsfile(tmpdir.name),) ): include.exit_test('CREATE PACKAGES TESTS FAILED')
  clear_all_changes()
  clear_versions()
  return


def cpkg_test(pname, pver='1.0', depended=None, desc="", section=None, priority=None, localpath=None, files=None, nofiles=False,
              username="", usermail='', scripts=None, yes=True, postfix=None):
  class Args(object):
    def __init__(self, args): self.__dict__ = args

  args = Args({cmd.a_pname:pname, cmd.a_pver:pver, cmd.a_depended:depended, cmd.a_desc:desc, cmd.a_section:section, cmd.a_priority:priority,
                             cmd.a_localpath:localpath, cmd.a_files:files, cmd.a_nofiles:nofiles, 
                             cmd.a_username:username, cmd.a_usermail:usermail, cmd.a_scripts:scripts, cmd.a_yes:yes, cmd.a_postfix:postfix})
  return cmd.create_package(args)
  
  
def runtests(): #{
  repo = 'deptrepo'
  br = 'master'
  include.print_test('CREATE PACKAGES TESTS')
  print('1. simple package test with single file')
  open(os.path.join(util.curpath, 'act4.so'), 'wb').close()
  if not cpkg_test(pname='act4'): include.exit_test('CREATE PACKAGES TESTS FAILED')
  if not os.path.isdir(reposrv.get_package_path(repo, br, 'act4_v1.0.zip')): print('Package act4 directory is not exist') and include.exit_test('CREATE PACKAGES TESTS FAILED')
  checkup()
  print('2. simple package test with no files')
  if not cpkg_test(pname='act4_nf', nofiles=True): include.exit_test('CREATE PACKAGES TESTS FAILED')
  if not os.path.isdir(reposrv.get_package_path(repo, br, 'act4_nf_v1.0.zip')): print('Package act4_nf directory is not exist') and include.exit_test('CREATE PACKAGES TESTS FAILED')
  checkup()
  print("3. cannot find files test")
  if cpkg_test(pname='some_package'): include.exit_test('CREATE PACKAGES TESTS FAILED')
  print('4. cannot find files specified')
  if cpkg_test(pname='act4', files=['act4d.so', 'act4e.so']): include.exit_test('CREATE PACKAGES TESTS FAILED')
  print('5. script path is not exist')
  if cpkg_test(pname='act4', scripts='not_existed/directory'): include.exit_test('CREATE PACKAGES TESTS FAILED')
  print('6. priority is not exists')
  if cpkg_test(pname='act4', priority='some_priority'): include.exit_test('CREATE PACKAGES TESTS FAILED')
  print('7. section is not exists')
  if cpkg_test(pname='act4', section='some_section'): include.exit_test('CREATE PACKAGES TESTS FAILED')
  print('8. version is not reachable')
  if cpkg_test(pname='act4', pver='1.0000001'): include.exit_test('CREATE PACKAGES TESTS FAILED')
  print('9. depended packages test')
  if not cpkg_test(pname='act4', depended=['act1', 'act2', 'act3']): include.exit_test('CREATE PACKAGES TESTS FAILED')  
  checkup()
  include.print_test('CREATE PACKAGES TESTS PASSED')
#}



if __name__ == '__main__':
  runtests()
